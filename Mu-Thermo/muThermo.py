#Mu-Thermo

#Import Modules
import numpy as np
import time as time
import sympy as sp
import pandas as pd
import math
import mpmath as mpm
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import colors
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from termcolor import colored
import sys
import os
import textwrap

#TODO List
"""
We need to rotate the tetragonals to (001), orthorhombics to (011), and rhomohedral to (111) this will work for the bulk systems so we can better compare to BZTO-18-Peng
"""

def Intro_Message():
	print('')
	print('Welcome to \u03BC-Thermo')
	print('A Program for Calculating Equilibrium Polarization States')
	print('and properties of Ferroelectric materials.')
	print('----------------------------------------------------------')
	print('')
	print('Copyright Notice (c) 2019 Jacob Zorn')
	print('Created at Pennsylvania State University')
	print('Version: 0.6.0')
	print('Last Updated: 08/14/2019')
	print('')
	print('----------------------------------------------------------')
	print('')
	print('Help and User Information: Can be found in the accompanying')
	print('README file and \u03BC-Thermo manual.')
	print('')
	print('----------------------------------------------------------')
	print('')
	print('This Software is Licensed via the XXXX License.')
	print('Information regarding this license can be found in ')
	print('the provided License.txt software provided with this')
	print('software or at XXX.Gitlab.XXX')
	print('')
	print('----------------------------------------------------------')
	print('')
	
def Print_Help_Statement():
	print('')
	print('Help for Thermodynamics of Ferroelectrics')
	print('----------------------------------------------------')
	print('')
	print('This program can be utilized to calculate a number')
	print('of properties related to ferroelectrics and their ')
	print('thermodynamics. A list of flags and options are can')
	print('be seen below. An example usage is also presented.')
	print('')
	print('----------------------------------------------------')
	print('')
	print('Note: All Agruments can be written as CAPS, smalls,')
	print('or mIxEd letters. Any combination can be read in.')
	print('')
	print('Main Agruments:')
	print('')
	print(' -Thin 	 := Do Calculations for a Thin Film System')
	print(' -Bulk 	 := Do Calculations for a Bulk System')
	print(' -Pot	 := Specifies the thermodynamic potential ')
	print('          you want to utilize for the calculations.')
	print('          The available potentials are listed below.')
	print(' -Study  := Specifying what study you want to run')
	print('           calculations for. Available studies are')
	print('           shown below.')
	print(' -Help or -h := Print this help statement.')
	print('')
	print('----------------------------------------------------')
	print('')
	print('Optional Agruments:')
	print('')
	print(' -Misfit   := Specify the misfit strain of the thin')
	print('            film system. Default = [0.00,0.00,0.00]')
	print(' -Stress   := Specify the Applied Stress of the bulk')
	print('            system. Default = [0,0,0,0,0,0]. Units=GPa')
	print(' -Temp     := Specify the temperature of the system.')
	print('            defaults to 298 K.')
	print(' -Misfit_h := Upper limits of the Misfit Strain of ')
	print('             the misfit strain. Can be Anisotropic. ')
	print('             Used for parameteric studies.')
	print(' -Misfit_l := Lower limits of the Misfit Strain of ')
	print('             the misfit strain. Can be Anisotropic. ')
	print('             Used for parameteric studies.')
	print(' -Temp_h   := Upper limit of the System Temperature.')
	print(' -Temp_l   := Lower limit of the System Temperature.')
	print(' -Stress_h := Upper limit of the Applied Stress of the')
	print('            bulk system. Cannot be Anisotropic.')
	print(' -Stress_l := Lower limit of the Applied Stress of the')
	print('            bulk system. Cannot be Anisotropic.')
	print(' -XF       := Fraction for PZT, KNN, and other systems.')
	print(' -Rotate := Rotate the Given System, must supply a')
	print('          list of Euler Angles. Consult ReadMe and')
	print('          other documentation for more information')
	print('')
	print('----------------------------------------------------')
	print('')
	print('Argument Values:')
	print('')
	print('Study Types:')
	print('--------------')
	print('')
	print(' Dielec   := Output the Dielectric Tensor of the System.')
	print(' Piezo    := Output the Piezoelectric Tensor of the System.')
	print(' Total_Pol:= Output the Magnitude of the Polarization Vector.')
	print(' Equil    := Output the Equilibrium Polarization Vector.')
	print(' Mis_Mis  := Parameteric Study for Misfit-Misfit Strain')
	print('             diagram. Needs Misfit_l and Misfit_h flags')
	print(' Mis_Temp := Parameteric Study for Misfit Strain-Temperature')
	print('             diagram. Needs Misfit_l, Misfit_h, Temp_h, and')
	print('             Temp_l flags.')
	print(' K_Temp   := Parameteric Study for Dielectric Constant -')
	print('             Temperature Diagram. Need Temp_l and Temp_h flags.')
	print(' D_Temp   := Parameteric Study for Piezoelectric Coef - ')
	print('             Temperature Diagram. Need Temp_l and Temp_h flags.')
	print(' P_Temp   := Parameteric Study for Polarization - Temperature')
	print('             diagram. Need Temp_l and Temp_h flags.')
	print(' K_Mis    := Parameteric Study for Dielectric Constant - ')
	print('             Misfit Strain Diagram. Need Misfit_l and Misfit_h')
	print('             flags.**Yet To Be Implemented Fully**')
	print(' D_Mis    := Parameteric Study for Piezoelectric Coef. - ')
	print('             Misfit Strain Diagram. Need Misfit_l and Misfit_h')
	print('             flags.**Yet To Be Implemented Fully**')
	print(' P_Mis    := Parameteric Study for Polarization Vec. - ')
	print('             Misfit Strain Diagram. Need Misfit_l and Misfit_h')
	print('             flags.**Yet To Be Implemented Fully**')
	print(' K_Stress := Parameteric Study for Dielectric Constant - ')
	print('             Applied Stress Diagram. Need Stress_l and Stress_h')
	print('             flags.**Yet To Be Implemented Fully**')
	print(' D_Stress := Parameteric Study for Piezoelectric Coef. - ')
	print('             Applied Stress Diagram. Need Stress_l and Stress_h')
	print('             flags.**Yet To Be Implemented Fully**')
	print(' P_Stress := Parameteric Study for Polarization Vec. - ')
	print('             Applied Stress Diagram. Need Stress_l and Stress_h')
	print('             flags.**Yet To Be Implemented Fully**')
	print('')
	print('--------------')
	print('')
	print('Available Potentials:')
	print('--------------')
	print('')
	print(' BTO-05-Li        := Journal Ref.')
	print(' BTO-07-Wang      := Journal Ref.')
	print(' BTO-10-Wang      := Journal Ref.')
	print(' PTO-02-Li        := Journal Ref.')
	print(' BFO-12-Vasudevam := Journal Ref.')
	print(' BFO-14-Xue       := Journal Ref.')
	print(' STO-07-Li        := Journal Ref.')
	print(' STO-10-Sheng     := Journal Ref.')
	print(' KNN-17-Pohlmann  := Journal Ref.')
	print('             **Must use XF Flag**')
	print(' KNO-09-Liang     := Journal Ref.')
	print('')
	print('More Potentials Will Be Added as they become available.')
	print('--------------')
	print('')
	print('Example Usages:')
	print('--------------')
	print('')
	print('Example 1:')
	print('python Thermodynamics_Notebook.py -thin -study total_pol')
	print(' -misfit [0.01,0.01,0.0] -pot BTO-05-Li')
	print('Expected Outcome:')
	print('Polarization Vector Magnitude = 0.362005 or similiar.')
	print('')
	print('Example 2:')
	print('python Thermodynamics_Notebook.py -bulk -study total_pol')
	print(' -stress [1e9,1e9.1e9,0,0,0] -pot KNO-09-Liang')
	print('Expected Outcome:')
	print('Polarization Vector : [0.30375 0.3038 0.3037] or similiar')
	print('')
	print('--------------')
	print('')
	print('----------------------------------------------------')
	print('Ending Help Screen')
	
def Read_Arguments():
	
	#Determine the number of arguments
	
	#Default Values
	study_type	= 'equil'
	temp		= 298
	misfit		= [0,0,0]
	stress		= [0,0,0,0,0,0]
	system		= 'bulk'
	pot_type	= 'BTO-10-Wang'
	rot 		= 0
	XF 			= 0.50
	
	skip_flag = 0
	for i_arg in range(0,len(sys.argv)):
		if skip_flag > 0:
			skip_flag = skip_flag - 1
		else:
			j_arg = sys.argv[i_arg]
			if j_arg.lower() == '-thin' and skip_flag == 0:
				skip_flag = 0;	system = 'thin'
			if j_arg.lower() == '-bulk' and skip_flag == 0:
				skip_flag = 0;	system = 'bulk'
			if j_arg.lower() == '-rotate' and skip_flag == 0:
				rotate_list = eval(sys.argv[i_arg+1]);	skip_flag = 1; rot = 1
			if j_arg.lower() == '-study' and skip_flag == 0:
				study_type = sys.argv[i_arg+1].lower(); skip_flag = 1
			if j_arg.lower() == '-pot' and skip_flag == 0:
				pot_type = sys.argv[i_arg+1];	skip_flag = 1
			if j_arg.lower() == '-misfit' and skip_flag == 0:
				misfit = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-stress' and skip_flag == 0:
				stress = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-temp' and skip_flag == 0:
				temp = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-misfit_l' and skip_flag == 0:
				misfit_l = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-misfit_h' and skip_flag == 0:
				misfit_h = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-temp_l' and skip_flag == 0:
				temp_l = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-temp_h' and skip_flag == 0:
				temp_h = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-stress_l' and skip_flag == 0:
				stress_l = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-stress_h' and skip_flag == 0:
				stress_h = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-xf':
				XF = eval(sys.argv[i_arg+1]); skip_flag = 1
			if j_arg.lower() == '-h' and skip_flag == 0 or j_arg.lower() == '-help' and skip_flag == 0:
				Print_Help_Statement()
				quit()
				
	if study_type == 'equil':
		print('Starting Equilibrium Polarization Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				f, equil = (Another_Thin_Film_Rotation(temp,misfit,rotate_list,pot_type,XF,500))
			else:
				f, equil = (Thin_Film(temp, misfit, pot_type, XF, 500))
		if system == 'bulk':
			if rot == 1:
				f, equil = (Bulk_Rotation(temp, stress, pot_type, XF, rotate_list, 500))
			else:
				f, equil = (Bulk(temp,stress,pot_type, XF, 500))
		
		print('Polarization Vector : ', equil)
	elif study_type == 'total_pol':
		print('Starting Total Polarization Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				f, equil = (Another_Thin_Film_Rotation(temp,misfit,rotate_list,pot_type,XF,500))
			else:
				f, equil = (Thin_Film(temp, misfit, pot_type, XF, 500))
		if system == 'bulk':
			if rot == 1:
				f, equil = (Bulk_Rotation(temp, stress, pot_type, XF, rotate_list, 500))
			else:
				f, equil = (Bulk(temp,stress,pot_type, XF, 500))
				
		print('Polarization Vector Magnitude = ',round((equil[0]**2 + equil[1]**2 + equil[2]**2)**0.5,5),'.')
	elif study_type == 'piezo':
		print('Starting Piezoelectric Coef. Calculations for: ', pot_type,'.')
	elif study_type == 'dielec':
		print('Starting Dielectric Constant Calculations for: ', pot_type,'.')
	elif study_type == 'mis_mis':
		a_list 		= [int(misfit_l[0]*10000),int(misfit_h[0]*10000),int((misfit_h[0]*10000-misfit_l[0]*10000)/20)]
		b_list		= [int(misfit_l[1]*10000),int(misfit_h[1]*10000),round((misfit_h[1]*10000-misfit_l[1]*10000)/20)]
		
		print('Starting Misfit Strain - Misfit Strain Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				print('Not yet implemented')
				Misfit_Misfit_Rotation(temp,a_list,b_list,rotate_list,pot_type,XF,500)
			else:
				Misfit_v_Misfit(a_list, b_list, temp, pot_type, XF, 500)
		else:
			print('This System Type is not Compatible with Misfit Strain - Misfit Strain Calculations. The Program will now exit.')
	elif study_type == 'mis_temp':
		misfit_l 	= misfit_l * 10000
		misfit_h 	= misfit_h * 10000
		a_list 		= [misfit_l[0],misfit_h[0],(misfit_h[0]-misfit_l[0])/10]
		b_list		= [misfit_l[1],misfit_h[1],(misfit_h[1]-misfit_l[1])/10]
		Temp_List 	= [temp_l,temp_h,(temp_h-temp_l)/20]
		
		print('Starting Misfit Strain - Temperature Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				print('Not yet implemented')
				#f, equil = (Another_Thin_Film_Rotation(temp,misfit,rotate_list,pot_type,0.50,500))
			else:
				f, equil = Misfit_v_Temp(Temp_List,a_list, pot_type, XF, 500)
		else:
			print('This System Type is not Compatible with Misfit Strain - Temperature Calculations. The Program will now exit.')
	elif study_type == 'k_temp':
		print('Starting Dielectric Constant - Temperature Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
		if system == 'bulk':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
	elif study_type == 'd_temp':
		print('Starting Piezoelectric Coef. - Temperature Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
		if system == 'bulk':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
	elif study_type == 'p_temp':
		print('Starting Polarization - Temperature Calculations for: ', pot_type,'.')
		
		t_list = [temp_l,temp_h,int((temp_h-temp_l)/20)]
		
		if system == 'thin':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				Polarization_v_Temp(t_list,'Film', pot_type, misfit,XF,500)
		if system == 'bulk':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				Polarization_v_Temp(t_list,'Bulk', pot_type, stress,XF,500)
	elif study_type == 'k_mis':
		print('Starting Dielectric Constant - Misfit Strain Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
		if system == 'bulk':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
	elif study_type == 'd_mis':
		print('Starting Piezoelectric Coef. - Misfit Strain Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
		if system == 'bulk':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
	elif study_type == 'p_mis':
		print('Starting Polarization - Misfit Strain Calculations for: ', pot_type,'.')
		
		a_list 		= [int(misfit_l[0]*10000),int(misfit_h[0]*10000),int((misfit_h[0]*10000-misfit_l[0]*10000)/20)]
		
		if system == 'thin':
			if rot == 1:
				Polarization_v_Rotation_Misfit(temp,a_list,pot_type,rotate_list,XF,500)
			else:
				Polarization_v_Misfit(temp,a_list,pot_type,XF,500)
		if system == 'bulk':
			print('This system is incompatible with this study. Try using\
 the Polarization versus Stress tag.')
		
		print('Completed Misfit Versus Polarization Calculations.')
	elif study_type == 'k_stress':
		print('Starting Dielectric Constant - Stress Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
		if system == 'bulk':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
	elif study_type == 'd_stress':
		print('Starting Piezoelectric Coef. - Stress Calculations for: ', pot_type,'.')
		if system == 'thin':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
		if system == 'bulk':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				print('Not Yet Implemented')
	elif study_type == 'p_stress':
		print('Starting Polarization - Stress Calculations for: ', pot_type,'.')
		if system == 'thin':
			print('This system is incompatible with this calculations. Try using\
					the Polarization versus Misfit Tag.')
		if system == 'bulk':
			if rot == 1:
				print('Not Yet Implemented')
			else:
				a_list = [stress_l[0]*10,stress_h[0]*10,int((stress_h[0]-stress_l[0])/10)]
				Polarization_v_Stress(temp,a_list,pot_type,XF,500)
		
	else:
		print('Incorrect Study Flag was Called.')
		print('Program Ending.')

def Read_Input_File():
	
	file_handler = open('muInput.in')
	
	tag_list = ['system','temp','potential','fraction','study','misfit','stress','study_temp','study_misfit','study_stress','study_fraction','rotate','rotation','elecfield','output_picture','output_data','output_flag','study_misfit_x','study_misfit_y']
	values = ['0','0','0','0','0','[0,0,0,0,0,0]','[0,0,0,0,0,0]','[0,0,0]','[0,0,0]','[0,0,0]','[0,0,0]','0','[0,0,0]','[0,0,0]','PNG','TXT',3,'[0,0,0,0,0,0]','[0,0,0,0,0,0]']
	tag_dict = dict(zip(tag_list,values))
	
	for line in file_handler:
		current_line = line.replace('\n','')
		try:
			if current_line[0] == '#':
				skip_flag = 0
			else:
				arg = current_line.split(' ')
				for key in tag_dict.keys():
					if key == arg[0].lower():
						tag_dict[key] = (arg[2])
						break
		except:
			skip_flag = 0
	
	return tag_dict

def main():
	
	# Plot_2D_Contour('xy', 223, 'BTO-05-Li', [0,0,0,0,0,0], [0,0,0], 0, 'Bulk')
	# quit()
	
	# Thin_Film_PolyDomain(223+150, [-0.002,0.002,0.0], [0,0,0], 'BTO-05-Li', 0.0, 500)
	# quit()
	
	Intro_Message()
	
	tag_dict = Read_Input_File()
	if tag_dict['study'].find('fraction') >= 0:
		if tag_dict['potential'] != 'BZTO-18-Peng' or tag_dict['potential'] != 'PZT-88-Haun' or tag_dict['potential'] != 'KNN-17-Pohlmann' or tag_dict['potential'] != 'BCTO-17-Cao' or tag_dict['potential'] != 'BSTO-17-Cao':
			print('The provided potential does not include a material fraction, therefore the provided study can not be completed.')
			print('Please adjust the potential and/or study.')
			print('Quitting and shutting down software.')
	
	if tag_dict['study'].lower() == 'equilibrium':#complete
		if tag_dict['system'].lower() == 'bulk':
			print('Running Equilibrium Polarization Vector for a Bulk System.')
			print('Using the ',tag_dict['potential'], ' Potential.')
			f, polar = (Bulk(eval(tag_dict['temp']),eval(tag_dict['stress']),\
			eval(tag_dict['elecfield']),tag_dict['potential'],\
			eval(tag_dict['fraction']),500))
			print('Equilibrium Polarization vector is')
			print(polar.tolist())
			print(Domain_Qualifier(polar.tolist(),'Bulk'))
		if tag_dict['system'].lower() == 'film':
			print('Running Equilibrium Polarization Vector for a Thin Film System.')
			print('Using the ',tag_dict['potential'], ' Potential.')
			if eval(tag_dict['rotate']) == 1:
				f, polar, prob = Thin_Film(eval(tag_dict['temp']), eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotation']),tag_dict['potential'],eval(tag_dict['fraction']),500)
			else:
				f, polar, prob = Thin_Film(eval(tag_dict['temp']), eval(tag_dict['misfit']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['fraction']),500)
			print('Equilibrium Polarization vector is')
			print(polar)
			print(Domain_Qualifier(polar,'film'))
	elif tag_dict['study'].lower() == 'total_polarization':#complete
		if tag_dict['system'].lower() == 'bulk':
			print('Running Polarization Vector Magnitude for a Bulk System.')
			print('Using the ',tag_dict['potential'], ' Potential.')
			f, polar = (Bulk(eval(tag_dict['temp']),eval(tag_dict['stress']),\
			eval(tag_dict['elecfield']),tag_dict['potential'],\
			eval(tag_dict['fraction']),500))
			print('Polarization Magnitude is:')
			print((polar[0]**2 + polar[1]**2 + polar[2]**2)**0.5)
		if tag_dict['system'].lower() == 'film':
			print('Running Polarization Vector Magnitude for a Thin Film System.')
			print('Using the ',tag_dict['potential'], ' Potential.')
			if eval(tag_dict['rotate']) == 1:
				f, polar, prob = Thin_Film(eval(tag_dict['temp']), eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotation']),tag_dict['potential'],eval(tag_dict['fraction']),500)
			else:
				f, polar, prob = Thin_Film(eval(tag_dict['temp']), eval(tag_dict['misfit']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['fraction']),500)
			print('Polarization Magnitude is:')
			print((polar[0]**2 + polar[1]**2 + polar[2]**2)**0.5)
	elif tag_dict['study'].lower() == 'dielectric':#complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Dielectric(eval(tag_dict['temp']),eval(tag_dict['stress']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['fraction']),500,1)	
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Dielectric(eval(tag_dict['temp']),eval(tag_dict['misfit']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500,1)
	elif tag_dict['study'].lower() == 'piezoelectric':#complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Piezoelectric_Tensor(eval(tag_dict['temp']),eval(tag_dict['stress']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['fraction']),500,1)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Piezoelectric_Tensor(eval(tag_dict['temp']),eval(tag_dict['misfit']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500,1)
	elif tag_dict['study'].lower() == 'misfit_temp': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Misfit_Temperature(eval(tag_dict['study_misfit']),eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['fraction']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'misfit_misfit': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Misfit_Misfit_Phase_Map(eval(tag_dict['study_misfit_x']),eval(tag_dict['study_misfit_y']),eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['fraction']),500)
	elif tag_dict['study'].lower() == 'fraction_temperature': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_XF_v_Temperature_Phase_Map(eval(tag_dict['study_fraction']),eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['stress']),eval(tag_dict['elecfield']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_XF_v_Temperature_Phase_Map(eval(tag_dict['study_fraction']),eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['misfit']),eval(tag_dict['elecfield']),500)
	elif tag_dict['study'].lower() == 'dielectric_temp': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Dielectric_Temp(eval(tag_dict['study_temp']),eval(tag_dict['stress']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['fraction']),500,0)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Dielectric_Temp(eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500)
	elif tag_dict['study'].lower() == 'piezoelectric_temp': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Piezoelectric_Temp(eval(tag_dict['study_temp']),eval(tag_dict['stress']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['fraction']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Piezoelectric_Temp(eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500)
	elif tag_dict['study'].lower() == 'dielectric_misfit': #complete
		if tag_dict['system'].lower() == 'bulk':
			print('This System is not compatiable with these study')
		if tag_dict['system'].lower() == 'film':
			Dielectric_v_Misfit(eval(tag_dict['study_misfit']),eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500)
	elif tag_dict['study'].lower() == 'piezoelectric_misfit':#complete
		if tag_dict['system'].lower() == 'bulk':
			print('This System is not compatiable with these study')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Piezoelectric_v_Misfit(eval(tag_dict['study_misfit']),eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500,0)
	elif tag_dict['study'].lower() == 'dielectric_stress': #complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Dielectric_Stress(eval(tag_dict['study_stress']),tag_dict['potential'],eval(tag_dict['temp']),eval(tag_dict['elecfield']),eval(tag_dict['fraction']),500)
		if tag_dict['system'].lower() == 'film':
			print('This study is incompatible with the choice of the system.')
	elif tag_dict['study'].lower() == 'piezoelectric_stress': #complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Piezoelectric_Stress(eval(tag_dict['study_stress']),tag_dict['potential'],eval(tag_dict['temp']),eval(tag_dict['elecfield']),eval(tag_dict['fraction']),500)
		if tag_dict['system'].lower() == 'film':
			print('This study is incompatible with the choice of the system.')
	elif tag_dict['study'].lower() == 'dielectric_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Dielectric_Fraction(eval(tag_dict['temp']),eval(tag_dict['stress']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['study_fraction']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Dielectric_Fraction(eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['study_fraction']),500)
	elif tag_dict['study'].lower() == 'piezoelectric_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Piezoelectric_Fraction(eval(tag_dict['temp']),eval(tag_dict['stress']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['study_fraction']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Piezoelectric_Fraction(eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['study_fraction']),500)
	elif tag_dict['study'].lower() == 'dielectric_misfit_temp': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Dielectric_Misfit_Temp(eval(tag_dict['study_temp']),eval(tag_dict['study_misfit']),tag_dict['potential'],eval(tag_dict['fraction']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'dielectric_misfit_misfit': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Dielectric_Misfit_Misfit(eval(tag_dict['study_misfit_x']),eval(tag_dict['study_misfit_y']),eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['fraction']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'piezoelectric_misfit_temp': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Piezoelectric_Misfit_Temp(eval(tag_dict['study_temp']),eval(tag_dict['study_misfit']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500)
	elif tag_dict['study'].lower() == 'piezoelectric_misfit_misfit': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Piezoelectric_Misfit_Misfit(eval(tag_dict['study_misfit_x']),eval(tag_dict['study_misfit_y']),eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['fraction']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'piezoelectric_misfit_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Piezoelectric_Misfit_Fraction(eval(tag_dict['study_fraction']),eval(tag_dict['study_misfit']),tag_dict['potential'],eval(tag_dict['temp']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'piezoelectric_temp_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Piezoelectric_Temperature_Fraction(eval(tag_dict['study_fraction']),eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['stress']),eval(tag_dict['elecfield']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Piezoelectric_Fraction_Temp(eval(tag_dict['study_temp']),eval(tag_dict['study_fraction']),eval(tag_dict['misfit']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'piezoelectric_stress_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Piezoelectric_Stress_Fraction(eval(tag_dict['temp']),eval(tag_dict['study_fraction']),eval(tag_dict['study_stress']),tag_dict['potential'],eval(tag_dict['elecfield']),500)
		if tag_dict['system'].lower() == 'film':
			print('This study is incompatible with the choice of the system.')
	elif tag_dict['study'].lower() == 'dielectric_misfit_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Dielectric_Misfit_Fraction(eval(tag_dict['study_fraction']),eval(tag_dict['study_misfit']),tag_dict['potential'],eval(tag_dict['temp']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'dielectric_temp_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Dielectric_Temp_Fraction(eval(tag_dict['study_temp']),eval(tag_dict['study_fraction']),eval(tag_dict['stress']),tag_dict['potential'],eval(tag_dict['elecfield']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Dielectric_Fraction_Temp(eval(tag_dict['study_fraction']),eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'dielectric_stress_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Dielectric_Stress_Fraction(eval(tag_dict['temp']),eval(tag_dict['study_fraction']),eval(tag_dict['study_stress']),tag_dict['potential'],eval(tag_dict['elecfield']),500)
		if tag_dict['system'].lower() == 'film':
			print('This study is incompatible with the choice of the system.')
	elif tag_dict['study'].lower() == 'polarization_temp': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Polarization_Temp(eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['stress']),eval(tag_dict['elecfield']),eval(tag_dict['fraction']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Polarization_Temp(eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500)
	elif tag_dict['study'].lower() == 'polarization_misfit': #complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Polarization_v_Misfit(eval(tag_dict['study_misfit']),eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500)
	elif tag_dict['study'].lower() == 'polarization_stress': #complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Polarization_Stress(eval(tag_dict['study_stress']),tag_dict['potential'],eval(tag_dict['temp']),eval(tag_dict['elecfield']),eval(tag_dict['fraction']),500)
		if tag_dict['system'].lower() == 'film':
			print('This study is incompatible with the choice of the system.')
	elif tag_dict['study'].lower() == 'polarization_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Polarization_Fraction(eval(tag_dict['temp']),eval(tag_dict['stress']),eval(tag_dict['elecfield']),tag_dict['potential'],eval(tag_dict['study_fraction']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Polarization_Fraction(eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['misfit']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['study_fraction']),500)
	elif tag_dict['study'].lower() == 'polarization_misfit_temp': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			#Need to include the plotting function in this function yet.
			Thin_Film_Polarization_Temp_Misfit(eval(tag_dict['study_temp']),eval(tag_dict['study_misfit']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),eval(tag_dict['fraction']),500)
	elif tag_dict['study'].lower() == 'polarization_misfit_misfit': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Polarization_Misfit_Misfit(eval(tag_dict['study_misfit_x']),eval(tag_dict['study_misfit_y']),eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['fraction']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'polarization_misfit_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			print('This study is incompatible with the choice of the system.')
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Polarization_Misfit_Fraction(eval(tag_dict['study_fraction']),eval(tag_dict['study_misfit']),tag_dict['potential'],eval(tag_dict['temp']),eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'polarization_stress_temp': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Polarization_Stress_Temperature(eval(tag_dict['study_temp']),eval(tag_dict['study_stress']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['fraction']),500)
		if tag_dict['system'].lower() == 'film':
			print('This study is incompatible with the choice of the system.')
	elif tag_dict['study'].lower() == 'polarization_stress_fraction': #Complete
		if tag_dict['system'].lower() == 'bulk':
			Bulk_Polarization_Stress_Fraction(eval(tag_dict['temp']),eval(tag_dict['study_fraction']),eval(tag_dict['study_stress']),tag_dict['potential'],eval(tag_dict['elecfield']),500)
		if tag_dict['system'].lower() == 'film':
			print('This study is incompatible with the choice of the system.')
	elif tag_dict['study'].lower() == 'polarization_fraction_temp': #Complete
		if tag_dict['system'].lower() == 'bulk': 
			Bulk_Polarization_Temperature_Fraction(eval(tag_dict['study_fraction']),eval(tag_dict['study_temp']),tag_dict['potential'],eval(tag_dict['stress']),eval(tag_dict['elecfield']),500)
		if tag_dict['system'].lower() == 'film':
			Thin_Film_Polarization_Temperature_Fraction(eval(tag_dict['study_temp']),eval(tag_dict['study_fraction']),eval(tag_dict['misfit']),tag_dict['potential'],eval(tag_dict['elecfield']),eval(tag_dict['rotate']),eval(tag_dict['rotation']),500)
	elif tag_dict['study'].lower() == 'energy_surface': #Complete
		if tag_dict['system'].lower() == 'bulk':
			# Plot_2D_Contour('xz',eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['stress']),eval(tag_dict['elecfield']),eval(tag_dict['fraction']),'Bulk')
			# Plot_2D_Contour('yz',eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['stress']),eval(tag_dict['elecfield']),eval(tag_dict['fraction']),'Bulk')
			# Plot_2D_Contour('xy',eval(tag_dict['temp']),tag_dict['potential'],eval(tag_dict['stress']),eval(tag_dict['elecfield']),eval(tag_dict['fraction']),'Bulk')
			
			Energy_Surface(tag_dict['potential'],eval(tag_dict['temp']),eval(tag_dict['stress']),eval(tag_dict['elecfield']),eval(tag_dict['fraction']))
		elif tag_dict['system'].lower() == 'film':
			print('Not Yet Implemented for thin film systems')
		else:
			print('Unrecognized System Temp. Please Check Input File.')
	else:
		print('Not a recoginized study tag. Please fix and try again')
		print('Program quitting')
	
def Plot_2D_Contour(Plane, Temp, Potential, Misfit, Elec_Field, XF, System):

	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Symbols()
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, Misfit,XF)
	
	Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
	
	if System == 'Bulk':
		Prob = Prob.subs({s1:Misfit[0],s2:Misfit[1],s3:Misfit[2],s4:Misfit[3],s5:Misfit[4],s6:Misfit[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	else:
		Prob = Prob.subs({s3:0,s4:0,s5:0,e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
		
		Prob += Misfit[0] * s1 + Misfit[1] * s2 + Misfit[2] * s6
		
		solutions = (sp.solve([Prob.diff(s1)+Misfit[0],Prob.diff(s2)+Misfit[1],Prob.diff(s6)+Misfit[2]],[s1,s2,s6]))
		
		Prob = Prob.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6]})
	
	newProb = sp.lambdify([p1,p2,p3],Prob,'numpy')
	
	xlist = np.linspace(-p0*1.1,p0*1.1,100)
	ylist = np.linspace(-p0*1.1,p0*1.1,100)
	x,y = np.meshgrid(xlist,ylist)
	
	if Plane.lower() == 'xz':
		z = newProb(x,0,y)
		#plt.contourf(x,y,z,30,cmap='viridis')
		plt.pcolor(x,y,z,cmap='rainbow')
		plt.xlabel(r'P$_1$ C/m$^2$')
		plt.ylabel(r'P$_3$ C/m$^2$')
		cbar = plt.colorbar()
		cbar.set_label('Free Energy J/m$^3$')
		plt.savefig(Potential + 'X-Z_Plane.png')
		plt.show()
	elif Plane.lower() == 'yz':
		z = newProb(0,x,y)
		plt.contourf(x,y,z,30,cmap='rainbow')
		plt.xlabel(r'P$_2$ C/m$^2$')
		plt.ylabel(r'P$_3$ C/m$^2$')
		cbar = plt.colorbar()
		cbar.set_label('Free Energy J/m$^3$')
		plt.savefig(Potential + 'Y-Z_Plane.png')
		plt.show()
	elif Plane.lower() == 'xy':
		z = newProb(x,y,0)
		#plt.contourf(x,y,z,30,cmap='hot')
		fig = plt.figure()
		ax = fig.gca(projection='3d')
		surf=ax.plot_surface(x,y,z,cmap=cm.viridis,linewidth=0)
		ax.set_zlim(z.min().min(),0)
		ax.set_axis_off()
		# plt.xlabel(r'P$_1$ C/m$^2$')
		# plt.ylabel(r'P$_2$ C/m$^2$')
		# cbar = plt.colorbar()
		# cbar.set_label('Free Energy J/m$^3$')
		# plt.savefig(Potential + 'X-Y_Plane.png')
		plt.show()
	
def Solver_Polarization(Prob,PolarSet,Iterations):

	p0 			= PolarSet
	#bounds		= [(1e-10,p0*1.0),(1e-10,p0*1.0),(1e-10,p0*1.0)]
	#bounds 	= [(-1*p0,1*p0),(-1*p0,1*p0),(-1*p0,1*p0)]
	bounds 		= [(1e-10,1),(1e-10,1),(1e-10,1)]
	#bounds 	= [(0,2),(0,2),(0,2)]
	popsize 	= 20
	iters		= Iterations
	crossp 		= 0.8
	mut 		= 0.6
	
	best_ener 	= 0
	
	ener_list		= []
	polar_list		= []
	compare_list	= []
	
	for DE_Iter in range(10):
		
		start = time.time()
	
		pop = np.random.rand(popsize,len(bounds))
		min_b,max_b = np.asarray(bounds).T
		diff = np.fabs(min_b - max_b)
		pop_denorm = min_b + pop * diff
		fitness = np.asarray([Prob(ind[0],ind[1],ind[2]) for ind in pop_denorm])
		best_idx = np.argmin(fitness)
		best = pop_denorm[best_idx]
		
		for i in range(iters):
			for j in range(popsize):
				idxs = [idx for idx in range(popsize) if idx != j]
				a,b,c = pop[np.random.choice(idxs,3,replace=False)]
				mutant = np.clip(a + mut * (b-c), 0, 1)
				cross_points = np.random.rand(len(bounds)) < crossp
				if not np.any(cross_points):
					cross_points[np.random.randint(0,len(bounds))] = True
				trial = np.where(cross_points, mutant, pop[j])
				trial_denorm = min_b + trial * diff
				f = Prob(trial_denorm[0],trial_denorm[1],trial_denorm[2])
				if f < fitness[j]:
					fitness[j] = f
					pop[j] = trial
					if f < fitness[best_idx]:
						best_idx = j
						best = trial_denorm
						best_ener = f
			#print(i, best_ener)	
		equil_polar = best
		
		end_time = time.time()
		
		if DE_Iter == 0:
			ener_list.append(best_ener)
			polar_list.append(best)
		if DE_Iter != 0:
			diff = abs((best_ener - ener_list[DE_Iter-1]) / ener_list[DE_Iter-1])
			if diff < 0.0001: #less than 0.1%
				break
			else:
				ener_list.append(best_ener)
				polar_list.append(best)

	return best_ener, equil_polar

def Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3):

	#Still need to implement the electric field energy
	
	a1 		= a_values[0];	a11 	= a_values[1]
	a12 	= a_values[2];	a111	= a_values[3]
	a112	= a_values[4];	a123	= a_values[5]
	a1111	= a_values[6];	a1112	= a_values[7]
	a1122	= a_values[8];	a1123	= a_values[9]
	
	s11		= s_values[0];	s12		= s_values[1];	s44		= s_values[2]
	
	q11		= q_values[0];	q12		= q_values[1];	q44		= q_values[2]
	
	Land = a1 * (p1**2 + p2**2 + p3**2) + a12*(p1**2 * p2**2 + p1**2 * p3**2 + p2**2 * p3**2)
	Land += a11 * (p1**4 + p2**4 + p3**4) + a111 * (p1**6 + p2**6 + p3**6)
	Land += a112 * (p1**4 * (p2**2 + p3**2) + p2**4 * (p1**2 + p3**2) + p3**4 * (p2**2 + p1**2))
	Land += a123 * (p1**2 * p2**2 * p3**2) + a1111 * (p1**8 + p2**8 + p3**8)
	Land += a1112 * (p1**6 * (p2**2 + p3**2) + p2**6 * (p1**2 + p3**2) + p3**6 * (p2**2 + p1**2))
	Land += a1122 * (p1**4 * p2**4 + p1**4 * p3**4 + p2**4 * p3**4)
	Land += a1123 * (p1**4 * p2**2 * p3**2 + p1**2 * p2**4 * p3**2 + p1**2 * p2**2 * p3**4)

	Elas  = 0.5 * s11 * (s1**2 + s2**2 + s3**2) + s12 * (s1*s2 + s1*s3 + s2*s3)
	Elas += 0.5 * s44 * (s4**2 + s5**2 + s6**2)
	
	QEn  =  q11 * (s1*p1**2 + s2*p2**2 + s3*p3**2) + q12 * (s1*(p2**2 + p3**2) + s3*(p1**2 + p2**2) + s2*(p1**2 +p3**2))
	QEn += q44 * (p2*p3*s4 + p1*p3*s5 + p2*p1*s6)
	
	Elec = e1*p1 + e2*p2 + e3*p3
	
	GTot = Land - Elas - QEn - Elec
	
	return GTot

def Setup_Helmholtz(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3):

	a1 		= a_values[0];	a11 	= a_values[1]
	a12 	= a_values[2];	a111	= a_values[3]
	a112	= a_values[4];	a123	= a_values[5]
	a1111	= a_values[6];	a1112	= a_values[7]
	a1122	= a_values[8];	a1123	= a_values[9]
	
	s11		= s_values[0];	s12		= s_values[1];	s44		= s_values[2]
	
	q11		= q_values[0];	q12		= q_values[1];	q44		= q_values[2]
	
	Land = a1 * (p1**2 + p2**2 + p3**2) + a12*(p1**2 * p2**2 + p1**2 * p3**2 + p2**2 * p3**2)
	Land += a11 * (p1**4 + p2**4 + p3**4) + a111 * (p1**6 + p2**6 + p3**6)
	Land += a112 * (p1**4 * (p2**2 + p3**2) + p2**4 * (p1**2 + p3**2) + p3**4 * (p2**2 + p1**2))
	Land += a123 * (p1**2 * p2**2 * p3**2) + a1111 * (p1**8 + p2**8 + p3**8)
	Land += a1112 * (p1**6 * (p2**2 + p3**2) + p2**6 * (p1**2 + p3**2) + p3**6 * (p2**2 + p1**2))
	Land += a1122 * (p1**4 * p2**4 + p1**4 * p3**4 + p2**4 * p3**4)
	Land += a1123 * (p1**4 * p2**2 * p3**2 + p1**2 * p2**4 * p3**2 + p1**2 * p2**2 * p3**4)

	Elas  = 0.5 * s11 * (s1**2 + s2**2 + s3**2) + s12 * (s1*s2 + s1*s3 + s2*s3)
	Elas += 0.5 * s44 * (s4**2 + s5**2 + s6**2)
	
	QEn  =  q11 * (s1*p1**2 + s2*p2**2 + s3*p3**2) + q12 * (s1*(p2**2 + p3**2) + s3*(p1**2 + p2**2) + s2*(p1**2 +p3**2))
	QEn += q44 * (p2*p3*s4 + p1*p3*s5 + p2*p1*s6)
	
	Elec = e1*p1 + e2*p2 + e3*p3
	
	Land = Land
	
	GTot = Land + Elas
	
	return GTot

def Setup_Landau(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3):

	a1 		= a_values[0];	a11 	= a_values[1]
	a12 	= a_values[2];	a111	= a_values[3]
	a112	= a_values[4];	a123	= a_values[5]
	a1111	= a_values[6];	a1112	= a_values[7]
	a1122	= a_values[8];	a1123	= a_values[9]
	
	s11		= s_values[0];	s12		= s_values[1];	s44		= s_values[2]
	
	q11		= q_values[0];	q12		= q_values[1];	q44		= q_values[2]
	
	Land = a1 * (p1**2 + p2**2 + p3**2) + a12*(p1**2 * p2**2 + p1**2 * p3**2 + p2**2 * p3**2)
	Land += a11 * (p1**4 + p2**4 + p3**4) + a111 * (p1**6 + p2**6 + p3**6)
	Land += a112 * (p1**4 * (p2**2 + p3**2) + p2**4 * (p1**2 + p3**2) + p3**4 * (p2**2 + p1**2))
	Land += a123 * (p1**2 * p2**2 * p3**2) + a1111 * (p1**8 + p2**8 + p3**8)
	Land += a1112 * (p1**6 * (p2**2 + p3**2) + p2**6 * (p1**2 + p3**2) + p3**6 * (p2**2 + p1**2))
	Land += a1122 * (p1**4 * p2**4 + p1**4 * p3**4 + p2**4 * p3**4)
	Land += a1123 * (p1**4 * p2**2 * p3**2 + p1**2 * p2**4 * p3**2 + p1**2 * p2**2 * p3**4)

	Elas  = 0.5 * s11 * (s1**2 + s2**2 + s3**2) + s12 * (s1*s2 + s1*s3 + s2*s3)
	Elas += 0.5 * s44 * (s4**2 + s5**2 + s6**2)
	
	QEn  =  q11 * (s1*p1**2 + s2*p2**2 + s3*p3**2) + q12 * (s1*(p2**2 + p3**2) + s3*(p1**2 + p2**2) + s2*(p1**2 +p3**2))
	QEn += q44 * (p2*p3*s4 + p1*p3*s5 + p2*p1*s6)
	
	Elec = e1*p1 + e2*p2 + e3*p3
	
	Land = Land
	
	GTot = Land - Elas - QEn #+ Elec
	
	return Land, Elas, QEn, Elec

def Energy_Surface(Potential, Temp, App_Stress, Elec_Field,XF):
	
	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Symbols()
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, App_Stress,XF)
	
	Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
	
	Prob = Prob.subs({s1:App_Stress[0],s2:App_Stress[1],s3:App_Stress[2],s4:App_Stress[3],s5:App_Stress[4],s6:App_Stress[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	
	norm_ener, equil_polar = Bulk(Temp, App_Stress, Elec_Field, Potential, XF, 500)
	
	#norm_ener is the minimized energy, therefore it is the zero point energy
	#equil_polar are the equilibrium polarization vector
	#r = (max(equil_polar))
	r = (equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5
	#r = 4 * p0
	newProb = sp.lambdify([p1,p2,p3],Prob,'numpy')
	
	n = 200
	m = int(n/2)
	
	phi = np.linspace(0,np.pi,m)
	theta = np.linspace(0,2*np.pi,n)
	
	EE = np.zeros((m,n))
	xei = np.zeros((m,n))
	yei = np.zeros((m,n))
	zei = np.zeros((m,n))
	
	for i in range(n):
		for j in range(m):
			x = r * np.sin(phi[j]) * np.cos(theta[i])
			y = r * np.sin(phi[j]) * np.sin(theta[i])
			z = r * np.cos(phi[j])
			EE[j,i] = newProb(x,y,z)
			
	EE = EE + 2*abs(norm_ener) 
	# EE = EE / EE.max().max() + 1
	# EE = EE * 4.0
	
	for i in range(n):
		for j in range(m):
			xei[j,i] = EE[j,i] * np.sin(phi[j]) * np.cos(theta[i])
			yei[j,i] = EE[j,i] * np.sin(phi[j]) * np.sin(theta[i])
			zei[j,i] = EE[j,i] * np.cos(phi[j])
			
	x_2d = np.linspace(-1.5*p0,1.5*p0,100)
	y_2d = np.linspace(-1.5*p0,1.5*p0,100)
	p2_x = np.linspace(-1,1,100);	p2_y = np.linspace(-1,1,100)
	p2_x,p2_y = np.meshgrid(p2_x,p2_y)
	d2_x,d2_y = np.meshgrid(x_2d,y_2d)
	zz = newProb(d2_x,d2_y,0)
	zy = newProb(d2_x,0,d2_y)
	zx = newProb(0,d2_x,d2_y)
	print(zz.min().min(),zz.max().max())
	#zz = 2 * ((zz - zz.min().min())/(zz.max().max() - zz.min().min())) - 1
	#zz = np.exp(zz) / ((np.exp(zz) + 1))
	plt.contourf(x_2d,y_2d,zz,30,cmap='jet_r')
	norm_zz = matplotlib.colors.Normalize(vmin=zz.min().min(),vmax=zz.min().min())
	m = cm.ScalarMappable(cmap=plt.cm.rainbow,norm=norm_zz)
	m.set_array([])
	plt.colorbar(m, fraction=0.025,pad=0.08)
	plt.show()
	quit()
	
	#Normalize xei, yei, zei
	xei_min = xei.min().min();	xei_max = xei.max().max()
	yei_min = yei.min().min();	yei_max = yei.max().max()
	zei_min = zei.min().min();	zei_max = zei.max().max()
	xei = 2 * ((xei - xei_min)/(xei_max - xei_min)) - 1
	yei = 2 * ((yei - yei_min)/(yei_max - yei_min)) - 1
	zei = 2 * ((zei - zei_min)/(zei_max - zei_min)) - 1
	xei_min = xei.min().min();	xei_max = xei.max().max()
	yei_min = yei.min().min();	yei_max = yei.max().max()
	zei_min = zei.min().min();	zei_max = zei.max().max()
	
	fig = plt.figure(figsize=(8,8))
	ax = fig.add_subplot(111,projection='3d')
	
	# ax.contourf(p2_x, p2_y, zz, zdir='z',cmap='rainbow',offset=zei_min+0.01)
	# ax.contour(p2_x,p2_y,zy,zdir='y',cmap='rainbow',offset=yei_max-0.01)
	# ax.contour(p2_x,p2_y,zx,zdir='x',cmap='rainbow',offset=xei_min+0.01)
	
	norm_surf = matplotlib.colors.Normalize(vmin=EE.min().min(),vmax=EE.max().max())
	norm_zz = matplotlib.colors.Normalize(vmin=zz.min().min(),vmax=zz.min().min())
	norm_zy = matplotlib.colors.Normalize(vmin=zy.min().min(),vmax=zy.min().min())
	norm_zx = matplotlib.colors.Normalize(vmin=zx.min().min(),vmax=zx.min().min())
	
	
	
	ax.plot_surface(xei,yei,zei,facecolors=plt.cm.rainbow(norm_surf(EE)),rstride=1, cstride=1, linewidth=0, antialiased=False)
	ax.set_xlim(xei_min,xei_max);	ax.set_ylim(yei_min,yei_max);	ax.set_zlim(zei_min,zei_max)
	ax.set_xlabel('X');		ax.set_ylabel('Y');		ax.set_zlabel('Z')
	#ax.contourf(p2_x, p2_y, zz,30, zdir='z',cmap='rainbow',offset=zei_min)
	ax.contourf(p2_x,p2_y,zz,50,zdir='z',cmap='rainbow',vmin=zz.min().min(),vmax=zz.max().max(),offset=zei_min)
	# ax.contourf(p2_x,zy,p2_y,30,zdir='y',cmap=plt.cm.rainbow(norm_zy(zy)),offset=yei_max)
	# ax.contourf(zx,p2_x,p2_y,30,zdir='x',cmap=plt.cm.rainbow(norm_zx(zx)),offset=xei_min)
	m = cm.ScalarMappable(cmap=plt.cm.rainbow,norm=norm_surf)
	m.set_array([])
	#plt.axis('off')
	cbar = plt.colorbar(m,ticks=[EE.min().min(),EE.max().max()], fraction=0.025,pad=0.08)
	cbar.ax.set_yticklabels(['Low','High'])
	ax.set_aspect('equal','box')
	fig.tight_layout()
	plt.savefig(str(Temp) + '.' + Potential + '.png')
	plt.show()
		
def Setup_Symbols():
	
	T  = sp.Symbol('Temp')
	p1 = sp.Symbol('p1')
	p2 = sp.Symbol('p2')
	p3 = sp.Symbol('p3')
	s1 = sp.Symbol('Sig1')
	s2 = sp.Symbol('Sig2')
	s3 = sp.Symbol('Sig3')
	s4 = sp.Symbol('Sig4')
	s5 = sp.Symbol('Sig5')
	s6 = sp.Symbol('Sig6')
	e1 = sp.Symbol('E1')
	e2 = sp.Symbol('E2')
	e3 = sp.Symbol('E3')
	
	return p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3

def Potential_Choice(Potential, T, App_Stress, XF):
	# Selects the necessary potential and the parameters that was called.

	if   Potential == 'BTO-10-Wang':
		Curie_Temp = 390
		a0 = abs(5*1e5*160*(np.cosh(160/300)/np.sinh(160/300) - np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)))
		a1 = 5*1e5*160*(np.cosh(160/T)/np.sinh(160/T) - np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp))
		a11 = -1.154 * 1e8 * (1 + 0.037*(App_Stress[0]*3/1e9))
		a12 = 6.530 * 1e8 * (1 + 0.037*(App_Stress[0]*3/1e9))
		a111 = -2.106 * 1e9 * (1 + 0.023*(App_Stress[0]*3/1e9))
		a112 = 4.091 * 1e9 * (1 + 0.023*(App_Stress[0]*3/1e9))
		a123 = -6.688 * 1e9 * (1 + 0.023*(App_Stress[0]*3/1e9))
		a1111 = 75900000000
		a1112 = -21930000000
		a1122 = -22210000000
		a1123 = 24160000000
		q11 = 0.11
		q12 = -0.045
		q44 = 0.029
		c11 = 177940000000
		c12 = 96350000000
		c44 = 121990000000
		p0 = 0.26
	elif Potential == 'BTO-Test':
		Curie_Temp = 110+273
		a0 = abs(3.3e5 * (T - Curie_Temp))
		a1 = 3.3e5 * (T - Curie_Temp)
		a11 = 3.6e6 * (T - 175+273)
		a12 = 4.9e8
		a111 = 6.6e9
		a112 = 2.9e9
		a123 = 7.6e7 * (T - 120+273) + 4.4e10
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.11
		q12 = -0.043
		q44 = 0.059
		s11 = 8.3e-12
		s12 = -2.7e-12
		s44 = 9.24e-12
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		p0 = 0.26
	elif Potential == 'BTO-84-Bell':
		Curie_Temp = 381
		a0 = (abs(3.34e5 * (T- Curie_Temp)))
		a1 = 3.34e5 * (T-Curie_Temp)
		a11 = 4.69e6 * (T-393) - 2.02e8
		a12 = 3.230e8
		a111 = -5.52e7 * (T-393) + 2.76e9
		a112 = 4.470e9
		a123 = 4.910e9
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.1
		q12 = -0.034
		q44 = 0.029
		c11 = 1.78e11
		c12 = 0.964e11
		c44 = 1.22e11
		p0 = 0.26
	elif Potential == 'BTO-01-Bell':
		Curie_Temp = 381
		a0 = (abs(3.34e5 * (T- Curie_Temp)))
		a1 = 3.34e5 * (T-Curie_Temp)
		a11 = 4.69e6 * (T-393) - 2.02e8
		a12 = 3.230e8
		a111 = -5.52e7 * (T-393) + 2.76e9
		a112 = 4.470e9
		a123 = 4.910e9
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.11
		q12 = -0.045
		q44 = 0.059
		c11 = 1.78e11
		c12 = 0.964e11
		c44 = 1.22e11
		p0 = 0.26
	elif Potential == 'BTO-05-Li':
		Curie_Temp = 388
		a0 = abs(4.124e5 * (300 - Curie_Temp))
		a1 = 4.124e5 * (T - Curie_Temp)
		a11 = -2.097e8
		a12 = 7.974e8
		a111 = 1.294e9
		a112 = -1.950e9
		a123 = -2.500e9
		a1111 = 3.863e10
		a1112 = 2.529e10
		a1122 = 1.637e10
		a1123 = 1.367e10
		q11 = 0.11
		q12 = -0.045
		q44 = 0.059
		c11 = 1.78e11
		c12 = 0.964e11
		c44 = 1.22e11
		p0 = 0.26
	elif Potential == 'BTO-07-Wang':
		Curie_Temp = 391
		a0 = abs(3.61e5 * (300 - Curie_Temp))
		a1 = 3.61e5 * (T - Curie_Temp)
		a11 = -1.83e9 + 4e6 * T
		a12 = -2.24e9 + 6.7e6 * T
		a111 = 1.39e10 - 3.2e7 * T
		a112 = -2.2e9
		a123 = 5.51e10
		a1111 = 4.84e10
		a1112 = 2.53e11
		a1122 = 2.8e11
		a1123 = 9.35e10
		q11 = (0.11 + 0.10) / 2
		q12 = -(0.045 + 0.034)/2
		q44 = 0.29
		c11 = 1.78e11
		c12 = 0.964e11
		c44 = 1.22e11
		p0 = 0.26
	elif Potential == 'BFO-16-Xue':
		Curie_Temp = 1120
		a0 = abs(4 * 1.093e5 * (300 - Curie_Temp))
		#a1 = 4.64385e5 * (T - Curie_Temp)
		a1 = 4 * 1.093e5 * (T - Curie_Temp)
		a11 = 4*5.318e8
		a12 = -4 * 5.123e8
		a111 = -4 * 4.4e8
		a112 = 4 * 2.074e8
		a123 = 4 * 4.198e8
		a1111 = 4 * 0.98e8
		a1112 = 4 * 1.1e7
		a1122 = 4 * 9.5e7
		a1123 = 4 * 2.0e8
		q11 = 0.032
		q12 = -0.016
		q44 = 0.02015
		c11 = 2.28e11
		c12 = 1.28e11
		c44 = 6.5e10
		p0 = 1.0
	elif Potential == 'BFO-12-Vasudevam':
		Curie_Temp = 1103
		a0 = abs(4.64385e5 * (300 - Curie_Temp))
		a1 = 4.64385e5 * (T - Curie_Temp)
		a11 = 2.29047e8
		a12 = 3.06361e8
		a111 = 5.99186e7
		a112 = -3.33980e5
		a123 = -1.77754e8
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.032
		q12 = -0.016
		q44 = 0.04
		c11 = 3.02e11
		c12 = 1.62e11
		c44 = 0.68e11
		p0 = 0.52
	elif Potential == 'BFO-14-Xue':
		Curie_Temp = 1193
		a0 = abs(4.0e5 * (300 - Curie_Temp))
		a1 = 4.0e5 * (T - Curie_Temp)
		a11 = 3.0e8
		a12 = 1.188e8
		a111 = 0
		a112 = 0
		a123 = 0
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.057
		q12 = -0.02
		q44 = 0.000733
		c11 = 2.280e11
		c12 = 1.28e11
		c44 = 0.65e11
		p0 = 0.52
	elif Potential == 'PTO-02-Li':
		Curie_Temp = 752.15
		a0 = abs(3.8e5 * (300 - Curie_Temp))
		a1 = 3.8e5 * (T - Curie_Temp)
		a11 = -73000000
		a12 = 7.5e8
		a111 = 2.6e8
		a112 = 6.1e8
		a123 = -3700000000
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.089
		q12 = -0.026
		q44 = 0.03375
		c11 = 174600000000
		c12 = 79370000000
		c44 = 111100000000
		p0 = 0.757
	elif Potential == 'STO-07-Li':
		a0 = abs(2.6353e7 * (1/(np.tanh(42/T)) - 0.90476))
		a1 = 2.6353e7 * (1/(np.tanh(42/T)) - 0.90476)
		a11 = 1696000000
		a12 = 1373000000
		a111 = 0
		a112 = 0
		a123 = 0
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.066
		q12 = -0.0135
		q44 = 0.0096
		c11 = 336000000000
		c12 = 107000000000
		c44 = 127000000000
		p0 = 0.10
	elif Potential == 'STO-10-Sheng':
		Curie_Temp = 30
		a0 = abs(4.06e7 * (1/(np.tanh(42/T)) - 1/(np.tanh(42/Curie_Temp))))
		a1 = 4.06e7 * (1/(np.tanh(42/T)) - 1/(np.tanh(42/Curie_Temp)))
		a11 = 1.701e9
		a12 = 3.645e9
		a111 = 0
		a112 = 0
		a123 = 0
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.04581
		q12 = -0.0135
		q44 = 0.0096
		c11 = 3.36e11
		c12 = 1.07e11
		c44 = 1.27e11
		p0 = 0.10
	elif Potential == 'KNN-17-Pohlmann':
		Curie_Temp = 650
		a0 = abs(2 * XF * 4.29e7 * (1/np.tanh(140/300) - 1/np.tanh(140/Curie_Temp)) + (1-2*XF)*5.98e7*(1/np.tanh(140/300) - 1/np.tanh(140/Curie_Temp)))
		a1 = 2 * XF * 4.29e7 * (1/np.tanh(140/T) - 1/np.tanh(140/Curie_Temp)) + (1-2*XF)*5.98e7*(1/np.tanh(140/T) - 1/np.tanh(140/Curie_Temp))
		a11 = 2*XF*(-2.7302e8) + (1-2*XF)*(-6.36e8)
		a12 = 2*XF*(1.0861e9) + (1-2*XF)*(9.66e8)
		a111 = 2*XF*(3.0448e9) + (1-2*XF)*(2.81e9)
		a112 = 2*XF*(-2.727e9)+(1-2*XF)*(-1.99e9)
		a123 = 2*XF*(1.5513e10)+(1-2*XF)*(4.5e9)
		a1111 = 2*XF*(2.4044e10) + (1-2*XF)*(1.74e10)
		a1112 = 2*XF*(3.7328e9) + (1-2*XF)*(5.99e9)
		a1122 = 2*XF*(3.3485e8) + (1-2*XF)*(2.5e10)
		a1123 = 2*XF*(-6.2017e10) + (1-2*XF)*(-1.17e10)
		q11 = 0.12
		q12 = -0.053
		q44 = 0.052
		s11 = 4.6e-12
		s12 = -1.1e-12
		s44 = 1.11e-11
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		p0 = 0.54
	elif Potential == 'KNO-09-Liang':
		Curie_Temp = 650
		a0 = abs(4.273e5 * (300 - Curie_Temp))
		a1 = 4.273e5 * (T - Curie_Temp)
		a11 = -636000000
		a12 = 966000000
		a111 = 2810000000
		a112 = -1990000000
		a123 = 6030000000
		a1111 = 17400000000
		a1112 = 5990000000
		a1122 = 25000000000
		a1123 = -11700000000
		q11 = 0.12
		q12 = -0.053
		q44 = 0.052
		s11 = 4.6e-12
		s12 = -1.1e-12
		s44 = 1.11e-11
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		p0 = 0.45
	elif Potential == 'BZTO-18-Peng':
		Curie_Temp = 115.038 - 80.884*XF - 1421.437*XF**2 - 3088.612*XF**3 + 8443.89*XF**4 + 273
		
		p0 = 0.26
		
		if XF >= 0 and XF <= 0.1:
			a0 = 4.124 + 4.16*XF + 176.6*XF**2 - 27761*XF**4
		else:
			a0 = 2.84 - 1.396*XF + 0.83 * (np.sinh(7.65-34.17*XF)/np.cosh(7.65-34.17*XF))
		
		a11 = -8.2571 - 174.043 * XF**3 + 6.2368 * (np.sinh(7.3152 - 43.3423*XF)/np.cosh(7.65 - 43.3423*XF))
		
		if XF >= 0 and XF <= 0.08:
			a12 = 7.974 + 101.897*XF - 84612.4*XF**4
		else:
			a12 = 11.2932 + 11.2489 * (np.sinh(2.1278 - 25.0715*XF)/np.cosh(2.1278 - 25.0715*XF))
			
		if XF >= 0 and XF <= 0.1:
			a111 = -102270.39 + 102271.684*np.exp(XF) - 102142*XF - 52790*XF**2
		else:
			a111 = 505.76 - ((501.54)/(1 + np.exp(50.193*(XF - 0.202)))) + 80359*XF**4
			
		if XF >=0 and XF <= 0.08:
			a112 = -1.95 - 64.042*XF - 1879.2*XF**2
		else:
			a112 = -10.996 - 8.601* (np.sinh(3.475 - 21.3579*XF)/np.cosh((3.475 - 21.3579*XF)))
			
		if XF >= 0 and XF <= 0.08:
			a123 = -2.5 - 179.4*XF + 2300*XF**2
		else:
			a123 = -3.513 + 19.363*XF - 27.1237*XF**2
			
		if XF >= 0 and XF <= 0.08:
			a1111 = 3.863 + 82.4165*XF + 666.47*XF**2
		else:
			a1111 = 7.3375 + 7.3934 * (np.sinh(7.6408 - 49.3288*XF)/np.cosh(7.6408 - 49.3288*XF))
			
		if XF >= 0 and XF <= 0.15:
			a1112 = 7.8471- 5.3814 * (np.sinh(1.9787 - 22.1963*XF)/np.cosh(1.9787 - 22.1963*XF))
		else:
			a1112 = 6.5749 + 6.6076 * (np.sinh(13.0115 - 77.0462*XF)/np.cosh(13.0115 - 77.0462*XF))
			
		if XF >= 0 and XF <= 0.15:
			a1122 = 7.05 - 6.066 * (np.sinh(1.41 - 20.091 * XF)/np.cosh(1.41 - 20.091 * XF))
		else:
			a1122 = 6.5378 + 6.6708 * (np.sinh(12.9508 - 76.7401*XF)/np.cosh(12.9508 - 76.7401*XF))
			
		if XF >= 0 and XF <= 0.1:
			a1123 = 8.845 - 7.7478 * (np.sinh(8 - 104*XF)/np.cosh(8 - 104*XF))
		else:
			a1123 = 8.1048 + 8.2268 * (np.sinh(6.5178 - 41.525*XF)/np.cosh(6.5178 - 41.525*XF))
			
		s11 = 8.3 - 4.87*XF;		s11 = s11*1e-12
		s12 = -2.7 + 2.025 * XF;	s12 = s12*1e-12
		s44 = 9.24 + 5.02 * XF;		s44 = s44*1e-12
		
		q11 = 0.11 - 0.004*XF
		q12 = -0.034 - 0.005*XF
		q44 = 0.029 + 0.002*XF
		
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		
		a1 = a0 * (T - Curie_Temp)	
		
		a1 = a1*1e5;		a11 = a11*1e8;		a12 = a12*1e8
		a111 = a111*1e9;	a112 = a112*1e9;	a123 = a123*1e9
		a1111 = a1111*1e10;	a1112 = a1112*1e10;	a1122 = a1122*1e10
		a1123 = a1123*1e10;
	elif Potential == 'BSTO-17-Cao':
		Curie_Temp = 390
		a0 = abs(5.0e5 * 160 * ((np.cosh(160/300)/np.sinh(160/300)) - (np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)))*(1-x) + 2.525e7*((np.cosh(54/300)/np.sinh(54/300)) - (np.cosh(54/30)/np.sinh(54/30)))*x)
		a1 = 5.0e5 * 160 * ((np.cosh(160/T)/np.sinh(160/T)) - (np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)))*(1-x) + 2.525e7*((np.cosh(54/T)/np.sinh(54/T)) - (np.cosh(54/30)/np.sinh(54/30)))*x
		a11 = -1.154e8
		a12 = 6.530e8 * (1 - 1.4196*XF)
		a111 = -2.106e9
		a112 = 4.091e9
		a123 = -6.688e9 * (1 - 1.3*XF)
		a1111 = 7.590e10
		a1112 = -2.193e10
		a1122 = -2.221e10
		a1123 = 2.416e10
		q11 = (1-XF)*0.11 + XF*0.066
		q12 = (1-XF)*(-0.045) + XF*(-0.0135)
		q44 = (1-XF)*(0.029) + XF*(0.0096)
		c11 = (1-XF)*177940000000 + XF*(3.36e11)
		c12 = (1-XF)*96350000000 + XF*(1.07e11)
		c44 = (1-XF)*121990000000 + XF*(1.27e11)
		p0 = 0.26
	elif Potential == 'BCTO-17-Cao':
		Curie_Temp = 390
		a0 = abs(5.0e5 * 160 * ((np.cosh(160/300)/np.sinh(160/300)) - (np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp))))
		a1 = 5.0e5 * 160 * ((np.cosh(160/T)/np.sinh(160/T)) - (np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)))
		a11 = -1.154e8
		a12 = 6.530e8 * (1 + 2.8775*XF)
		a111 = -2.106e9
		a112 = 4.091e9 * (1 + 3.5865*XF)
		a123 = -6.688e9 * (1 + 4.0015*XF)
		a1111 = 7.590e10
		a1112 = -2.193e10
		a1122 = -2.221e10
		a1123 = 2.416e10
		q11 = (1-XF)*0.11 + XF*0.066
		q12 = (1-XF)*(-0.045) + XF*(-0.0135)
		q44 = (1-XF)*(0.029) + XF*(0.0096)
		c11 = (1-XF)*177940000000 + XF*(4.03e11)
		c12 = (1-XF)*96350000000 + XF*(1.07e11)
		c44 = (1-XF)*121990000000 + XF*(9.99e10)
		p0 = 0.26
	elif Potential == 'BSTO-18-Huang':
		Curie_Temp = 390
		a0 = abs(8.0e7 * ((np.cosh(160/300)/np.sinh(160/300))-(np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)) - (-1.75)*XF))
		a1 = 8.0e7 * ((np.cosh(160/T)/np.sinh(160/T))-(np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)) - (-1.75)*XF)
		a11 = -1.154e8 * (1 - 1.05*XF)
		a12 = 6.530e8 * (1 - 1.05*XF)
		a111 = -2.106e9 * (1 - 0.483*XF)
		a112 = 4.091e9 * (1 - 0.483*XF)
		a123 = -6.688e9 * (1 - 0.483*XF)
		a1111 = 7.590e10
		a1112 = -2.193e10
		a1122 = -2.221e10
		a1123 = 2.416e10
		q11 = (1-XF)*0.11 + XF*0.066
		q12 = (1-XF)*(-0.045) + XF*(-0.0135)
		q44 = (1-XF)*(0.029) + XF*(0.0096)
		c11 = (1-XF)*177940000000 + XF*(3.36e11)
		c12 = (1-XF)*96350000000 + XF*(1.07e11)
		c44 = (1-XF)*121990000000 + XF*(1.27e11)
		p0 = 0.26
	elif Potential == 'PZT-88-Haun':
		CurieT = 462.23 + 843.4*XF - 2105.5*XF**2 + 4041.8*XF**3 - 3828.3*XF**4 + 1337.8*XF**5
		
		if XF >= 0.0 and XF < 0.5:
			Curie = (2.1716 / (1.0 + 500.05*(XF-0.5)**2) + 0.131*XF+2.01)*10**5
		else:
			Curie = (2.8339 / (1.0+126.56*(XF-0.5)**2) + 1.4132)*10**5
		
		Z1 = ((-9.6 - 0.012501*XF)*np.exp(-12.6*XF) +0.42743*XF+2.6213)*10**14 / Curie
		Z2 = ((16.225-0.088651*XF)*np.exp(-21.255*XF)-0.76973*XF+0.887)*10**14 / Curie
		a0 = 1/(2*8.85E-012*Curie)
		a11 = (10.612 - 22.655*XF + 10.955*XF**2)*10**13/Curie
		a111 = (12.026-17.296*XF+ 9.179*XF**2)*10**13/Curie
		a112 = (58.804*np.exp(-29.397*XF)-3.3754*XF+4.2904)*10**14 / Curie
		p0 = 0.757
		
		a1 = a0 * (T - CurieT)
		a11 = (10.612 - 22.655*XF + 10.955*XF**2)*10**13 / Curie
		a12 = Z1/3-a11
		a111 = (12.026 - 17.296*XF + 9.179*XF**2)*10**13 / Curie
		a112 = (58.804 * np.exp(-29.397*XF) - 3.3754*XF + 4.2904)*10**14 / Curie
		a123 = (Z2 - 3*a111 - 6*a112)
		a1111 = 0;	a1112 = 0;	a1122 = 0;	a1123 = 0
		
		q11 = 0.045624+0.042796*XF+0.029578/(1+200*(XF-0.5)**2)
		q12 = -0.026568/( 1+ 200*(XF-0.5)**2) - 0.012093*XF - 0.013386
		q44 = 0.5*(0.046147+0.020857*XF+0.025325/(1+200.*(XF-0.5)**2))
		
		if XF >= 0 and XF < 0.45:
			s11 = 8.8e-12;	s12 = -2.9e-12;	s44 = 2.46e-11
		elif XF >= 0.45 and XF < 0.55:
			s11 = 1.05e-11;	s12 = -3.7e-12;	s44 = 2.87e-11
		elif XF >= 0.55 and XF < 0.65:
			s11 = 8.6e-12;	s12 = -2.8e-12;	s44 = 2.12e-11
		elif XF >= 0.65 and XF < 0.75:
			s11 = 8.4e-12;	s12 = -2.7e-12;	s44 = 1.75e-11
		elif XF >= 0.75 and XF < 0.85:
			s11 = 8.2e-12;	s12 = -2.6e-12;	s44 = 1.44e-11
		elif XF >= 0.85 and XF <= 1.0:
			s11 = 8.1e-12;	s12 = -2.5e-12;	s44 = 1.2e-11
			
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		
	else:
		print('The Potential: ', Potential, ' is not yet implemented in this code.')
		quit()

	s11 = (c11 + c12) / ((c11 + 2*c12) * (c11 - c12))
	s12 = (-c12) / ((c11 + 2*c12) * (c11 - c12))
	s44 = 1/c44

	a_values = [a1, a11, a12, a111, a112, a123, a1111, a1112, a1122, a1123]
	c_values = [c11, c12, c44]
	s_values = [s11, s12, s44]
	q_values = [q11, q12, q44]
	
	return a0, a_values, c_values, s_values, q_values, p0
	
def Bulk(Temp, App_Stress, Elec_Field, Potential, XF, Iterations):
	
	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Symbols()
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, App_Stress,XF)
	
	Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
	
	Prob = Prob.subs({s1:App_Stress[0],s2:App_Stress[1],s3:App_Stress[2],s4:App_Stress[3],s5:App_Stress[4],s6:App_Stress[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	
	newProb = sp.lambdify([p1,p2,p3],Prob,'numpy')
	
	f, equil_polar = Solver_Polarization(newProb, p0, Iterations)
	
	return f, equil_polar

def Thin_Film(Temp, Misfit, Elec_Field, Potential, XF, Iterations):

	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Symbols()
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, Misfit,XF)
	
	Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
	
	Helmholtz = Setup_Helmholtz(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
	
	Prob = Prob.subs({s3:0,s4:0,s5:0,e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	
	solutions = (sp.solve([Prob.diff(s1)+Misfit[0],Prob.diff(s2)+Misfit[1],Prob.diff(s6)+Misfit[2]],[s1,s2,s6]))
	
	Prob += Misfit[0] * s1 + Misfit[1] * s2 + Misfit[2] * s6
	
	Prob = Prob.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6]})
	
	Helmholtz = Helmholtz.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6],s3:0,s4:0,s5:0,e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	
	newProb = sp.lambdify([p1,p2,p3],Prob,'numpy')
	
	f, equil_polar = Solver_Polarization(newProb,p0,Iterations)
	
	return f, equil_polar, Prob

def Thin_Film_Rotation(Temp, Misfit, Elec_Field, Rotation_List, Potential, XF, Iterations):

	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Symbols()
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, Misfit,XF)
	
	Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
	
	Prob = Prob.subs({e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})

	phi, theta, psi = sp.symbols('Phi Theta Psi')
	r = Rotation_List
	
	A_1ij = sp.Matrix([[sp.cos(phi),sp.sin(phi),0],[-sp.sin(phi),sp.cos(phi),0],[0,0,1]])
	A_2ij = sp.Matrix([[1,0,0],[0,sp.cos(theta),sp.sin(theta)],[0,-sp.sin(theta),sp.cos(theta)]])
	A_3ij = sp.Matrix([[sp.cos(psi),sp.sin(psi),0],[-sp.sin(psi),sp.cos(psi),0],[0,0,1]])
	
	m = A_3ij.subs([(psi,np.deg2rad(r[2]))]).evalf() * A_2ij.subs([(theta,np.deg2rad(r[1]))]).evalf() * A_1ij.subs([(phi,np.deg2rad(r[0]))]).evalf()
	
	m = m.transpose()
	
	e1app = Misfit[0];	e2app = Misfit[1];	e6app = Misfit[2];
	s3app = Misfit[3];	s4app = Misfit[4];	s5app = Misfit[5];
	
	s1g, s2g, s6g, e3g, e4g, e5g = sp.symbols('S1g S2g S6g Eg3 Eg4 Eg5')
	
	sig_app = sp.Matrix([[s1g,s6g,s5app],[s6g,s2g,s4app],[s5app,s4app,s3app]])
	eta_app = sp.Matrix([[e1app,0.5 * e6app, 0.5 * e5g],[0.5*e6app,e2app,0.5*e4g],[0.5*e5g,0.5*e4g,e3g]])
	
	pglobal = (m * sp.Matrix([p1,p2,p3]))
	sig_local = m.transpose() * sig_app * m 
	eta_local = m.transpose() * eta_app * m
	
	# pglobal = (m.transpose() * sp.Matrix([p1,p2,p3]))
	# sig_local = m * sig_app * m.transpose() 
	# eta_local = m * eta_app * m.transpose()
	
	dProb = Prob
	
	equations = [
		dProb.diff(s1).subs({s1:sig_local[0,0],s2:sig_local[1,1],s3:sig_local[2,2],s4:sig_local[1,2],s5:sig_local[0,2],s6:sig_local[0,1]}) + eta_local[0,0],
		dProb.diff(s2).subs({s1:sig_local[0,0],s2:sig_local[1,1],s3:sig_local[2,2],s4:sig_local[1,2],s5:sig_local[0,2],s6:sig_local[0,1]}) + eta_local[1,1],
		dProb.diff(s3).subs({s1:sig_local[0,0],s2:sig_local[1,1],s3:sig_local[2,2],s4:sig_local[1,2],s5:sig_local[0,2],s6:sig_local[0,1]}) + eta_local[2,2],
		dProb.diff(s4).subs({s1:sig_local[0,0],s2:sig_local[1,1],s3:sig_local[2,2],s4:sig_local[1,2],s5:sig_local[0,2],s6:sig_local[0,1]}) + 2*eta_local[1,2],
		dProb.diff(s5).subs({s1:sig_local[0,0],s2:sig_local[1,1],s3:sig_local[2,2],s4:sig_local[1,2],s5:sig_local[0,2],s6:sig_local[0,1]}) + 2*eta_local[0,2],
		dProb.diff(s6).subs({s1:sig_local[0,0],s2:sig_local[1,1],s3:sig_local[2,2],s4:sig_local[1,2],s5:sig_local[0,2],s6:sig_local[0,1]}) + 2*eta_local[0,1],
	]
	
	solutions = sp.solve(equations,[s1g,s2g,s6g,e3g,e4g,e5g])
	
	Prob = Prob.subs({s1:sig_local[0,0],s2:sig_local[1,1],s3:sig_local[2,2],s4:sig_local[1,2],s5:sig_local[0,2],s6:sig_local[0,1]}) + Misfit[0]*s1g + Misfit[1] * s2g + Misfit[2] * s6g
	
	Prob = Prob.subs({s1g:solutions[s1g],s2g:solutions[s2g],s6g:solutions[s6g],e3g:solutions[e3g],e4g:solutions[e4g],e5g:solutions[e5g]})
	
	newProb = sp.lambdify([p1,p2,p3],Prob,'numpy')
	
	f, equil_polar = Solver_Polarization(newProb,p0,Iterations)
	
	pglo = (m * sp.Matrix(equil_polar))
	
	return f, pglo, Prob
	
def Bulk_Domain_Classifer(Polarization):
	pol = Polarization
	
	px = pol[0];	py = pol[1];	pz = pol[2]
	p = (px**2 + py**2 + pz**2)**0.5
	
	if abs(px) == abs(py) and abs(py) == abs(pz):
		domain = 'Rhomohedral'
	elif abs(px) == abs(py) and abs(pz) != abs(py) and abs(px) > 0 and abs(py) > 0 and abs(pz) > 0:
		domain = 'Monoclinic'
	elif abs(px) == abs(pz) and abs(pz) != abs(py) and abs(px) > 0 and abs(py) > 0 and abs(pz) > 0:
		domain = 'Monoclinic'
	elif abs(pz) == abs(py) and abs(pz) != abs(px) and abs(px) > 0 and abs(py) > 0 and abs(pz) > 0:
		domain = 'Monoclinic'
	elif abs(pz) != abs(px) and abs(pz) != abs(py) and abs(py) != abs(px) and abs(px) > 0.0001 and abs(py) > 0.0001 and abs(pz) > 0.0001:
		domain = 'Triclinic'
	else:
		domain = 'Orthor or Tetra'
		psi = 0
		phi = 0

		psi = math.acos(pz/p)
		try:
			phi = math.acos(px/(p * math.sin(psi)))
		except:
			phi = 0

		phi = phi * 180/math.pi
		psi = psi * 180/math.pi

		if psi > 90:
			psi = psi - 90
			
		if phi > 90:
			phi = phi - 90
		
		if psi == 90 and phi == 0 or psi == 90 and phi == 90 or psi == 0 and phi == 0:
			domain = 'Tetra'
		else:
			domain = 'Orthor'
		
		
		
	return domain
		
def Thin_Film_Dielectric(Temp, Misfit, Elec_Field, Potential,Rotate_Flag, Rotation_List, XF, Iterations, OutputFlag):

	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Symbols()
	
	if Rotate_Flag == 1:
		f, equil_polar, Prob = Thin_Film_Rotation(Temp, Misfit, Elec_Field, Rotation_List, Potential, XF, Iterations)
	else:
		f, equil_polar, Prob = Thin_Film(Temp, Misfit, Elec_Field, Potential, XF, Iterations)
	
	#Susceptibilities
	n1 = Prob.diff(p1,p1);		n2 = Prob.diff(p2,p2);		n3 = Prob.diff(p3,p3);
	n4 = Prob.diff(p2,p3);		n5 = Prob.diff(p1,p3);		n6 = Prob.diff(p1,p2);
	
	suscep_derives = [n1, n2, n3, n4, n5, n6]
	
	suscep_list = []
	for ij in suscep_derives:
		suscep_list.append(ij.evalf(subs={p1:abs(equil_polar[0]),p2:abs(equil_polar[1]),p3:abs(equil_polar[2])}))
	
	x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
	x_values = np.matrix(x_values,dtype='float')
	x_values = np.linalg.inv(x_values)
	
	k_values = (x_values)/8.85e-12
	
	k11 = k_values[0,0];	k22 = k_values[1,1];	k33 = k_values[2,2]
	k23 = k_values[1,2];	k13 = k_values[0,2];	k12 = k_values[0,1]
	
	k_print = [k11, k22, k33, k12, k13, k23]
	
	if OutputFlag == 1:
		Print_Dielectric_Tensor(k_values, Potential)
	
	return k_print

def Domain_Qualifier(Polarization,System):
	
	Polar = Polarization
		
	if System.lower() == 'bulk':
		#We can only choose from T, O, R
		
		p1 = abs(Polar[0]); p2 = abs(Polar[1]);	p3 = abs(Polar[2])
		
		#Get Polar Ratios
		
		if p1 < 0.0001 and p2 < 0.0001 and p3 < 0.0001:
			Domain = 'Cubic'
		
		p1p2 = p1 / p2 ; 	p2p1 = p2 / p1
		p1p3 = p1 / p3 ;	p3p1 = p3 / p1
		p2p3 = p2 / p3 ;	p3p2 = p3 / p2
		
		p1p2 = min(p1p2,p2p1)
		p1p3 = min(p1p3,p3p1)
		p2p3 = min(p2p3,p3p2)
		
		#Tetragonal Domains
		if p1 < 0.0001 and p2 < 0.0001 and p3 > 0.0001:
			Domain = 'Tet'; Polar = [p1,p2,p3]
		elif p1 > 0.0001 and p2 < 0.0001 and p3 < 0.0001:
			Domain = 'Tet'; Polar = [p3,p2,p1]
		elif p1 < 0.0001 and p2 > 0.0001 and p3 < 0.0001:
			Domain = 'Tet'; Polar = [p1,p3,p2]
		
		#Orthorhombic Domains
		if p1 < 0.0001 and p2 > 0.0001 and p3 > 0.0001 and p2p3 > 0.95:
			Domain = 'O23'; Polar = [p1, p2, p3]
		elif p1 > 0.0001 and p2 < 0.0001 and p3 > 0.0001 and p1p3 > 0.95:
			Domain = 'O13'; Polar = [p2, p1, p3]
		elif p1 > 0.0001 and p2 > 0.0001 and p3 < 0.0001 and p1p2 > 0.95:
			Domain = 'O12'; Polar = [p3, p2, p1]
		
		#Monoclinic Domains
		if p1 > 0.0001 and p2 > 0.0001 and p1 < p3 and p2 < p3 and p3 > 0.0001 and 0.59 < p1p3 < 1.0:
			Domain = 'Ma'
			
		if p1 > 0.0001 and p2 > 0.0001 and p1 > p3 and p2 > p3 and p3 > 0.0001 and 0.99 < (p1 / p3) < 1.5:
			Domain = 'Mb'
		
		if p1 > 0.0001 and p2 < 0.0001 and p3 > 0.0001 and p1p3 < 0.95:
			Domain = 'Mca'	
		
		if p1 < 0.0001 and p2 > 0.0001 and p3 > 0.0001 and p2p3 < 0.95:
			Domain = 'Mcb'
		
		if p1 > 0.0001 and p2 > 0.0001 and p3 < 0.0001 and p1p2 < 0.95:
			Domain = 'Mab'
			
		#Rhomohedral Domains
		if p1p2 >= 0.95 and p1p3 >= 0.95 and p2p3 >= 0.95 and p1p2 <= 1.05 and p1p3 <= 1.05 and p2p3 <= 1.05:
			if p1 > 0.0001 and p2 > 0.0001 and p3 > 0.0001:
				Domain = 'R'
			else:
				Domain = 'Cubic'	
			
	else:
		p1 = abs(Polar[0]); p2 = abs(Polar[1]);	p3 = abs(Polar[2])
		
		#Get Polar Ratios
		
		p1p2 = p1 / p2 ; 	p2p1 = p2 / p1
		p1p3 = p1 / p3 ;	p3p1 = p3 / p1
		p2p3 = p2 / p3 ;	p3p2 = p3 / p2
		
		p1p2 = min(p1p2,p2p1)
		p1p3 = min(p1p3,p3p1)
		p2p3 = min(p2p3,p3p2)

		if p1 < 0.0001 and p2 < 0.0001 and p3 < 0.0001:
			Domain = 'Cubic'
		
		#Tetragonal Domains
		if p1 < 0.0001 and p2 < 0.0001 and p3 > 0.0001:
			Domain = 'C'
		elif p1 > 0.0001 and p2 < 0.0001 and p3 < 0.0001:
			Domain = 'A1'
		elif p1 < 0.0001 and p2 > 0.0001 and p3 < 0.0001:
			Domain = 'A2'
		
		#Orthorhombic Domains
		if p1 < 0.0001 and p2 > 0.0001 and p3 > 0.0001 and p2p3 > 0.95:
			Domain = 'O23'
		elif p1 > 0.0001 and p2 < 0.0001 and p3 > 0.0001 and p1p3 > 0.95:
			Domain = 'O13'
		elif p1 > 0.0001 and p2 > 0.0001 and p3 < 0.0001 and p1p2 > 0.95:
			Domain = 'O12'
		
		#Monoclinic Domains
		if p1 > 0.0001 and p2 > 0.0001 and p1 < p3 and p2 < p3 and p3 > 0.0001 and 0.59 < p1p3 < 1.0:
			Domain = 'Ma'
			
		if p1 > 0.0001 and p2 > 0.0001 and p1 > p3 and p2 > p3 and p3 > 0.0001 and 0.99 < (p1 / p3) < 1.5:
			Domain = 'Mb'
		
		if p1 > 0.0001 and p2 < 0.0001 and p3 > 0.0001 and p1p3 < 0.95:
			Domain = 'Mca'	
		
		if p1 < 0.0001 and p2 > 0.0001 and p3 > 0.0001 and p2p3 < 0.95:
			Domain = 'Mcb'
		
		if p1 > 0.0001 and p2 > 0.0001 and p3 < 0.0001 and p1p2 < 0.95:
			Domain = 'Mab'
			
		#Rhomohedral Domains
		if p1p2 >= 0.95 and p1p3 >= 0.95 and p2p3 >= 0.95 and p1p2 <= 1.05 and p1p3 <= 1.05 and p2p3 <= 1.05:
			if p1 > 0.0001 and p2 > 0.0001 and p3 > 0.0001:
				Domain = 'R'
			else:
				Domain = 'Cubic'
				
		if p1p2 >=0.01 and p1p2 <=0.95 and p1p3 >=0.01 and p1p3 <=0.95 and p2p3 >=0.01 and p2p3 <=0.95:
			if p1 > 0.01 and p2 > 0.01 and p3 > 0.01:
				Domain = 'Triclinic'
			
	return Domain, Polar
	
def Bulk_Dielectric(Temp, App_Stress, Elec_Field, Potential, XF, Iterations, OutputFlag):

	f, equil_polar = Bulk(Temp, App_Stress, Elec_Field, Potential, XF, Iterations)
	
	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1 ,e2, e3 = Setup_Symbols()
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, App_Stress, XF)
	
	Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2 ,s3, s4, s5, s5, e1, e2, e3)
	
	Prob = Prob.subs({s1:App_Stress[0],s2:App_Stress[0],s3:App_Stress[0],s4:App_Stress[0],s5:App_Stress[0],s6:App_Stress[0],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
		
	domain, equil_polar = Domain_Qualifier(equil_polar,'Bulk')
	
	#Susceptibilities
	n1 = Prob.diff(p1,p1);		n2 = Prob.diff(p2,p2);		n3 = Prob.diff(p3,p3);
	n4 = Prob.diff(p2,p3);		n5 = Prob.diff(p1,p3);		n6 = Prob.diff(p1,p2);
	
	suscep_derives = [n1, n2, n3, n4, n5, n6]
	
	suscep_list = []
	for ij in suscep_derives:
		suscep_list.append(ij.evalf(subs={p1:abs(equil_polar[0]),p2:abs(equil_polar[1]),p3:abs(equil_polar[2]),s1:App_Stress[0],s2:App_Stress[1],s3:App_Stress[2],s4:App_Stress[3],s5:App_Stress[4],s6:App_Stress[5]}))
	
	x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
	x_values = np.matrix(x_values,dtype='float')
	
	x_values = np.abs(np.linalg.inv(x_values))
	
	k_values = (x_values)/8.85e-12
	
	k11 = k_values[0,0];	k22 = k_values[1,1];	k33 = k_values[2,2]
	k23 = k_values[1,2];	k13 = k_values[0,2];	k12 = k_values[0,1]
	
	k_print = [k11, k22, k33, k12, k13, k23]
	
	if OutputFlag == 1:
		Print_Dielectric_Tensor(k_values,Potential)
	
	return k_print
	
def Print_Dielectric_Tensor(Tensor,Potential):

	print('')
	print('*********************************************************')
	print('------------------Dielectric Tensor----------------------')
	print('-------------------',Potential,'-------------------------')
	print('-------------------Printed Unitless----------------------')
	print('{:.3f}'.format(round(Tensor[0,0],4)),'','{:.3f}'.format(round(Tensor[0,1],4)),'','{:.3f}'.format(round(Tensor[0,2],4)))
	print('{:.3f}'.format(round(Tensor[1,0],4)),'','{:.3f}'.format(round(Tensor[1,1],4)),'','{:.3f}'.format(round(Tensor[1,2],4)))
	print('{:.3f}'.format(round(Tensor[2,0],4)),'','{:.3f}'.format(round(Tensor[2,1],4)),'','{:.3f}'.format(round(Tensor[2,2],4)))
	print('---------------------------------------------------------')
	print('*********************************************************')
		
def Polarization_v_Temperature(Temp_List, System, Potential, Misfit_App_Stress, Elec_Field, XF, Iterations):
	
	pol_list = []
	p1_list  = [];	p2_list	= [];	p3_list = []
	tem_list = []
	
	if System == 'Film':
		t_step = Temp_List[0]
		while t_step <= Temp_List[1]:
			f, equil_polar = Thin_Film(t_step, Misfit_App_Stress, Elec_Field, Potential,XF, Iterations)
			pol_list.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_list.append(abs(equil_polar[0]))
			p2_list.append(abs(equil_polar[1]))
			p3_list.append(abs(equil_polar[2]))
			tem_list.append(t_step)
			t_step += Temp_List[2]
			
	elif System == 'Bulk':
		t_step = Temp_List[0]
		while t_step <= Temp_List[1]:
			f, equil_polar = Bulk(t_step, Misfit_App_Stress, Elec_Field, Potential,XF, Iterations)
			domain, equil_polar = Domain_Qualifier(equil_polar,'Bulk')
			pol_list.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_list.append(abs(equil_polar[0]))
			p2_list.append(abs(equil_polar[1]))
			p3_list.append(abs(equil_polar[2]))
			tem_list.append(t_step)
			t_step += Temp_List[2]
	
	else:
		print('Incorrect System Type')
		
	plt.plot(tem_list,pol_list)
	plt.plot(tem_list,p1_list)
	plt.plot(tem_list,p2_list)
	plt.plot(tem_list,p3_list)
	plt.ylim(0,max(pol_list)*1.25)
	plt.savefig('P_v_Temp_'+Potential+'.png')
	plt.show()

def Bulk_XF_v_Temperature_Phase_Map(XF_List,Temp_List,Potential,App_Stress,Elec_Field,Iterations):
	
	Domain_Data = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for tem in Temp_l_List:
		temp_data = []
		for xf in XF_l_List:
			f, equil_polar = Bulk(tem,App_Stress,Elec_Field,Potential,xf,Iterations)
			domain,equil_polar = Domain_Qualifier(equil_polar,'Bulk')
			temp_data.append(domain)
		Domain_Data.append(temp_data)
		
	Phase_Map_Plotting(Domain_Data, 'Material Fraction', 'Temperature', XF_List, Temp_List)
	
def Thin_Film_XF_v_Temperature_Phase_Map(XF_List,Temp_List,Potential,Misfit,Elec_Field,Iterations):
	
	Domain_Data = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for tem in Temp_l_List:
		temp_data = []
		for xf in XF_l_List:
			f, equil_polar, prob = Thin_Film(tem, Misfit, Elec_Field, Potential, xf, Iterations)
			domain,equil_polar = Domain_Qualifier(equil_polar,'Film')
			temp_data.append(domain)
		Domain_Data.append(temp_data)
		
	Phase_Map_Plotting(Domain_Data, 'Material Fraction', 'Temperature(K)', XF_List, Temp_List,System='Film')

def Thin_Film_Misfit_Misfit_Phase_Map(Misfit_X_List, Misfit_Y_List,Temp, Potential, Elec_Field,XF,Iterations):
	
	Domain_Data = []
	Mis_x_l_list = np.arange(Misfit_X_List[0],Misfit_X_List[1]+Misfit_X_List[2]/2,Misfit_X_List[2])
	Mis_y_l_list = np.arange(Misfit_Y_List[0],Misfit_Y_List[1]+Misfit_Y_List[2]/2,Misfit_Y_List[2])
	
	for mis_x in Mis_x_l_list:
		temp_data = []
		for mis_y in Mis_y_l_list:
			On_Screen_Updater('Misfit_x',mis_x,Parameter2='Misfit_y',Parameter2Value=mis_y)
			f, equil_polar, prob = Thin_Film(Temp, [mis_x,mis_y,0,0,0,0],Elec_Field,Potential, XF,Iterations)
			domain, equil_polar = Domain_Qualifier(equil_polar,'Film')
			temp_data.append(domain)
			print(mis_x, mis_y, equil_polar,domain)
		Domain_Data.append(temp_data)
	
	print(Domain_Data)
	
	Phase_Map_Plotting(Domain_Data, 'Misfit Strain (x Direction)','Misfit Strain (y Direction)',Misfit_X_List,Misfit_Y_List,System='Film')

def Bulk_XF_v_Temperature_Dielectric_Map(XF_List,Temp_List,Potential,App_Stress,Elec,Iterations):
	
	k11_list	= [];	k22_list	= [];	k33_list	= [];
	xf_list		= [];	tem_list	= [];
	
	xf = XF_List[0]
	while xf <= XF_List[1]:
		tem = Temp_List[0]
		temp_k1 = [];	temp_k2 = [];	temp_k3 = [];
		temp_xf = [];	temp_te = []
		while tem <= Temp_List[1]:
			k_values = Bulk_Dielectric(tem,App_Stress,Elec,Potential,xf,Iterations,0)
			k11 = k_values[0];	k22 = k_values[1]
			k33 = k_values[2]
			temp_k1.append(k11);	temp_k2.append(k22);
			temp_k3.append(k33);	temp_xf.append(xf);	temp_te.append(tem-273)
			On_Screen_Updater('Composition',round(xf,4),Parameter2='Temperature (K)',Parameter2Value=t_step)
			tem += Temp_List[2]
		k11_list.append(temp_k1);	k22_list.append(temp_k2)
		k33_list.append(temp_k3);	xf_list.append(temp_xf)
		tem_list.append(temp_te)
		xf += XF_List[2]
		
	k11_array = np.asarray(k11_list)
	tem_array = np.asarray(tem_list)
	xf_array  = np.asarray(xf_list)
	plt.contourf(xf_array,tem_array,k11_array,cmap='jet')
	plt.colorbar()
	plt.show()

def Thin_Film_XF_v_Temperature_Dielectric_Map(XF_List,Temp_List,Potential,Misfit,Elec_Field,Iterations):
	
	k11_list	= [];	k22_list	= [];	k33_list	= [];
	xf_l		= [];	tem_list	= [];
	
	xf = XF_List[0]
	while xf <= XF_List[1]:
		tem = Temp_List[0]
		while tem <= Temp_List[1]:
			k1, k2, k3, k4, k5, k6 = Thin_Film_Dielectric(tem,Misfit,Elec_Field,0,[0,0,0],xf,Iterations)
			k11_list.append(k1);	k22_list.append(k2);	k33_list.append(k3)
			tem_list.append(tem)
			On_Screen_Updater('Material Fraction',round(xf,4),Parameter2='Temperature (K)',Parameter2Value=t_step)
			tem += Temp_List[2]
		xf += XF_List[2]
		
def Polarization_v_Misfit(Misfit_List, Temp, Potential, Elec_Field, Rotate_Flag, Rotation_List, XF, Iterations):

	pol_list	= [];	p1_list = [];	
	p2_list		= [];	p3_list = []
	
	Misfit_l_List = np.arange(Misfit_List[0],Misfit_List[1]+(Misfit_List[2])/2,Misfit_List[2])
	
	mstep = Misfit_List[0]

	for mstep in Misfit_l_List:
		Misfit = [mstep,mstep,0,0,0,0]
		if Rotate_Flag == 1:
			f, equil_polar, Prob = Thin_Film_Rotation(Temp, Misfit, Elec_Field, Rotation_List, Potential, XF, Iterations)
		else:
			f, equil_polar, Prob = Thin_Film(Temp, Misfit, Elec_Field, Potential, XF, Iterations)
		pol_list.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
		p1_list.append(equil_polar[0])
		p2_list.append(equil_polar[1])
		p3_list.append(equil_polar[2])
		On_Screen_Updater('Misfit',round(mstep,4))
	
	Custom_LinePlot(Misfit_l_List.tolist(),[pol_list,p1_list,p2_list,p3_list],'Misfit Strain','Polarization C/m$^2$',SeriesLabel=['|P|','P$_1$','P$_2$','P$_3$'],MarkerType=['o','s','>','<'],LineStyle=[':','-','--','--'],SaveFlag=True,SaveName=(Potential + 'Misfit_Polarization'))
	Export_Numerical_Data(pol_list,Misfit_List,'Misfit Strain',Potential,'Polarization_Misfit',Data2=p1_list,Data3=p2_list,Data4=p3_list)

def Dielectric_v_Misfit(Misfit_List, Temp, Potential, Elec_Field,Rotate_Flag,Rotation_List, XF, Iterations):

	k11_list = [];	k22_list = [];	k33_list = []
	Misfit_l_List = np.arange(Misfit_List[0],Misfit_List[1]+(Misfit_List[2])/2,Misfit_List[2])
	
	for mstep in Misfit_l_List:
		k_list = Thin_Film_Dielectric(Temp, [round(mstep,4),round(mstep,4),0,0,0,0], Elec_Field, Potential,Rotate_Flag,Rotation_List, XF, Iterations,0)
		k11_list.append(k_list[0]);	k22_list.append(k_list[1]);	k33_list.append(k_list[2])
		On_Screen_Updater('Misfit',round(mstep,4))
		
	Custom_LinePlot(Misfit_l_List.tolist(),[k11_list,k22_list,k33_list],'Misfit Strain','Dielectric Constant',SeriesLabel=['K$_{11}$','K$_{22}$','K$_{33}$'],MarkerType=['o','s','>'],LineStyle=[':','-','--'],SaveFlag=True,SaveName=(Potential + 'Misfit_Dielectric'))
	Export_Numerical_Data(k11_list,Misfit_l_List,'Misfit Strain',Potential,'Dielectric_Misfit',Data2=k22_list,Data3=k33_list)

def Thin_Film_Piezoelectric_Tensor(Temp, Misfit, Elec_Field, Potential, Rotate_Flag, Rotation_List, XF, Iterations, OutputFlag):

	if Rotate_Flag == 1:
		f, equil_polar, Prob = Thin_Film_Rotation(Temp, Misfit, Elec_Field, Rotation_List, Potential, XF, Iterations)
	else:
		f, equil_polar, Prob = Thin_Film(Temp, Misfit, Elec_Field, Potential, XF, Iterations)
		
	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Symbols()
	
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, Misfit,XF)
	
	Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
	
	Prob += Misfit[0] * s1 + Misfit[1] * s2 + Misfit[2] * s6
	
	solutions = (sp.solve([Prob.diff(s1)+Misfit[0],Prob.diff(s2)+Misfit[1],Prob.diff(s6)+Misfit[2]],[s1,s2,s6]))
	
	#Strains
	eta1 = -Prob.diff(s1);		eta2 = -Prob.diff(s2);		eta3 = -Prob.diff(s3);
	eta4 = -Prob.diff(s4);		eta5 = -Prob.diff(s5);		eta6 = -Prob.diff(s6);
	
	Prob = Prob.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6]})
	Prob = Prob.subs({s3:Misfit[3],s4:Misfit[4],s5:Misfit[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	eta1 = eta1.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6],s3:Misfit[3],s4:Misfit[4],s5:Misfit[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	eta2 = eta2.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6],s3:Misfit[3],s4:Misfit[4],s5:Misfit[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	eta3 = eta3.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6],s3:Misfit[3],s4:Misfit[4],s5:Misfit[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	eta4 = eta4.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6],s3:Misfit[3],s4:Misfit[4],s5:Misfit[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	eta5 = eta5.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6],s3:Misfit[3],s4:Misfit[4],s5:Misfit[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	eta6 = eta6.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6],s3:Misfit[3],s4:Misfit[4],s5:Misfit[5],e1:Elec_Field[0],e2:Elec_Field[1],e3:Elec_Field[2]})
	
	#Susceptibilities
	n1 = Prob.diff(p1,p1);		n2 = Prob.diff(p2,p2);		n3 = Prob.diff(p3,p3);
	n4 = Prob.diff(p2,p3);		n5 = Prob.diff(p1,p3);		n6 = Prob.diff(p1,p2);
	
	#We next need to calculate the derivative of strain with respect to the polarization
	e1_1 = eta1.diff(p1);		e1_2 = eta1.diff(p2);		e1_3 = eta1.diff(p3)
	e2_1 = eta2.diff(p1);		e2_2 = eta2.diff(p2);		e2_3 = eta2.diff(p3)
	e3_1 = eta3.diff(p1);		e3_2 = eta3.diff(p2);		e3_3 = eta3.diff(p3)
	e4_1 = eta4.diff(p1);		e4_2 = eta4.diff(p2);		e4_3 = eta4.diff(p3)
	e5_1 = eta5.diff(p1);		e5_2 = eta5.diff(p2);		e5_3 = eta5.diff(p3)
	e6_1 = eta6.diff(p1);		e6_2 = eta6.diff(p2);		e6_3 = eta6.diff(p3)
	
	#Now lets package all of this neatly into a list of lists
	eta_derives = [[e1_1,e1_2,e1_3],[e2_1,e2_2,e2_3],[e3_1,e3_2,e3_3],[e4_1,e4_2,e4_3],[e5_1,e5_2,e5_3],[e6_1,e6_2,e6_3]]
	strain_derives = [eta1, eta2, eta3, eta4, eta5, eta6]
	suscep_derives = [n1, n2, n3, n4, n5, n6]
	
	#Now lets calculate a new list of lists that contains the evaluation of the eta_derives
	d_list = []
	for ij in eta_derives:
		temp_list = []
		for jk in ij:
			temp_list.append(jk.evalf(subs={p1:abs(equil_polar[0]),p2:abs(equil_polar[1]),p3:abs(equil_polar[2])}))
		d_list.append(temp_list)
	
	strain_list = []
	for ij in strain_derives:
		strain_list.append(ij.evalf(subs={p1:abs(equil_polar[0]),p2:abs(equil_polar[1]),p3:abs(equil_polar[2])}))
		
	suscep_list = []
	for ij in suscep_derives:
		suscep_list.append(ij.evalf(subs={p1:abs(equil_polar[0]),p2:abs(equil_polar[1]),p3:abs(equil_polar[2])}))
	
	x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
	x_values = np.matrix(x_values,dtype='float')
	x_values = np.linalg.inv(x_values)
	
	
	d_values = []
	for j in range(1,4):
		temp_list = []
		for i in range(1,7):
			dvalue = 0
			for k in range(1,4):
				dvalue += d_list[i-1][k-1] * x_values[j-1,k-1]
			temp_list.append(dvalue)
		d_values.append(temp_list)
		
	
	#THIS WORKS FOR THE DIAGONAL TERMS. NOW LETS GET IT FIXED FOR OFF-DIAGONAL TERMS
	if OutputFlag == 1:
		Print_Piezoelectric_Tensor(np.matrix(d_values,dtype='float'))
	return d_values
	
def Bulk_Piezoelectric_Tensor(Temp, App_Stress, Elec_Field, Potential, XF, Iterations,OutputFlag):

	f, equil_polar = Bulk(Temp, App_Stress, Elec_Field, Potential, XF, Iterations)
	
	domain, equil_polar = Domain_Qualifier(equil_polar,'Bulk')
	
	p1, p2, p3, s1, s2, s3, s4, s5, s6, e1 ,e2, e3 = Setup_Symbols()
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, App_Stress, XF)
	
	Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2 ,s3, s4, s5, s5, e1, e2, e3)
	
	#Strains
	eta1 = -Prob.diff(s1);		eta2 = -Prob.diff(s2);		eta3 = -Prob.diff(s3);
	eta4 = -Prob.diff(s4);		eta5 = -Prob.diff(s5);		eta6 = -Prob.diff(s6);
	
	#Susceptibilities
	n1 = Prob.diff(p1,p1);		n2 = Prob.diff(p2,p2);		n3 = Prob.diff(p3,p3);
	n4 = Prob.diff(p2,p3);		n5 = Prob.diff(p1,p3);		n6 = Prob.diff(p1,p2);
	
	#We next need to calculate the derivative of strain with respect to the polarization
	e1_1 = eta1.diff(p1);		e1_2 = eta1.diff(p2);		e1_3 = eta1.diff(p3)
	e2_1 = eta2.diff(p1);		e2_2 = eta2.diff(p2);		e2_3 = eta2.diff(p3)
	e3_1 = eta3.diff(p1);		e3_2 = eta3.diff(p2);		e3_3 = eta3.diff(p3)
	e4_1 = eta4.diff(p1);		e4_2 = eta4.diff(p2);		e4_3 = eta4.diff(p3)
	e5_1 = eta5.diff(p1);		e5_2 = eta5.diff(p2);		e5_3 = eta5.diff(p3)
	e6_1 = eta6.diff(p1);		e6_2 = eta6.diff(p2);		e6_3 = eta6.diff(p3)
	
	#Now lets package all of this neatly into a list of lists
	eta_derives = [[e1_1,e1_2,e1_3],[e2_1,e2_2,e2_3],[e3_1,e3_2,e3_3],[e4_1,e4_2,e4_3],[e5_1,e5_2,e5_3],[e6_1,e6_2,e6_3]]
	strain_derives = [eta1, eta2, eta3, eta4, eta5, eta6]
	suscep_derives = [n1, n2, n3, n4, n5, n6]
	
	#Now lets calculate a new list of lists that contains the evaluation of the eta_derives
	d_list = []
	for ij in eta_derives:
		temp_list = []
		for jk in ij:
			temp_list.append(jk.evalf(subs={p1:abs(equil_polar[0]),p2:abs(equil_polar[1]),p3:abs(equil_polar[2]),s1:App_Stress[0],s2:App_Stress[1],s3:App_Stress[2],s4:App_Stress[3],s5:App_Stress[4],s6:App_Stress[5]}))
		d_list.append(temp_list)
	
	strain_list = []
	for ij in strain_derives:
		strain_list.append(ij.evalf(subs={p1:abs(equil_polar[0]),p2:abs(equil_polar[1]),p3:abs(equil_polar[2]),s1:App_Stress[0],s2:App_Stress[1],s3:App_Stress[2],s4:App_Stress[3],s5:App_Stress[4],s6:App_Stress[5]}))
		
	suscep_list = []
	for ij in suscep_derives:
		suscep_list.append(ij.evalf(subs={p1:abs(equil_polar[0]),p2:abs(equil_polar[1]),p3:abs(equil_polar[2]),s1:App_Stress[0],s2:App_Stress[1],s3:App_Stress[2],s4:App_Stress[3],s5:App_Stress[4],s6:App_Stress[5]}))
	
	x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
	x_values = np.matrix(x_values,dtype='float')
	x_values = np.linalg.inv(x_values)
	
	
	d_values = []
	for j in range(1,4):
		temp_list = []
		for i in range(1,7):
			dvalue = 0
			for k in range(1,4):
				dvalue += d_list[i-1][k-1] * x_values[j-1,k-1]
			temp_list.append(dvalue)
		d_values.append(temp_list)
		
	
	#THIS WORKS FOR THE DIAGONAL TERMS. NOW LETS GET IT FIXED FOR OFF-DIAGONAL TERMS
	
	if OutputFlag == 1:
		Print_Piezoelectric_Tensor(np.matrix(d_values,dtype='float'))
	return d_values
	
def Print_Piezoelectric_Tensor(Tensor):

	Tensor = Tensor * 1e12

	print('')
	print('*********************************************************')
	print('-----------------Piezoelectric Tensor---------------------')
	print('-------------------Printed in pm/V-----------------------')
	print('{:.3f}'.format(round(Tensor[0,0],4)),'','{:.3f}'.format(round(Tensor[0,1],4)),'','{:.3f}'.format(round(Tensor[0,2],4)),'','{:.3f}'.format(round(Tensor[0,3],4)),'','{:.3f}'.format(round(Tensor[0,4],4)),'','{:.3f}'.format(round(Tensor[0,5],4)))
	print('{:.3f}'.format(round(Tensor[1,0],4)),'','{:.3f}'.format(round(Tensor[1,1],4)),'','{:.3f}'.format(round(Tensor[1,2],4)),'','{:.3f}'.format(round(Tensor[1,3],4)),'','{:.3f}'.format(round(Tensor[1,4],4)),'','{:.3f}'.format(round(Tensor[1,5],4)))
	print('{:.3f}'.format(round(Tensor[2,0],4)),'','{:.3f}'.format(round(Tensor[2,1],4)),'','{:.3f}'.format(round(Tensor[2,2],4)),'','{:.3f}'.format(round(Tensor[2,3],4)),'','{:.3f}'.format(round(Tensor[2,4],4)),'','{:.3f}'.format(round(Tensor[2,5],4)))
	print('---------------------------------------------------------')
	print('*********************************************************')

def Dielectric_Bulk_Surface(Temp, App_Stress, Elec_Field, Potential, XF, Iterations):
		
	k_values = Bulk_Dielectric(Temp, App_Stress, Elec_Field, Potential, XF, Iterations,1)
	
	n = 200
	m = int(n/2)
	
	phi = np.linspace(0,np.pi,m)
	theta = np.linspace(0,2*np.pi,n)
	
	EE = np.zeros((m,n))
	xei = np.zeros((m,n))
	yei = np.zeros((m,n))
	zei = np.zeros((m,n))
	
	for i in range(n):
		for j in range(m):
			EE[j,i] = np.sin(theta[i])**2 *np.cos(phi[j])**2 *k_values[0] + k_values[1]*np.sin(theta[i])**2 *np.sin(phi[j])**2  + k_values[2]*np.cos(theta[i])**2 
			
	#EE = EE + abs(EE.min().min())
	
	for i in range(n):
		for j in range(m):
			xei[j,i] = EE[j,i] * np.sin(phi[j]) * np.cos(theta[i])
			yei[j,i] = EE[j,i] * np.sin(phi[j]) * np.sin(theta[i])
			zei[j,i] = EE[j,i] * np.cos(phi[j])
			
	mins = [xei.min().min(),yei.min().min(),zei.min().min()]
	maxs = [xei.max().max(),yei.max().max(),zei.max().max()]
	
	fig = plt.figure()
	#ax = fig.gca(projection='3d')
	ax = fig.add_subplot(111,projection='3d')
	norm = matplotlib.colors.Normalize(vmin=EE.min().min(),vmax=EE.max().max())
	ax.plot_surface(xei,yei,zei,facecolors=plt.cm.rainbow(norm(EE)),rstride=1, cstride=1, linewidth=0, antialiased=False)
	m = cm.ScalarMappable(cmap=plt.cm.rainbow,norm=norm)
	m.set_array([])
	plt.axis('off')
	cbar = plt.colorbar(m,ticks=[EE.min().min(),EE.max().max()], fraction=0.025,pad=0.08,orientation='horizontal')
	plt.xlim(1.05*min(mins),1.05*max(maxs))
	plt.ylim(1.05*min(mins),1.05*max(maxs))
	#plt.zlim(1.05*min(mins),1.05*max(maxs))
	cbar.ax.set_xticklabels(['Low','High'])
	
	# for angle in range(0, 361,2):
		# ax.view_init(0, angle)
		# plt.draw()
		# plt.pause(0.0001)
	
	ax.view_init(0,30)
	plt.savefig('Dielectric_' + Potential +'.png')
	plt.show()
	
def Piezoelectric_Bulk_Surface(Temp, App_Stress, Elec_Field, Potential, XF, Iterations,OutputFlag):
	
	d_values = Bulk_Piezoelectric_Tensor(Temp, App_Stress, Elec_Field, Potential, XF, Iterations,OutputFlag)
	
	n = 200
	m = int(n/2)
	
	phi = np.linspace(0,np.pi,m)
	theta = np.linspace(0,2*np.pi,n)
	
	EE = np.zeros((m,n));	xei = np.zeros((m,n))
	yei = np.zeros((m,n));	zei = np.zeros((m,n))
	
	for i in range(n):
		for j in range(m):
			value = (d_values[0][4] + d_values[2][0]) * np.sin(theta[i])**2 * np.sin(phi[j])**2
			value += (d_values[1][3] + d_values[2][2]) * np.sin(theta[i])**2 * np.cos(phi[j])**2
			value += d_values[2][2] * np.cos(theta[i])**2
			value = value * np.cos(theta[i]) * 1e12
			EE[j,i] = value
			
	EE = EE + abs(EE.min().min())
	
	for i in range(n):
		for j in range(m):
			xei[j,i] = EE[j,i] * np.sin(phi[j]) * np.cos(theta[i])
			yei[j,i] = EE[j,i] * np.sin(phi[j]) * np.sin(theta[i])
			zei[j,i] = EE[j,i] * np.cos(phi[j])
			
	mins = [xei.min().min(),yei.min().min(),zei.min().min()]
	maxs = [xei.max().max(),yei.max().max(),zei.max().max()]
	
	fig = plt.figure()
	#ax = fig.gca(projection='3d')
	ax = fig.add_subplot(111,projection='3d')
	norm = matplotlib.colors.Normalize(vmin=EE.min().min(),vmax=EE.max().max())
	ax.plot_surface(xei,yei,zei,facecolors=plt.cm.rainbow(norm(EE)),rstride=1, cstride=1, linewidth=0, antialiased=False)
	m = cm.ScalarMappable(cmap=plt.cm.rainbow,norm=norm)
	m.set_array([])
	plt.axis('off')
	cbar = plt.colorbar(m,ticks=[EE.min().min(),EE.max().max()], fraction=0.025,pad=0.08,orientation='horizontal')
	plt.xlim(1.05*min(mins),1.05*max(maxs))
	plt.ylim(1.05*min(mins),1.05*max(maxs))
	#plt.zlim(1.05*min(mins),1.05*max(maxs))
	cbar.ax.set_xticklabels(['Low','High'])
	
	# for angle in range(0, 361,2):
		# ax.view_init(0, angle)
		# plt.draw()
		# plt.pause(0.0001)
	
	ax.view_init(0,30)
	plt.savefig('Dielectric_' + Potential +'.png')
	plt.show()
	
def Thin_Film_Piezoelectric_v_Misfit(Misfit_List, Temp, Potential, Elec_Field, Rotate_Flag, Rotation_List, XF, Iterations, OutputFlag):

	d33list = [];	d15list = []
	Misfit_l_List = np.arange(Misfit_List[0],Misfit_List[1]+(Misfit_List[2])/2,Misfit_List[2])
	
	for mstep in Misfit_l_List:
		d_values = Thin_Film_Piezoelectric_Tensor(Temp, [mstep,mstep,0,0,0,0], Elec_Field, Potential, Rotate_Flag, Rotation_List, XF, Iterations, OutputFlag)
		d33list.append(d_values[2][2] * 1e12)
		d15list.append(d_values[0][4] * 1e12)
		On_Screen_Updater('Misfit',round(mstep,4))
		
	Custom_LinePlot(Misfit_l_List.tolist(),[d33list,d15list],'Misfit Strain','Piezoelectric Coefficient (pm/V)',SeriesLabel=['d$_{33}$','d$_{15}$'],MarkerType=['o','s'],LineStyle=[':','-'],SaveFlag=True,SaveName=(Potential + 'Misfit_Piezoelectric'))
	Export_Numerical_Data(d33list,Misfit_l_List,'Misfit Strain',Potential,'Piezoelectric_Misfit',Data2=d15list)
	
def Thin_Film_Polarization_Temp(Temp_List,Potential,Misfit,Elec_Field, Rotate_Flag, Rotation_List, XF,Iterations):
	
	pol_list = []
	p1_list  = [];	p2_list	= [];	p3_list = []
	tem_list = []
	
	t_step = Temp_List[0]
	while t_step <= Temp_List[1]:
		if Rotate_Flag == 1:
			f, equil_polar, Prob = Thin_Film_Rotation(t_step, Misfit, Elec_Field, Rotation_List, Potential, XF, Iterations)
		else:
			f, equil_polar, Prob = Thin_Film(t_step, Misfit, Elec_Field, Potential, XF, Iterations)
		pol_list.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
		p1_list.append(abs(equil_polar[0]))
		p2_list.append(abs(equil_polar[1]))
		p3_list.append(abs(equil_polar[2]))
		tem_list.append(t_step)
		On_Screen_Updater('Temperature (K)',t_step)
		t_step += Temp_List[2]
			
	Custom_LinePlot(tem_list, [pol_list,p1_list,p2_list,p3_list], 'Temperature (K)','Polarization (C/m$^2$)',SeriesLabel=['Total Polarization','P$_1$','P$_2$','P$_3$'],MarkerType=['o','s','>'],LineStyle=['--',':','-'],SaveFlag=True,SaveName=(Potential + 'Temperature_Polarization'))
	
def Bulk_Polarization_Temp(Temp_List, Potential, App_Stress, Elec_Field, XF, Iterations):
	pol_list = []
	p1_list  = [];	p2_list	= [];	p3_list = []
	tem_list = []
	
	t_step = Temp_List[0]
	while t_step <= Temp_List[1]:
		f, equil_polar = Bulk(t_step, App_Stress, Elec_Field, Potential,XF, Iterations)
		domain, equil_polar = Domain_Qualifier(equil_polar,'Bulk')
		pol_list.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
		p1_list.append(abs(equil_polar[0]))
		p2_list.append(abs(equil_polar[1]))
		p3_list.append(abs(equil_polar[2]))
		tem_list.append(t_step)
		On_Screen_Updater('Temperature (K)',t_step)
		t_step += Temp_List[2]
		
	plt.plot(tem_list,pol_list,label='Total Polar')
	plt.plot(tem_list,p1_list,label='P$_1$')
	plt.plot(tem_list,p2_list,label='P$_2$')
	plt.plot(tem_list,p3_list,label='P$_3$')
	plt.xlabel('Temperature (K)')
	plt.ylabel('Polarization C/m$^2$')
	plt.legend()
	plt.ylim(0,max(pol_list)*1.25)
	plt.savefig('P_v_Temp_'+Potential+'.png')
	plt.show()

def Bulk_Polarization_Stress(Stress_List, Potential, Temp, Elec_Field, XF, Iterations):
	
	pol_list = []
	p1_list  = [];	p2_list	= [];	p3_list = []
	stress_list = []
	
	stress_step = Stress_List[0]
	while stress_step <= Stress_List[1]:
		f, equil_polar = Bulk(Temp, [-stress_step,-stress_step,-stress_step,0,0,0], Elec_Field, Potential,XF, Iterations)
		domain, equil_polar = Domain_Qualifier(equil_polar,'Bulk')
		pol_list.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
		p1_list.append(abs(equil_polar[0]))
		p2_list.append(abs(equil_polar[1]))
		p3_list.append(abs(equil_polar[2]))
		stress_list.append(stress_step)
		On_Screen_Updater('Stress (GPa)',stress_step)
		stress_step += Stress_List[2]
		
	plt.plot(stress_list,pol_list,label='Total Polar')
	plt.plot(stress_list,p1_list,label='P$_1$')
	plt.plot(stress_list,p2_list,label='P$_2$')
	plt.plot(stress_list,p3_list,label='P$_3$')
	plt.legend()
	plt.ylabel('Polarization C/m$^2$')
	plt.xlabel('Stress (GPa)')
	plt.ylim(0,max(pol_list)*1.25)
	plt.savefig('P_v_Temp_'+Potential+'.png')
	plt.show()

def Bulk_Piezoelectric_Stress(Stress_List, Potential, Temp, Elec_Field, XF, Iterations):
	
	stress_step = Stress_List[0]
	stress_list = []
	d33list = []
	
	while stress_step <= Stress_List[1]:
		d_values = Bulk_Piezoelectric_Tensor(Temp, [-stress_step,-stress_step,-stress_step,0,0,0], Elec_Field, Potential, XF, Iterations, 0)
		d33list.append(d_values[2][2] * 1e12)
		stress_list.append(stress_step/1e9)
		On_Screen_Updater('Stress (GPa)',stress_step)
		stress_step += Stress_List[2]
		
	plt.scatter(stress_list,d33list)
	plt.xlabel('Stress (GPa)')
	plt.ylabel('Piezoelectric Coefficient d$_{33}$ (pm/V)')
	#plt.savefig(Potential + 'Misfit_v_Piezoelectric.png')
	plt.show()
		
def Bulk_Dielectric_Temp(Temp_List, App_Stress, Elec_Field, Potential, XF, Iterations, OutputFlag):	
	
	k11_list = [];	k22_list = [];	k33_list = []
	tem_list = []
	
	t_step = Temp_List[0]
	while t_step <= Temp_List[1]:
		k_list = Bulk_Dielectric(t_step, App_Stress, Elec_Field, Potential, XF, Iterations, OutputFlag)
		k11_list.append(k_list[0]);	k22_list.append(k_list[1]);	k33_list.append(k_list[2])
		tem_list.append(t_step)
		On_Screen_Updater('Temperature (K)',t_step)
		t_step += Temp_List[2]
		
		
	Custom_LinePlot(tem_list, [k11_list,k22_list,k33_list], 'Temperature (K)','Dielectric Constant',SeriesLabel=['k$_{11}$','k$_{22}$','k$_{33}$'],MarkerType=['o','s','>'],LineStyle=['--',':','-'],SaveFlag=True,SaveName=(Potential + 'Temperature_Dielectric'))
	
def Thin_Film_Dielectric_Temp(Temp_List,Potential,Misfit,Elec_Field, Rotate_Flag, Rotation_List, XF,Iterations):

	k11_list = [];	k22_list = [];	k33_list = []
	tem_list = []
	
	t_step = Temp_List[0]
	while t_step <= Temp_List[1]:
		k_list = Thin_Film_Dielectric(t_step, Misfit, Elec_Field, Potential,Rotate_Flag, Rotation_List, XF, Iterations, 0)
		k11_list.append(k_list[0]);	k22_list.append(k_list[1]);	k33_list.append(k_list[2])
		tem_list.append(t_step)
		On_Screen_Updater('Temperature (K)',t_step)
		t_step += Temp_List[2]
		
		
	Custom_LinePlot(tem_list, [k11_list,k22_list,k33_list], 'Temperature (K)','Dielectric Constant',SeriesLabel=['k$_{11}$','k$_{22}$','k$_{33}$'],MarkerType=['o','s','>'],LineStyle=['--',':','-'],SaveFlag=True,SaveName=(Potential + 'Temperature_Dielectric'))

def Thin_Film_Dielectric_Misfit(Misfit_List, Potential, Temp, Elec_Field, Rotate_Flag, Rotation_List, XF, Iterations):
	
	k11_list = [];	k22_list = [];	k33_list = []
	mis_list = []
	
	m_step = Misfit_List[0]
	while m_step <= Misfit_List[1]:
		k_list = Thin_Film_Dielectric(Temp, [m_step,m_step,0,0,0,0], Elec_Field, Potential, Rotate_Flag, Rotation_List, XF, Iterations)
		k11_list.append(k_list[0]);	k22_list.append(k_list[1]);	k33_list.append(k_list[2])
		mis_list.append(m_step)
		m_step += Misfit_List[2]
		
	Custom_LinePlot(mis_list, [k11_list,k22_list,k33_list], 'Misfit Strain','Dielectric Constant',SeriesLabel=['k$_{11}$','k$_{22}$','k$_{33}$'],MarkerType=['o','s','>'],LineStyle=['--',':','-'],SaveFlag=True,SaveName=(Potential + 'Misfit_Dielectric'))

def Bulk_Dielectric_Stress(Stress_List, Potential, Temp, Elec_Field, XF, Iterations):
	
	k11_list = [];	k22_list = [];	k33_list = []
	Stress_l_List = np.arange(Stress_List[0],Stress_List[1]+Stress_List[2],Stress_List[2])
	
	for stress_step in Stress_l_List:
		k_list = Bulk_Dielectric(Temp, [-stress_step,-stress_step,-stress_step,0,0,0], Elec_Field, Potential, XF, Iterations, 0)
		k11_list.append(k_list[0]);	k22_list.append(k_list[1]);	k33_list.append(k_list[2])
		On_Screen_Updater('Stress',stress_step/1e9)
		
	Custom_LinePlot(Stress_l_List, [k11_list,k22_list,k33_list], 'Stress (GPa)','Dielectric Constant',SeriesLabel=['k$_{11}$','k$_{22}$','k$_{33}$'],MarkerType=['o','s','>'],LineStyle=['--',':','-'],SaveFlag=True,SaveName=(Potential + 'Stress_Dielectric'))
	
def Bulk_Piezoelectric_Temp(Temp_List, App_Stress, Elec_Field, Potential, XF, Iterations):
	
	d33list = [];	tem_list = []
	t_step = Temp_List[0]
	while t_step <= Temp_List[1]:
		d_values = Bulk_Piezoelectric_Tensor(t_step, App_Stress, Elec_Field, Potential, XF, Iterations, 0)
		d33list.append(d_values[2][2] * 1e12)
		tem_list.append(t_step)
		t_step += Temp_List[2]
		
	plt.scatter(tem_list,d33list)
	plt.xlabel('Temperature (K)')
	plt.ylabel('Piezoelectric Coefficient d$_{33}$ (pm/V)')
	#plt.savefig(Potential + 'Misfit_v_Piezoelectric.png')
	plt.show()

def Thin_Film_Piezoelectric_Temp(Temp_List,Potential,Misfit,Elec_Field, Rotate_Flag, Rotation_List, XF,Iterations):

	tem_list = [];	d33list = []
	
	t_step = Temp_List[0]
	while t_step <= Temp_List[1]:
		d_values = Thin_Film_Piezoelectric_Tensor(t_step, Misfit, Elec_Field, Potential, Rotate_Flag, Rotation_List, XF, Iterations, 0)
		d33list.append(d_values[2][2] * 1e12)
		tem_list.append(t_step)
		t_step += Temp_List[2]
		
	plt.scatter(tem_list,d33list)
	plt.xlabel('Temperature (K)')
	plt.ylabel('Piezoelectric Coefficient d$_{33}$ (pm/V)')
	#plt.savefig(Potential + 'Misfit_v_Piezoelectric.png')
	plt.show()

def Thin_Film_Dielectric_Fraction(Temp, Potential, Misfit, Elec_Field, Rotate_Flag, Rotation_List, XF_List, Iterations):

	k11list = [];	k22list = [];	k33list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+(XF_List[2])/2,XF_List[2])
	
	for xf_step in XF_l_List:
		k_list = Thin_Film_Dielectric(Temp, Misfit, Elec_Field, Potential,Rotate_Flag, Rotation_List, xf_step, Iterations, 0)
		k11list.append(k_list[0]);	k22list.append(k_list[1]);	k33list.append(k_list[2])
		
	Custom_LinePlot(XF_l_List.tolist(),[k11list,k22list,k33list],'Material Fraction','Dielectric Constant',SeriesLabel=['k$_{11}$','k$_{22}$','k$_{33}$'],MarkerType=['o','s','<'],LineStyle=[':','-','--'],SaveFlag=True,SaveName=(Potential + 'Fraction_Dielectric'))
	Export_Numerical_Data(k11list,XF_l_List,'Material Fraction',Potential,'Dielectric_Fraction',Data2=k22list,Data3=k33list)

def Bulk_Dielectric_Fraction(Temp, App_Stress, Elec_Field, Potential, XF_List, Iterations):

	k11_list = [];	k22_list = [];	k33_list = []
	x_list = []
	
	xf_step = XF_List[0]
	while xf_step <= XF_List[1]+0.001:
		k_list = Bulk_Dielectric(Temp, App_Stress, Elec_Field, Potential, xf_step, Iterations, 0)
		k11_list.append(k_list[0]);	k22_list.append(k_list[1]);	k33_list.append(k_list[2])
		x_list.append(xf_step)
		xf_step += XF_List[2]
		
	
	#Need to put a check in here for if some values should be zero to make sure it recalculates
	
	Custom_LinePlot(x_list,[k11_list,k22_list,k33_list],'Material Fraction','Dielectric Constant',SeriesLabel=['k$_{11}$','k$_{22}$','k$_{33}$'],MarkerType=['o','v','s'],LineStyle=['-','--',':'],SaveFlag=True,SaveName=(Potential+'_Dielectric'))
	plt.show()

def Thin_Film_Piezoelectric_Fraction(Temp, Potential, Misfit, Elec_Field, Rotate_Flag, Rotation_List, XF_List, Iterations):

	d33list = [];	d15list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	
	for xf_step in XF_l_List:
		d_values = Thin_Film_Piezoelectric_Tensor(Temp, Misfit, Elec_Field, Potential, Rotate_Flag, Rotation_List, xf_step, Iterations, 0)
		d33list.append(d_values[2][2] * 1e12)
		d15list.append(d_values[0][4] * 1e12)
		
	Custom_LinePlot(XF_l_List.tolist(),[d33list,d15list],'Material Fraction','Piezoelectric Coefficient (pm/V)',SeriesLabel=['d$_{33}$','d$_{15}$'],MarkerType=['o','s'],LineStyle=['--',':'],SaveFlag=True,SaveName=(Potential + 'Thin_Film_Fraction_Piezoelectric'))
	
def Thin_Film_Piezoelectric_Fraction_Temp(Temp_List, XF_List, Misfit, Potential, Elec_Field, Rotate_Flag, Rotation_List, Iterations):
	
	D33_Data = [];	D15_Data = [];
	
	XF_l_List = np.arange(XF_List[0],XF_List[1] + XF_List[2],XF_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1] + Temp_List[2],Temp_List[2])
	
	for temp in Temp_l_List:
		temp33_data = [];	temp15_data = [];
		for xf_step in XF_l_List:
			d_values = Thin_Film_Piezoelectric_Tensor(temp, Misfit, Elec_Field, Potential, Rotate_Flag, Rotation_List, xf_step, Iterations,0)
			temp33_data.append(d_values[2][2] * 1e12)
			temp15_data.append(d_values[0][4] * 1e12)
		D33_Data.append(temp33_data)
		D15_Data.append(temp15_data)
		
	Property_Map_Plotting(D33_Data,'Material Fraction','Temperature (K)',XF_List,Temp_List,Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Fraction_Temp_D33'),CLabel='Piezoelectric Coefficient d$_{33}$ (pm/V)')
	Property_Map_Plotting(D15_Data,'Material Fraction','Temperature (K)',XF_List,Temp_List,Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Fraction_Temp_D15'),CLabel='Piezoelectric Coefficient d$_{15}$ (pm/V)')
	Export_Numerical_Data(D33_Data,XF_l_List.tolist(),'Material Fraction',Potential,'Thin_Film_Piezoelectric_Fraction_Temp',YRange=Temp_l_List.tolist(),YLabel='Temperature (K)', Data2=D15_Data)

def Bulk_Piezoelectric_Fraction(Temp, App_Stress, Elec_Field, Potential, XF_List, Iterations):

	d33list = [];	d15list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	
	for xf_step in XF_l_List:
		d_values = Bulk_Piezoelectric_Tensor(Temp, App_Stress, Elec_Field, Potential, xf_step, Iterations, 0)
		d33list.append(d_values[2][2] * 1e12)
		d15list.append(d_values[0][4] * 1e12)
		
	Custom_LinePlot(XF_l_List.tolist(),[d33list,d15list],'Material Fraction','Piezoelectric Coefficient (pm/V)',SeriesLabel=['d$_{33}$','d$_{15}$'],MarkerType=['o','s'],LineStyle=['--',':'],SaveFlag=True,SaveName=(Potential + 'Fraction_Piezoelectric'))

def Bulk_Polarization_Fraction(Temp, App_Stress, Elec_Field, Potential, XF_List, Iterations):

	pol_list = []
	p1_list  = [];	p2_list	= [];	p3_list = []
	x_list = []
	
	xf_step = XF_List[0]
	while xf_step <= XF_List[1]:
		f, equil_polar = Bulk(Temp, App_Stress, Elec_Field, Potential,xf_step, Iterations)
		domain, equil_polar = Domain_Qualifier(equil_polar,'Bulk')
		pol_list.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
		p1_list.append(abs(equil_polar[0]))
		p2_list.append(abs(equil_polar[1]))
		p3_list.append(abs(equil_polar[2]))
		x_list.append(xf_step)
		xf_step += XF_List[2]
		
	plt.plot(x_list,pol_list,label='Total Polar')
	plt.plot(x_list,p1_list,label='P$_1$')
	plt.plot(x_list,p2_list,label='P$_2$')
	plt.plot(x_list,p3_list,label='P$_3$')
	plt.xlabel('Material Fraction')
	plt.ylabel('Polarization C/m$^2$')
	plt.legend()
	plt.ylim(0,max(pol_list)*1.25)
	plt.savefig('P_v_Temp_'+Potential+'.png')
	plt.show()

def Thin_Film_Polarization_Fraction(Temp, Potential, Misfit, Elec_Field, Rotate_Flag, Rotation_List, XF_List, Iterations):

	pol_list = []
	p1_list  = [];	p2_list	= [];	p3_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	
	for xf_step in XF_l_List:
		if Rotate_Flag == 1:
			f, equil_polar, Prob = Thin_Film_Rotation(Temp, Misfit, Elec_Field, Rotation_List, Potential, xf_step, Iterations)
		else:
			f, equil_polar, Prob = Thin_Film(Temp, Misfit, Elec_Field, Potential, xf_step, Iterations)
		pol_list.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
		p1_list.append(abs(equil_polar[0]))
		p2_list.append(abs(equil_polar[1]))
		p3_list.append(abs(equil_polar[2]))
		On_Screen_Updater('Composition',xf_step)
		
	plt.plot(XF_l_List,pol_list,label='Total Polar')
	plt.plot(XF_l_List,p1_list,label='P$_1$')
	plt.plot(XF_l_List,p2_list,label='P$_2$')
	plt.plot(XF_l_List,p3_list,label='P$_3$')
	plt.xlabel('Material Fraction')
	plt.ylabel('Polarization C/m$^2$')
	plt.legend()
	plt.ylim(0,max(pol_list)*1.25)
	plt.savefig('P_v_Temp_'+Potential+'.png')
	plt.show()

def Thin_Film_Polarization_Temp_Misfit(Temp_List,Misfit_List,Potential,Elec_Field,Rotate_Flag, Rotation_List, XF, Iterations):
	
	pol_list	= [];		p1_list = []
	p2_list		= [];		p3_list = []
	Temp_l_List 	= np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	Misfit_l_List	= np.arange(Misfit_List[0],Misfit_List[1]+Misfit_List[2],Misfit_List[2])
	
	for tem in Temp_l_List:
		pol_temp	= [];		p1_temp = []
		p2_temp		= [];		p3_temp = []
		for mis_step in Misfit_l_List:
			if Rotate_Flag == 1:
				f, equil_polar, Prob = Thin_Film_Rotation(tem, [mis_step,mis_step,0,0,0,0], Elec_Field, Rotation_List, Potential, XF, Iterations)
			else:
				f, equil_polar, Prob = Thin_Film(tem, [mis_step,mis_step,0,0,0,0], Elec_Field, Potential, XF, Iterations)
				
			pol_temp.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_temp.append(abs(equil_polar[0]))
			p2_temp.append(abs(equil_polar[1]))
			p3_temp.append(abs(equil_polar[2]))
			On_Screen_Updater('Temperature',tem,Parameter2='Misfit Strain',Parameter2Value=mis_step)
			
		pol_list.append(pol_temp);		p1_list.append(p1_temp)
		p2_list.append(p2_temp);		p3_list.append(p3_temp)
	
	Property_Map_Plotting(pol_list,'Misfit Strain','Temperature (K)',Misfit_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Temp'),CLabel='Magnitude of Polarization')
	Property_Map_Plotting(p1_list,'Misfit Strain','Temperature (K)',Misfit_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Temp'),CLabel='P$_1$')
	Property_Map_Plotting(p2_list,'Misfit Strain','Temperature (K)',Misfit_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Temp'),CLabel='P$_2$')
	Property_Map_Plotting(p3_list,'Misfit Strain','Temperature (K)',Misfit_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Temp'),CLabel='P$_3$')

	
	Export_Numerical_Data(pol_list,Misfit_l_List.tolist(),'Misfit Strain',Potential,'Thin_Film_Polarization_Temp_Misfit',YRange=Temp_l_List.tolist(),YLabel='Temperature (K)',Data2=p1_list,Data3=p2_list,Data4=p3_list)

def Thin_Film_Misfit_Temperature(Misfit_List, Temp_List, Potential, XF, Elec_Field, Rotate_Flag, Rotation_List, Iterations):

	Domain_Data = []
	Mis_l_List = np.arange(Misfit_List[0],Misfit_List[1]+Misfit_List[2],Misfit_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for tem in Temp_l_List:
		temp_data = []
		for mis in Mis_l_List:
			if Rotate_Flag == 1:
				f, equil_polar, Prob = Thin_Film_Rotation(tem, [mis,mis,0,0,0,0], Elec_Field, Rotation_List, Potential, XF, Iterations)
			else:
				f, equil_polar, Prob = Thin_Film(tem, [mis,mis,0,0,0,0], Elec_Field, Potential, XF, Iterations)
			domain,equil_polar = Domain_Qualifier(equil_polar,'Film')
			temp_data.append(domain)
			On_Screen_Updater('Temperature',tem,Parameter2='Misfit Strain',Parameter2Value=mis)
		Domain_Data.append(temp_data)
		
	Phase_Map_Plotting(Domain_Data, 'Misfit Strain', 'Temperature(K)', Misfit_List, Temp_List,System='Film')
	Export_Numerical_Data(Domain_Data, Temp_l_List.tolist(),'Temperature (K)',Potential,'Phases_Thin_Film_Misfit_Temperature',YRange=Mis_l_List.tolist(),YLabel='Misfit Strain')

def Bulk_Polarization_Temperature_Fraction(XF_List, Temp_List, Potential, App_Stress,Elec_Field,Iterations):

	pol_list	= [];		p1_list = []
	p2_list		= [];		p3_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for tem in Temp_l_List:
		pol_temp	= [];		p1_temp = []
		p2_temp		= [];		p3_temp = []
		for xf in XF_l_List:
			f, equil_polar = Bulk(tem, App_Stress, Elec_Field, Potential,xf, Iterations)
			domain, equil_polar = Domain_Qualifier(equil_polar,'Bulk')
			pol_temp.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_temp.append(abs(equil_polar[0]))
			p2_temp.append(abs(equil_polar[1]))
			p3_temp.append(abs(equil_polar[2]))
			On_Screen_Updater('Temperature (K)',tem,Parameter2='Material Fraction',Parameter2Value=xf)
			
		pol_list.append(pol_temp);		p1_list.append(p1_temp)
		p2_list.append(p2_temp);		p3_list.append(p3_temp)
	
	Property_Map_Plotting(pol_list,'Material Fraction','Temperature (K)',XF_List,Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Temp_Fraction_PMag'),CLabel='Magnitude of Polarization')
	Property_Map_Plotting(p1_list,'Material Fraction','Temperature (K)',XF_List,Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Temp_Fraction_P1'),CLabel='P$_1$')
	Property_Map_Plotting(p2_list,'Material Fraction','Temperature (K)',XF_List,Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Temp_Fraction_P2'),CLabel='P$_2$')
	Property_Map_Plotting(p3_list,'Material Fraction','Temperature (K)',XF_List,Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Temp_Fraction_P3'),CLabel='P$_3$')
	
	Export_Numerical_Data(pol_list,Temp_l_List.tolist(),'Temperature (K)',Potential,'Polarization_Temp_Fraction',YRange=XF_l_List.tolist(),YLabel='Material Fraction',Data2=p1_list,Data3=p2_list,Data4=p3_list)	

def Bulk_Piezoelectric_Temperature_Fraction(XF_List, Temp_List, Potential, App_Stress,Elec_Field,Iterations):
	
	D33_Data = [];	D15_Data = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for tem in Temp_l_List:
		temp33_data = [];	temp15_data = []
		for xf in XF_l_List:
			d_values = Bulk_Piezoelectric_Tensor(tem,App_Stress,Elec_Field,Potential, xf, Iterations,0)
			temp33_data.append(d_values[2][2] * 1e12)
			temp15_data.append(d_values[0][4] * 1e12)
		D33_Data.append(temp33_data)
		D15_Data.append(temp15_data)
	
	Property_Map_Plotting(D33_Data, 'Material Fraction', 'Temperature (K)', XF_List, Temp_List,Colormap='rainbow',SaveFlag=True,SaveName='XF_Temp_D33',CLabel='Piezoelectric Coefficient d$_{33}$ (pm/V)')
	Property_Map_Plotting(D15_Data, 'Material Fraction', 'Temperature (K)', XF_List, Temp_List,Colormap='rainbow',SaveFlag=True,SaveName='XF_Temp_D15',CLabel='Piezoelectric Coefficient d$_{15}$ (pm/V)')
	Export_Numerical_Data(D33_Data,Temp_l_List.tolist(),'Temperature (K)',Potential,'Bulk_Piezoelectric_Temperature_Fraction',YRange=XF_l_List.tolist(),YLabel='XF',Data2=D15_Data)

def Bulk_Polarization_Stress_Temperature(Temp_List, Stress_List, Potential, Elec_Field, XF, Iterations):
	
	pol_list	= [];		p1_list = []
	p2_list		= [];		p3_list = []
	Stress_l_List = np.arange(Stress_List[0],Stress_List[1]+Stress_List[2],Stress_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for tem in Temp_l_List:
		pol_temp	= [];		p1_temp = []
		p2_temp		= [];		p3_temp = []
		for stress_step in Stress_l_List:
			f, equil_polar = Bulk(tem, [-stress_step,-stress_step,-stress_step,0,0,0], Elec_Field, Potential,XF, Iterations)
			pol_temp.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_temp.append(abs(equil_polar[0]))
			p2_temp.append(abs(equil_polar[1]))
			p3_temp.append(abs(equil_polar[2]))
			
		pol_list.append(pol_temp);		p1_list.append(p1_temp)
		p2_list.append(p2_temp);		p3_list.append(p3_temp)
			
	Property_Map_Plotting(pol_list,'Stress (GPa)','Temperature (K)',Stress_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Stress_Temp_Pol_Mag'),CLabel='Magnitude of Polarization')
	Property_Map_Plotting(p1_list,'Stress (GPa)','Temperature (K)',Stress_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Stress_Temp_P1'),CLabel='P$_1$')
	Property_Map_Plotting(p2_list,'Stress (GPa)','Temperature (K)',Stress_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Stress_Temp_P2'),CLabel='P$_2$')
	Property_Map_Plotting(p3_list,'Stress (GPa)','Temperature (K)',Stress_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Stress_Temp_P3'),CLabel='P$_3$')
	
	Export_Numerical_Data(pol_list,Stress_l_List.tolist(),'Stress (GPa)',Potential,'Polarization_Stress_Temp',YRange=Temp_l_List.tolist(),YLabel='Temperature (K)',Data2=p1_list,Data3=p2_list,Data4=p3_list)	
	
def Bulk_Polarization_Stress_Fraction(Temp, XF_List, Stress_List, Potential, Elec_Field, Iterations):
	
	pol_list	= [];		p1_list = []
	p2_list		= [];		p3_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Stress_l_List = np.arange(Stress_List[0],Stress_List[1]+Stress_List[2],Stress_List[2])
	
	for xf_step in XF_l_List:
		pol_temp	= [];		p1_temp = []
		p2_temp		= [];		p3_temp = []
		for stress_step in Stress_l_List:
			f, equil_polar = Bulk(Temp, [-stress_step,-stress_step,-stress_step,0,0,0], Elec_Field, Potential,xf_step, Iterations)
			pol_temp.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_temp.append(abs(equil_polar[0]))
			p2_temp.append(abs(equil_polar[1]))
			p3_temp.append(abs(equil_polar[2]))
			
		pol_list.append(pol_temp);		p1_list.append(p1_temp)
		p2_list.append(p2_temp);		p3_list.append(p3_temp)
		
	
	Property_Map_Plotting(pol_list,'Stress (GPa)','Material Fraction',Stress_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Stress_Fraction_Pol_Mag'),CLabel='Magnitude of Polarization')
	Property_Map_Plotting(p1_list,'Stress (GPa)','Material Fraction',Stress_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Stress_Fraction_P1'),CLabel='P$_1$')
	Property_Map_Plotting(p2_list,'Stress (GPa)','Material Fraction',Stress_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Stress_Fraction_P2'),CLabel='P$_2$')
	Property_Map_Plotting(p3_list,'Stress (GPa)','Material Fraction',Stress_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Stress_Fraction_P3'),CLabel='P$_3$')
	
	Export_Numerical_Data(pol_list,Stress_l_List.tolist(),'Stress (GPa)',Potential,'Polarization_Stress_Fraction',YRange=XF_l_List.tolist(),YLabel='Material Fraction',Data2=p1_list,Data3=p2_list,Data4=p3_list)	
	
def Bulk_Piezoelectric_Stress_Fraction(Temp,XF_List,Stress_List,Potential,Elec_Field,Iterations):
	
	D33_Data = [];	D15_Data = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Stress_l_List = np.arange(Stress_List[0],Stress_List[1]+Stress_List[2],Stress_List[2])
	
	for xf_step in XF_l_List:
		temp33_data = [];	temp15_data = []
		for stress_step in Stress_l_List:
			d_values = Bulk_Piezoelectric_Tensor(Temp,[-stress_step,-stress_step,-stress_step,0,0,0],Elec_Field,Potential,xf_step,Iterations,0)
			temp33_data.append(d_values[2][2] * 1e12)
			temp15_data.append(d_values[0][4] * 1e12)
		D33_Data.append(temp33_data)
		D15_Data.append(temp15_data)
		
	Property_Map_Plotting(D33_Data,'Stress (Pa)','Material Fraction',Stress_List,XF_List,Colormap='rainbow',SaveFlag=True,SaveName='XF_Stress_D33',CLabel='Piezoelectric Coefficient d$_33$ (pm/V)')
	Property_Map_Plotting(D15_Data,'Stress (Pa)','Material Fraction',Stress_List,XF_List,Colormap='rainbow',SaveFlag=True,SaveName='XF_Stress_D15',CLabel='Piezoelectric Coefficient d$_15$ (pm/V)')
	Export_Numerical_Data(D33_Data,XF_l_List.tolist(),'Material Fraction',Potential,'Bulk_Piezoelectric_Stress_Fraction',YRange=Stress_l_List.tolist(),YLabel='Stress',Data2=D15_Data)

def Bulk_Dielectric_Temp_Fraction(Temp_List,XF_List,App_Stress, Potential, Elec_Field, Iterations):
	
	k11_list = [];	k22_list = [];	k33_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for tem in Temp_l_List:
		temp_k1 = [];	temp_k2 = [];	temp_k3 = []
		for xf_step in XF_l_List:
			k_list = Bulk_Dielectric(tem,App_Stress,Elec_Field, Potential,xf_step,Iterations,0)
			temp_k1.append(k_list[0]);	temp_k2.append(k_list[1]);
			temp_k3.append(k_list[2])
		k11_list.append(temp_k1)
		k22_list.append(temp_k2)
		k33_list.append(temp_k3)
		
	Property_Map_Plotting(k11_list,'Material Fraction','Temperature (K)', XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Temp_Fraction_K11'),CLabel='Dielectric Constant K$_{11}$')
	Property_Map_Plotting(k22_list,'Material Fraction','Temperature (K)', XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Temp_Fraction_K22'),CLabel='Dielectric Constant K$_{22}$')
	Property_Map_Plotting(k33_list,'Material Fraction','Temperature (K)', XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Temp_Fraction_K33'),CLabel='Dielectric Constant K$_{33}$')
	Export_Numerical_Data(k11_list,XF_l_List.tolist(),'Material Fraction',Potential,'Bulk_Dielectric_Temp_Fraction',YRange=Temp_l_List.tolist(),YLabel='Temperature (K)',Data2=k22_list,Data3=k33_list)

def Bulk_Dielectric_Stress_Fraction(Temp,XF_List,Stress_List,Potential,Elec_Field,Iterations):

	k11_list = [];	k22_list = [];	k33_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Stress_l_List = np.arange(Stress_List[0],Stress_List[1]+Stress_List[2],Stress_List[2])
	
	for xf_step in XF_l_List:
		temp_k1 = [];	temp_k2 = [];	temp_k3 = []
		for stress_step in Stress_l_List:
			k_list = Bulk_Dielectric(Temp, [-stress_step,-stress_step,-stress_step,0,0,0], Elec_Field, Potential, xf_step, Iterations, 0)
			temp_k1.append(k_list[0]);	temp_k2.append(k_list[1]);
			temp_k3.append(k_list[2])
		k11_list.append(temp_k1)
		k22_list.append(temp_k2)
		k33_list.append(temp_k3)
		
	Property_Map_Plotting(k11_list,'Stress (Pa)','Material Fraction',Stress_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Stress_Fraction_K11'))
	Property_Map_Plotting(k22_list,'Stress (Pa)','Material Fraction',Stress_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Stress_Fraction_K22'))
	Property_Map_Plotting(k33_list,'Stress (Pa)','Material Fraction',Stress_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Stress_Fraction_K33'))
	Export_Numerical_Data(k11_list,Stress_l_List.tolist(),'Stress (Pa)',Potential,'Bulk_Dielectric_Stress_Fraction',YRange=XF_l_List.tolist(),YLabel='Material Fraction',Data2=k22_list,Data3=k33_list)

def Thin_Film_Piezoelectric_Misfit_Temp(Temp_List,Misfit_List,Potential,Elec_Field, Rotate_Flag, Rotation_List, XF, Iterations):

	D33_Data = [];	D15_Data = []
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	Mis_l_List = np.arange(Misfit_List[0],Misfit_List[1]+Misfit_List[2],Misfit_List[2])
	
	for tem in Temp_l_List:
		temp33_data = [];	temp15_data = []
		for mis in Mis_l_List:
			d_values = Thin_Film_Piezoelectric_Tensor(tem, [mis,mis,0,0,0,0], Elec_Field, Potential, Rotate_Flag, Rotation_List, XF, Iterations, 0)
			temp33_data.append(d_values[2][2] * 1e12)
			temp15_data.append(d_values[0][4] * 1e12)
			On_Screen_Updater('Temperature',tem,Parameter2='Misfit Strain',Parameter2Value=mis)
		D33_Data.append(temp33_data)
		D15_Data.append(temp15_data)
		
	Property_Map_Plotting(D33_Data,'Misfit Strain','Temperature (K)',Misfit_List,Temp_List,Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Temp_Misfit_D33'))
	Property_Map_Plotting(D15_Data,'Misfit Strain','Temperature (K)',Misfit_List,Temp_List,Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Temp_Misfit_D15'))
	Export_Numerical_Data(D33_Data,Mis_l_List.tolist(),'Misfit Strain',Potential,'Thin_Film_Piezoelectric_Misfit_Temp',YRange=Temp_l_List.tolist(),YLabel='Temperature (K)',Data2=D15_Data)

def Thin_Film_Polarization_Misfit_Misfit(Misfit_X_List,Misfit_Y_List,Temp,Potential,XF,Elec_Field,Rotate_Flag,Rotation_List,Iterations):
	
	pol_list	= [];		p1_list = []
	p2_list		= [];		p3_list = []
	Mis_x_l_List = np.arange(Misfit_X_List[0],Misfit_X_List[1]+(Misfit_X_List[2]/2),Misfit_X_List[2])
	Mis_y_l_List = np.arange(Misfit_Y_List[0],Misfit_Y_List[1]+(Misfit_Y_List[2]/2),Misfit_Y_List[2])
	
	for mis_x in Mis_x_l_List:
		pol_temp	= [];		p1_temp = []
		p2_temp		= [];		p3_temp = []
		for mis_y in Mis_y_l_List:
			if Rotate_Flag == 1:
				f, equil_polar, Prob = Thin_Film_Rotation(Temp, [mis_x,mis_y,0,0,0,0], Elec_Field, Rotation_List, Potential, XF, Iterations)
			else:
				f, equil_polar, Prob = Thin_Film(Temp, [mis_x,mis_y,0,0,0,0], Elec_Field, Potential, XF, Iterations)
			pol_temp.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_temp.append(abs(equil_polar[0]))
			p2_temp.append(abs(equil_polar[1]))
			p3_temp.append(abs(equil_polar[2]))
			On_Screen_Updater('Misfit Strain X',mis_x,Parameter2='Misfit Strain Y',Parameter2Value=mis_y)
			
		pol_list.append(pol_temp);		p1_list.append(p1_temp)
		p2_list.append(p2_temp);		p3_list.append(p3_temp)
		
	Property_Map_Plotting(pol_list,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List, Misfit_Y_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Misfit'),CLabel='Magnitude of Polarization')
	Property_Map_Plotting(p1_list,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List, Misfit_Y_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Misfit'),CLabel='P$_1$')
	Property_Map_Plotting(p2_list,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List, Misfit_Y_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Misfit'),CLabel='P$_2$')
	Property_Map_Plotting(p3_list,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List, Misfit_Y_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Misfit'),CLabel='P$_3$')

	
	Export_Numerical_Data(pol_list,Mis_x_l_List.tolist(),'Misfit Strain (x)',Potential,'Thin_Film_Polarization_Misfit_Misfit',YRange=Mis_y_l_List.tolist(),YLabel='Misfit Strain (y)',Data2=p1_list,Data3=p2_list,Data4=p3_list)
	
def Thin_Film_Piezoelectric_Misfit_Misfit(Misfit_X_List,Misfit_Y_List,Temp,Potential,XF,Elec_Field,Rotate_Flag,Rotation_List,Iterations):

	D33_Data = [];	D15_Data = []
	Mis_x_l_List = np.arange(Misfit_X_List[0],Misfit_X_List[1]+(Misfit_X_List[2]/2),Misfit_X_List[2])
	Mis_y_l_List = np.arange(Misfit_Y_List[0],Misfit_Y_List[1]+(Misfit_Y_List[2]/2),Misfit_Y_List[2])
	
	for mis_x in Mis_x_l_List:
		temp33_data = [];	temp15_data = []
		for mis_y in Mis_y_l_List:
			d_values = Thin_Film_Piezoelectric_Tensor(Temp, [mis_x,mis_y,0,0,0,0], Elec_Field, Potential, Rotate_Flag, Rotation_List, XF, Iterations, 0)
			temp33_data.append(d_values[2][2] * 1e12)
			temp15_data.append(d_values[0][4] * 1e12)
			On_Screen_Updater('Misfit Strain X',mis_x,Parameter2='Misfit Strain Y',Parameter2Value=mis_y)
		D33_Data.append(temp33_data)
		D15_Data.append(temp15_data)
		
	Property_Map_Plotting(D33_Data,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List,Misfit_Y_List,Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Misfit_D33'),CLabel='Piezoelectric Coefficient d$_{33}$ (pm/V)')
	Property_Map_Plotting(D15_Data,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List,Misfit_Y_List,Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Misfit_D15'),CLabel='Piezoelectric Coefficient d$_{15}$ (pm/V)')
	Export_Numerical_Data(D33_Data,Mis_x_l_List.tolist(),'Misfit Strain (x)',Potential,'Thin_Film_Piezoelectric_Misfit_Misfit',YRange=Mis_y_l_List.tolist(),YLabel='Misfit Strain (y)',Data2=D15_Data)

def Thin_Film_Polarization_Misfit_Fraction(XF_List,Misfit_List,Potential,Temp,Elec_Field,Rotate_Flag,Rotation_List, Iterations):

	pol_list	= [];		p1_list = []
	p2_list		= [];		p3_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Mis_l_List = np.arange(Misfit_List[0],Misfit_List[1]+Misfit_List[2],Misfit_List[2])
	
	for xf_step in XF_l_List:
		pol_temp	= [];		p1_temp = []
		p2_temp		= [];		p3_temp = []
		for mis in Mis_l_List:
			if Rotate_Flag == 1:
				f, equil_polar, Prob = Thin_Film_Rotation(Temp, [mis,mis,0,0,0,0], Elec_Field, Rotation_List, Potential, xf_step, Iterations)
			else:
				f, equil_polar, Prob = Thin_Film(Temp, [mis,mis,0,0,0,0], Elec_Field, Potential, xf_step, Iterations)
			pol_temp.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_temp.append(abs(equil_polar[0]))
			p2_temp.append(abs(equil_polar[1]))
			p3_temp.append(abs(equil_polar[2]))
			On_Screen_Updater('Composition',xf_step,Parameter2='Misfit Strain',Parameter2Value=mis)
			
		pol_list.append(pol_temp);		p1_list.append(p1_temp)
		p2_list.append(p2_temp);		p3_list.append(p3_temp)
		
	Property_Map_Plotting(pol_list,'Misfit Strain','Material Fraction',Misfit_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Fraction'),CLabel='Magnitude of Polarization')
	Property_Map_Plotting(p1_list,'Misfit Strain','Material Fraction',Misfit_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Fraction'),CLabel='P$_1$')
	Property_Map_Plotting(p2_list,'Misfit Strain','Material Fraction',Misfit_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Fraction'),CLabel='P$_2$')
	Property_Map_Plotting(p3_list,'Misfit Strain','Material Fraction',Misfit_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Misfit_Fraction'),CLabel='P$_3$')
	
	Export_Numerical_Data(pol_list,Mis_l_List.tolist(),'Misfit Strain',Potential,'Thin_Film_Polarization_Misfit_Fraction',YRange=XF_l_List.tolist(),YLabel='Material Fraction',Data2=p1_list,Data3=p2_list,Data4=p3_list)

def Thin_Film_Polarization_Temperature_Fraction(Temp_List, XF_List, Misfit, Potential, Elec_Field, Rotate_Flag, Rotation_List, Iterations):
	
	pol_list	= [];		p1_list = []
	p2_list		= [];		p3_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for xf_step in XF_l_List:
		pol_temp	= [];		p1_temp = []
		p2_temp		= [];		p3_temp = []
		for tem in Temp_l_List:
			if Rotate_Flag == 1:
				f, equil_polar, Prob = Thin_Film_Rotation(tem, Misfit, Elec_Field, Rotation_List, Potential, xf_step, Iterations)
			else:
				f, equil_polar, Prob = Thin_Film(tem, Misfit, Elec_Field, Potential, xf_step, Iterations)
			pol_temp.append((equil_polar[0]**2 + equil_polar[1]**2 + equil_polar[2]**2)**0.5)
			p1_temp.append(abs(equil_polar[0]))
			p2_temp.append(abs(equil_polar[1]))
			p3_temp.append(abs(equil_polar[2]))
			On_Screen_Updater('Temperature',tem,Parameter2='Composition',Parameter2Value=xf_step)
			
		pol_list.append(pol_temp);		p1_list.append(p1_temp)
		p2_list.append(p2_temp);		p3_list.append(p3_temp)
		
	Property_Map_Plotting(pol_list,'Material Fraction','Temperature (K)',XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Temperature_Fraction_Pol_Mag'),CLabel='Magnitude of Polarization')
	Property_Map_Plotting(p1_list,'Material Fraction','Temperature (K)',XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Temperature_Fraction_P1'),CLabel='P$_1$')
	Property_Map_Plotting(p2_list,'Material Fraction','Temperature (K)',XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Temperature_Fraction_P2'),CLabel='P$_2$')
	Property_Map_Plotting(p3_list,'Material Fraction','Temperature (K)',XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Polarization_Temperature_Fraction_P3'),CLabel='P$_3$')
	
	Export_Numerical_Data(pol_list,XF_l_List.tolist(),'Misfit Strain',Potential,'Thin_Film_Polarization_Temperature_Fraction',YRange=Temp_l_List.tolist(),YLabel='Temperature (K)',Data2=p1_list,Data3=p2_list,Data4=p3_list)
	
def Thin_Film_Dielectric_Misfit_Fraction(XF_List,Misfit_List,Potential,Temp,Elec_Field,Rotate_Flag,Rotation_List, Iterations):

	k11_list = [];	k22_list = [];	k33_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Mis_l_List = np.arange(Misfit_List[0],Misfit_List[1]+Misfit_List[2],Misfit_List[2])
	
	for xf_step in XF_l_List:
		temp_k1 = [];	temp_k2 = [];	temp_k3 = []
		for mis in Mis_l_List:
			k_list = Thin_Film_Dielectric(Temp,[mis,mis,0,0,0,0],Elec_Field,Potential,Rotate_Flag,Rotation_List,xf_step,Iterations,0)
			temp_k1.append(k_list[0]);	temp_k2.append(k_list[1]);
			temp_k3.append(k_list[2])
			On_Screen_Updater('Composition',xf_step,Parameter2='Misfit Strain',Parameter2Value=mis)
		k11_list.append(temp_k1)
		k22_list.append(temp_k2)
		k33_list.append(temp_k3)
		
	Property_Map_Plotting(k11_list,'Misfit Strain','Material Fraction',Misfit_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Fraction_K11'),CLabel='Dielectric Constant K$_{11}$')
	Property_Map_Plotting(k22_list,'Misfit Strain','Material Fraction',Misfit_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Fraction_K33'),CLabel='Dielectric Constant K$_{22}$')
	Property_Map_Plotting(k33_list,'Misfit Strain','Material Fraction',Misfit_List, XF_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Fraction_K33'),CLabel='Dielectric Constant K$_{33}$')
	Export_Numerical_Data(k11_list,Mis_l_List.tolist(),'Misfit Strain',Potential,'Thin_Film_Dielectric_Misfit_Fraction',YRange=XF_l_List.tolist(),YLabel='Material Fraction',Data2=k22_list,Data3=k33_list)

def Thin_Film_Piezoelectric_Misfit_Fraction(XF_List,Misfit_List,Potential,Temp,Elec_Field,Rotate_Flag,Rotation_List, Iterations):

	D33_Data = [];	D15_Data = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Mis_l_List = np.arange(Misfit_List[0],Misfit_List[1]+Misfit_List[2],Misfit_List[2])
	
	for xf_step in XF_l_List:
		temp33_data = [];	temp15_data = []
		for mis in Mis_l_List:
			d_values = Thin_Film_Piezoelectric_Tensor(Temp, [mis,mis,0,0,0,0], Elec_Field, Potential, Rotate_Flag, Rotation_List, xf_step, Iterations, 0)
			temp33_data.append(d_values[2][2] * 1e12)
			temp15_data.append(d_values[0][4] * 1e12)
			On_Screen_Updater('Composition',xf_step,Parameter2='Misfit Strain',Parameter2Value=mis)
		D33_Data.append(temp33_data)
		D15_Data.append(temp15_data)
	
	Property_Map_Plotting(D33_Data,'Misfit Strain','Material Fraction',Misfit_List,XF_List,Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Fraction_Misfit_D33'),CLabel='Piezoelectric Coefficient d$_{33}$ (pm/V)')
	Property_Map_Plotting(D15_Data,'Misfit Strain','Material Fraction',Misfit_List,XF_List,Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Fraction_Misfit_D15'),CLabel='Piezoelectric Coefficient d$_{15}$ (pm/V)')
	Export_Numerical_Data(D33_Data,Mis_l_List.tolist(),'Misfit Strain',Potential,'Thin_Film_Piezoelectric_Misfit_Fraction',YRange=XF_l_List.tolist(),YLabel='Material Fraction',Data2=D15_Data)

def Thin_Film_Dielectric_Misfit_Misfit(Misfit_X_List,Misfit_Y_List,Temp,Potential,XF,Elec_Field,Rotate_Flag,Rotation_List,Iterations):
	
	k11_list = [];	k22_list = [];	k33_list = []
	Mis_x_l_List = np.arange(Misfit_X_List[0],Misfit_X_List[1]+(Misfit_X_List[2]/2),Misfit_X_List[2])
	Mis_y_l_List = np.arange(Misfit_Y_List[0],Misfit_Y_List[1]+(Misfit_Y_List[2]/2),Misfit_Y_List[2])
	
	for mis_x in Mis_x_l_List:
		temp_k1 = [];	temp_k2 = [];	temp_k3 = []
		for mis_y in Mis_y_l_List:
			k_list = Thin_Film_Dielectric(Temp,[mis_x,mis_y,0,0,0,0],Elec_Field,Potential,Rotate_Flag,Rotation_List,XF,Iterations,0)
			temp_k1.append(k_list[0]);	temp_k2.append(k_list[1]);
			temp_k3.append(k_list[2])
			On_Screen_Updater('Misfit Strain X',mis_x,Parameter2='Misfit Strain Y',Parameter2Value=mis_y)
		k11_list.append(temp_k1)
		k22_list.append(temp_k2)
		k33_list.append(temp_k3)
		
	Property_Map_Plotting(k11_list,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List, Misfit_Y_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Misfit_K11'),CLabel='Dielectric Constant K$_{11}$')
	Property_Map_Plotting(k22_list,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List, Misfit_Y_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Misfit_K33'),CLabel='Dielectric Constant K$_{22}$')
	Property_Map_Plotting(k33_list,'Misfit Strain (x)','Misfit Strain (y)',Misfit_X_List, Misfit_Y_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Misfit_K33'),CLabel='Dielectric Constant K$_{33}$')
	Export_Numerical_Data(k11_list,Mis_x_l_List.tolist(),'Misfit Strain (x)',Potential,'Thin_Film_Dielectric_Misfit_Misfit',YRange=Mis_x_l_List.tolist(),YLabel='Misfit Strain (y)',Data2=k22_list,Data3=k33_list)

def Thin_Film_Dielectric_Misfit_Temp(Temp_List,Misfit_List,Potential,XF,Elec_Field,Rotate_Flag,Rotation_List, Iterations):
	
	k11_list = [];	k22_list = [];	k33_list = []
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	Mis_l_List = np.arange(Misfit_List[0],Misfit_List[1]+Misfit_List[2],Misfit_List[2])
	
	for tem in Temp_l_List:
		temp_k1 = [];	temp_k2 = [];	temp_k3 = []
		for mis in Mis_l_List:
			k_list = Thin_Film_Dielectric(tem,[mis,mis,0,0,0,0],Elec_Field,Potential, Rotate_Flag,Rotation_List,XF,Iterations,0)
			temp_k1.append(k_list[0]);	temp_k2.append(k_list[1]);
			temp_k3.append(k_list[2])
			On_Screen_Updater('Temperature',tem,Parameter2='Misfit Strain',Parameter2Value=mis)
		k11_list.append(temp_k1)
		k22_list.append(temp_k2)
		k33_list.append(temp_k3)
	
	Property_Map_Plotting(k11_list,'Misfit Strain','Temperature (K)',Misfit_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Fraction_K11'))
	Property_Map_Plotting(k22_list,'Misfit Strain','Temperature (K)',Misfit_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Fraction_K22'))
	Property_Map_Plotting(k33_list,'Misfit Strain','Temperature (K)',Misfit_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Misfit_Fraction_K33'))
	Export_Numerical_Data(k11_list,Temp_l_List.tolist(),'Temperature (K)',Potential,'Thin_Film_Dielectric_Misfit_Temp',YRange=Mis_l_List.tolist(),YLabel='Misfit Strain',Data2=k22_list,Data3=k33_list)

def Thin_Film_Dielectric_Fraction_Temp(XF_List, Temp_List, Potential, Misfit, Elec_Field, Rotate_Flag, Rotation_List, Iterations):

	k11_list = [];	k22_list = [];	k33_list = []
	XF_l_List = np.arange(XF_List[0],XF_List[1]+XF_List[2],XF_List[2])
	Temp_l_List = np.arange(Temp_List[0],Temp_List[1]+Temp_List[2],Temp_List[2])
	
	for tem in Temp_l_List:
		temp_k1 = [];	temp_k2 = [];	temp_k3 = []
		for xf_step in XF_l_List:
			k_list = Thin_Film_Dielectric(tem, Misfit, Elec_Field, Potential, Rotate_Flag, Rotation_List, xf_step, Iterations,0)
			temp_k1.append(k_list[0]);	temp_k2.append(k_list[1]);
			temp_k3.append(k_list[2])
			On_Screen_Updater('Temperature',tem,Parameter2='Composition',Parameter2Value=xf_step)
		k11_list.append(temp_k1)
		k22_list.append(temp_k2)
		k33_list.append(temp_k3)
		
	Property_Map_Plotting(k11_list,'Material Fraction','Temperature',XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Temp_Fraction_K11'),CLabel='Dielectric Constant K$_{11}$')
	Property_Map_Plotting(k22_list,'Material Fraction','Temperature',XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Temp_Fraction_K22'),CLabel='Dielectric Constant K$_{22}$')
	Property_Map_Plotting(k33_list,'Material Fraction','Temperature',XF_List, Temp_List, Colormap='rainbow',SaveFlag=True,SaveName=(Potential + '_Temp_Fraction_K33'),CLabel='Dielectric Constant K$_{33}$')
	Export_Numerical_Data(k11_list,XF_l_List.tolist(),'Material Fraction',Potential,'Thin_Film_Dielectric_Fraction_Temp',YRange=Temp_l_List.tolist(),YLabel='Temperature (K)',Data2=k22_list,Data3=k33_list)

def Thin_Film_PolyDomain(Temp, Misfit, Elec_Field, Potential, XF, Iterations):

	#Let's make a general assumption of only 2 phases (this will be expanded to "N" phases later (with the constraint of (N<=10)
	"""
	Issues I am facing with regard to the polydomain model. 
	
	1. Need to revamp the solver. The single domain configuration will always be the most lowest energy state, thus the solver will just make p1_1 = p1_2, etc. So how do I avoid that?
	
	2. Getting overflow problems but that that is due to the larger numbers involved. I wonder if we can compute the function, then grab the biggest coefficient, and normalize everything to the largest coefficient.
	
	We know that when the partial derivatives of every available variable is = 0 then we will be at minimum. Therefore, can we solve this system in that sense?
	"""
	
	#We need to set up symbols for "N" different phases/domains, to do this we will specify such that V(Number)_(Phase)
	
	#According to V.G. Koukhar's work, Sn_1 == Sn_2, thus Sn_1 == Sn_2 == Sm
	
	p1_1, p2_1, p3_1, s1_1, s2_1, s3_1, s4_1, s5_1, s6_1, e1_1, e2_1, e3_1 = sp.symbols('p1_1 p2_1 p3_1 s1_1 s2_1 s3_1 s4_1 s5_1 s6_1 e1_1 e2_1 e3_1')
	p1_2, p2_2, p3_2, s1_2, s2_2, s3_2, s4_2, s5_2, s6_2, e1_2, e2_2, e3_2 = sp.symbols('p1_2 p2_2 p3_2 s1_2 s2_2 s3_2 s4_2 s5_2 s6_2 e1_2 e2_2 e3_2')
	
	phase_frac = sp.Symbol('PF')
	
	a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, Temp, Misfit,XF)
	
	Land1, Elas1, QEn1, Elec1 = Setup_Landau(a_values, c_values, s_values, q_values, p1_1, p2_1, p3_1, s1_1, s2_1, s3_1, s4_1, s5_1, s6_1, e1_1, e2_1, e3_1)
	
	Land2, Elas2, QEn2, Elec2 = Setup_Landau(a_values, c_values, s_values, q_values, p1_2, p2_2, p3_2, s1_2, s2_2, s3_2, s4_2, s5_2, s6_2, e1_2, e2_2, e3_2)
	
	Gibbs1 = Land1 - Elas1 - QEn1
	Gibbs2 = Land2 - Elas2 - QEn2
	
	Prob1 = Land1 + Elas1 - Elec1
	Prob2 = Land2 + Elas2 - Elec2
	
	#Now we have to rotate p1_1,p2_1,p3_1,p1_2,p2_2,p3_2,s1_1,s1_2,s1_3,s1_4,s1_5,s1_6,s1_2,s2_2,s3_2,s4_2,s5_2,s6_2
	
	phi, theta, psi = sp.symbols('Phi Theta Psi')
	
	A_1ij = sp.Matrix([[sp.cos(phi),sp.sin(phi),0],[-sp.sin(phi),sp.cos(phi),0],[0,0,1]])
	A_2ij = sp.Matrix([[1,0,0],[0,sp.cos(theta),sp.sin(theta)],[0,-sp.sin(theta),sp.cos(theta)]])
	A_3ij = sp.Matrix([[sp.cos(psi),sp.sin(psi),0],[-sp.sin(psi),sp.cos(psi),0],[0,0,1]])
	
	#m = A_3ij * A_2ij * A_1ij
	
	m = A_3ij.subs([(psi,np.deg2rad(0))]).evalf() * A_2ij.subs([(theta,np.deg2rad(315))]).evalf() * A_1ij.subs([(phi,np.deg2rad(0))]).evalf()
	
	m = m.transpose()
	
	print('Domain Wall Orientation: ', m * sp.Matrix([0,0,1]))
	print('Crystallographic Reference Frame: ', m.transpose() * sp.Matrix([0,1,1]))
	
	
	stress_1 = sp.Matrix([[s1_1,s6_1,s5_1],[s6_1,s2_1,s4_1],[s5_1,s4_1,s3_1]])
	stress_2 = sp.Matrix([[s1_2,s6_2,s5_2],[s6_2,s2_2,s4_2],[s5_2,s4_2,s3_2]])
	polar_1 = sp.Matrix([p1_1,p2_1,p3_1])
	polar_2 = sp.Matrix([p1_2,p2_2,p3_2])
	
	stress_1 = m * stress_1 * m.transpose()
	stress_2 = m * stress_2 * m.transpose()
	polar_1 = m.transpose() * polar_1
	polar_2 = m.transpose() * polar_2
	
	Gibbs_1_Transformed = Gibbs1.subs({p1_1:polar_1[0],p2_1:polar_1[1],p3_1:polar_1[2],s1_1:stress_1[0,0],s2_1:stress_1[1,1],s3_1:stress_1[2,2],s4_1:stress_1[1,2],s5_1:stress_1[0,2],s6_1:stress_1[0,1]})
	Gibbs_2_Transformed = Gibbs2.subs({p1_2:polar_2[0],p2_2:polar_2[1],p3_2:polar_2[2],s1_2:stress_2[0,0],s2_2:stress_2[1,1],s3_2:stress_2[2,2],s4_2:stress_2[1,2],s5_2:stress_2[0,2],s6_2:stress_2[0,1]})

	#Total Free Energy of the system
	Total_Prob = phase_frac * Prob1 + (1-phase_frac) * Prob2
	
	Total_Prob = Total_Prob.subs({e1_1:0,e1_2:0,e2_2:0,e2_1:0,e3_1:0,e3_2:0})
	
	"""
	A great deal of papers on this topic fall back to the original derivations performed by Koukhar and Pertsev, where they outline as many as 18 different equality conditions that can lead to deriving the necessary strain conditions.
	
	Let's first consider this from a macroscopic point of view. 
	
	Assuming the film as a whole must conform to the film constraints, then the average strain across the sample must agree with the misfit epitaxial strain as generally expected. The same goes the stress conditions. However, microscopically that can differ but by the definition and property of domain walls, we can derive those conditions as they apply. Thus it is possible to confirm and derive the necessary conditions and components.
	"""
	
	#We now begin to define the necessary equations for solving
	equations = [
		#First the average macroscopic strains (aka SUM(dG/ds...))
		phase_frac * -Gibbs1.diff(s1_1) + (1-phase_frac) * -Gibbs2.diff(s1_2) - Misfit[0],
		phase_frac * -Gibbs1.diff(s2_1) + (1-phase_frac) * -Gibbs2.diff(s2_2) - Misfit[1],
		phase_frac * -Gibbs1.diff(s6_1) + (1-phase_frac) * -Gibbs2.diff(s6_2) - Misfit[2],
		#We also know that the general stress along/in the "Z" direction must be zero thus leading to
		phase_frac * s3_1 + (1-phase_frac) * s3_2,
		phase_frac * s4_1 + (1-phase_frac) * s4_2,
		phase_frac * s5_1 + (1-phase_frac) * s5_2,
		Gibbs1.diff(s1_1).subs({s1_1:stress_1[0,0]}) - Gibbs2.diff(s1_2).subs({s1_2:stress_2[0,0]}),
		Gibbs1.diff(s2_1).subs({s2_1:stress_1[1,1]}) - Gibbs2.diff(s2_2).subs({s2_2:stress_2[1,1]}),
		Gibbs1.diff(s6_1).subs({s6_1:stress_1[0,2]}) - Gibbs2.diff(s6_2).subs({s6_2:stress_2[0,1]}),
		stress_1[2,2] - stress_2[2,2],
		stress_1[1,2] - stress_2[1,2],
		stress_1[0,2] - stress_2[0,2]
		# #Continuing we now say that the three principal strains in microscopic view must be equal
		# Gibbs_1_Transformed.diff(s1_1).simplify() - Gibbs_2_Transformed.diff(s2_1).simplify(),
		# Gibbs_1_Transformed.diff(s2_1).simplify() - Gibbs_2_Transformed.diff(s2_2).simplify(),
		# Gibbs_1_Transformed.diff(s6_1).simplify() - Gibbs_2_Transformed.diff(s6_2).simplify(),
		# #Lastly, we define the microscopic strain points of view and their conditions, which is basically the inverse of the general stresses
		# stress_1[2,2].simplify() - stress_2[2,2].simplify(),
		# stress_1[1,2].simplify() - stress_2[1,2].simplify(),
		# stress_1[0,2].simplify() - stress_2[0,2].simplify()
	]
	
	solutions = sp.solve(equations,[s1_1,s2_1,s3_1,s4_1,s5_1,s6_1,s1_2,s2_2,s3_2,s4_2,s5_2,s6_2])
	
	for key in solutions.keys():
		# print(key)
		# print(solutions[key])
		# print('')
		Total_Prob = Total_Prob.subs({key:solutions[key]})
	
	# print(Total_Prob)
	# print('')
	
	newProb = sp.lambdify([p1_1,p2_1,p3_1,p1_2,p2_2,p3_2,phase_frac],Total_Prob,'numpy')

	ener, equil_polar, time_to_compute = Solver_Polydomain(newProb, p0, 500)
	
	# print(time_to_compute)
	# print(ener)
	# print(equil_polar)
	
	print('Polar State 1: ', round(equil_polar[0],4),round(equil_polar[1],4),round(equil_polar[2],4))
	print('Polar State 2: ', round(equil_polar[3],4),round(equil_polar[4],4),round(equil_polar[5],4))
	print('Phase Fraction of Polar State 1: ', round(equil_polar[6],4))
	print('Total Energy of the System: ', ener)
	print('Time to Compute was: ', time_to_compute,' seconds')
	
def Solver_Polydomain(Prob, PolarSet, Iterations):
	
	p0 = PolarSet
	bounds = [(1e-10,p0),(1e-10,p0),(1e-10,p0),(1e-10,p0),(1e-10,p0),(1e-10,p0),(0.1,0.9)]
	popsize = 20
	iters = Iterations
	crossp = 0.7
	mut = 0.6
	
	domain_flag = 1
	
	#What if we create a differential evolution algorithm that checks each population individual to 
	#ensure that they consist of two different domains. This should help to ensure that the algorithm
	#goes down the correct "rabbit hole" for finding the best solution
	
	while domain_flag == 1:
		pop = np.random.rand(popsize,len(bounds))
		min_b, max_b = np.asarray(bounds).T
		diff = np.fabs(min_b - max_b)
		pop_denorm = min_b + pop * diff
		for j in pop_denorm:
			print(Domain_Qualifier(j[:3],'Film'))
		domain_flag = 0
	
	fitness = np.asarray([Prob(ind[0],ind[1],ind[2],ind[3],ind[4],ind[5],ind[6]) for ind in pop_denorm])
	best_idx = np.argmin(fitness)
	best = pop_denorm[best_idx]
	
	quit()
	
	start = time.time()
	
	for i in range(iters):
		for j in range(popsize):
			idxs = [idx for idx in range(popsize) if idx != j]
			a,b,c = pop[np.random.choice(idxs,3,replace=False)]
			mutant = np.clip(a + mut * (b-c), 0, 1)
			cross_points = np.random.rand(len(bounds)) < crossp
			if not np.any(cross_points):
				cross_points[np.random.randint(0,len(bounds))] = True
			trial = np.where(cross_points,mutant,pop[j])
			trial_denorm = min_b + trial * diff
			#print(i,trial_denorm[6])
			f = Prob(trial_denorm[0],trial_denorm[1],trial_denorm[2],trial_denorm[3],trial_denorm[4],trial_denorm[5],trial_denorm[6])
			if f < fitness[j]:
				fitness[j] = f
				pop[j] = trial
				if f < fitness[best_idx]:
					best_idx = j
					best = trial_denorm
					best_ener = f
	
	end = time.time()
	equil_polar = best
	
	return best_ener, equil_polar, (end - start)

		
#These aren't quite Journal Quality Yet but we are getting there.
#Need to complete them a little bit more but closer than before.
#Need to Create a Data Output Function as well.

def On_Screen_Updater(Parameter1,Parameter1Value,Parameter2='',Parameter2Value=''):
	
	if Parameter2 == '':
		text_to_print = 'Completed: ' + Parameter1 + ' = ' + str(Parameter1Value)
	else:
		text_to_print = 'Completed: ' + Parameter1 + ' = ' + str(round(Parameter1Value,4)) + ' and ' + Parameter2 + ' = ' + str(round(Parameter2Value,4))
	
	print(text_to_print)

def Custom_LinePlot(X_List, Y_Lists, XLabel, YLabel, SeriesLabel=[''], MarkerType=['o'], LineStyle=['--'], SaveFlag=0, SaveName=''):
	
	from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
	ymin = 0;	ymax = 0;	ymin_round_digits = -3;	ymax_round_digits = -3
	
	for ylist, ylabel, m_type, l_type in zip(Y_Lists, SeriesLabel, MarkerType, LineStyle):
		if max(ylist) > ymax:
			ymax = max(ylist)
		if min(ylist) < ymin:
			ymin = min(ylist)
		plt.plot(X_List,ylist,label=ylabel, marker=m_type, ls=l_type)
		plt.minorticks_on()
	
	#Rounding of the minimums and maximums
	if ymax > 100:
		ymax = round(ymax,-2)
	if ymax > 1000:
		ymax = round(ymax,-3)
	if ymax > 10000:
		ymax = round(ymax,-4)
	if ymax > 100000:
		ymax = 10000
	if ymin < -1000:
		ymin = round(ymin,-3)
	if ymin < -10000:
		ymin = round(ymin,-4)
	if ymin < -100000:
		ymin = -10000
	
	plt.xticks(np.linspace(round(min(X_List),2),round(max(X_List),2),5))
	plt.yticks(np.linspace(ymin,ymax,5))
	plt.tick_params(axis='both',direction='inout',length=8,width=1,right=True,top=True)
	plt.tick_params(axis='both',direction='in',which='minor',length=3,width=1,right=True,top=True)
	plt.xlabel(XLabel,fontweight='bold',fontvariant='small-caps',fontsize=14)
	plt.ylabel(YLabel,fontweight='bold',fontvariant='small-caps',fontsize=14)
	plt.ylim(ymin,ymax)
	plt.legend(loc='upper right')
	
	if SaveFlag == True:
		plt.savefig(SaveName + '.png',dpi=300)
	plt.show()
	
def Custom_ScatterPlot(X_List, Y_Lists, XLabel, YLabel, SeriesLabel=[''], MarkerType=['o'], SaveFlag=0, SaveName=''):

	from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
	
	fig, ax = plt.subplots()
	
	for ylist, ylabel, m_type, l_type in zip(Y_Lists, SeriesLabel, MarkerType, LineStyle):
		ax.plot(X_List,ylist,label=ylabel,marker=m_type,ls=l_type)
		ax.xaxis.set_major_locator(MultipleLocator(0.05))
		ax.xaxis.set_minor_locator(MultipleLocator(0.01))
		ax.tick_params(direction='inout')
		#tick params on the y-axis as well
		
	maxs = [];	mins = []
	for list in Y_Lists:
		maxs.append(max(list));		mins.append(min(list))

	summer = 0
	for v in maxs:
		summer += v
	
	v = summer/len(maxs)
	
	if max(maxs)/v > 2:
		plt.ylim(0,v)
	else:
		plt.ylim(min(mins),max(maxs))
		
	plt.xlim(min(X_List),max(X_List))
	plt.xlabel(XLabel)
	plt.ylabel(YLabel)
	plt.legend()
	
	if SaveFlag == True:
		plt.savefig(SaveName + '.png',dpi=300)

def Property_Map_Plotting(Data, XLabel, YLabel, XTicks, YTicks,SaveFlag=False, SaveName='', CLabel='Piezoelectric Coefficient d$_{33}$ (pm/V)',Colormap='viridis'):
	
	Data = np.asarray(Data,dtype='float')
	
	x, y = np.meshgrid(np.arange(XTicks[0],XTicks[1]+XTicks[2],XTicks[2]),np.arange(YTicks[0],YTicks[1]+YTicks[2],YTicks[2]))
	
	plt.pcolor(x,y,Data,cmap=Colormap)
	# norm_visual = matplotlib.colors.Normalize(vmin=Data.min().min(),vmax=Data.min().min())
	# m = cm.ScalarMappable(cmap=Colormap,norm=norm_visual)
	# m.set_array([])
	#cbar = plt.colorbar(m)
	cbar_ticks = np.linspace(min(0,round(Data.min().min(),2)),round(Data.max().max(),2),5)
	cbar = plt.colorbar(ticks=cbar_ticks.tolist())
	cbar.set_label(CLabel, fontweight='bold',fontvariant='small-caps',fontsize=14)
	plt.xticks(np.linspace(XTicks[0],XTicks[1],5))
	plt.yticks(np.linspace(YTicks[0],YTicks[1],5))
	plt.minorticks_on()
	plt.tick_params(axis='both',direction='inout',length=8,width=1,right=True,top=True)
	plt.tick_params(axis='both',direction='in',which='minor',length=3,width=1,right=True,top=True)
	plt.xlabel(XLabel, fontweight='bold',fontvariant='small-caps',fontsize=14)
	plt.ylabel(YLabel, fontweight='bold',fontvariant='small-caps',fontsize=14)
	
	if SaveFlag == True:
		plt.savefig(SaveName + '.png',dpi=300)

	plt.show()
	
def Phase_Map_Plotting(Data, XLabel, YLabel, XTicks, YTicks, System='Bulk',SaveFlag=False, SaveName=''):

	#The Data will be setup into a list of lists. We will here convert from list of lists to a numpy array
	
	domain_array = np.asarray(Data)
	
	unique_domains = np.unique(domain_array)
	num_uni_domains = len(unique_domains)
	domain_dict = {}
	for d in range(num_uni_domains):
		domain_dict[unique_domains[d]] = d
	
	size_factor = int(len(Data)) * int(len(Data[0]))
	
	
	x, y = np.meshgrid(np.arange(XTicks[0],XTicks[1]+XTicks[2],XTicks[2]),np.arange(YTicks[0],YTicks[1]+YTicks[2],YTicks[2]))

	#We only have (currently) a maximum of 6 Domain Variants for Bulk:
		#Tet, Orthorhombic, Rhomo, Mono, Tri, Cubic
	#We only have (currently) a maximum of 10 Domain Variants for Films:
		#A1, A2, C, O12, O13, O23, Rhomo, Mono, Tri, Cubic
	#Therefore, lets create two new colormaps one for bulk and one for films
	
	if System == 'Bulk':
		domain_array[domain_array == 'A1'] 		= 1	
		domain_array[domain_array == 'A2'] 		= 1
		domain_array[domain_array == 'C'] 		= 1
		domain_array[domain_array == 'O12'] 	= 3
		domain_array[domain_array == 'O13'] 	= 3
		domain_array[domain_array == 'O23'] 	= 3
		domain_array[domain_array == 'R']		= 5
		domain_array[domain_array == 'Ma']		= 2
		domain_array[domain_array == 'Mb']		= 2
		domain_array[domain_array == 'Mca']		= 4
		domain_array[domain_array == 'Mcb']		= 4
		domain_array[domain_array == 'Mab']		= 4
		domain_array[domain_array == 'Cubic']	= 6
		
		domain_array = domain_array.astype('int')
		
		fig = plt.figure()
		ax = fig.add_subplot(111)
		plt.scatter(x,y,c=domain_array,marker='s',cmap=Generate_Discrete_HeatMap(6))
		cbar = plt.colorbar(ticks=[1.5,2.5,3.5,4.5,5.5,6.5])
		plt.clim(1,7)
		cbar.set_ticklabels(['Tetragonal','Monoclinic','Orthorhombic','Monoclinic','Rhomohedral','Cubic'])
		
	if System == 'Film':
		for udomain in range(num_uni_domains):
			domain_array[domain_array == unique_domains[udomain]] = udomain
		# domain_array[domain_array == 'A1'] 		= 1 #Rearrange these numbers to help with breaking everything down	
		# domain_array[domain_array == 'A2'] 		= 2
		# domain_array[domain_array == 'C'] 		= 3
		# domain_array[domain_array == 'O12'] 	= 4
		# domain_array[domain_array == 'O13'] 	= 5
		# domain_array[domain_array == 'O23'] 	= 6
		# domain_array[domain_array == 'R']		= 7
		# domain_array[domain_array == 'Ma']		= 8
		# domain_array[domain_array == 'Mb']		= 9
		# domain_array[domain_array == 'Mca']		= 10
		# domain_array[domain_array == 'Mcb']		= 11
		# domain_array[domain_array == 'Mab']		= 12
		# domain_array[domain_array == 'Cubic']	= 13
		
		domain_array = domain_array.astype('int')
		
		mticks = []
		for udomain in range(num_uni_domains):
			mticks.append(udomain + 0.5)
		
		fig = plt.figure()
		ax = fig.add_subplot(111)
		plt.scatter(x,y,c=domain_array,marker='s',s=10000/size_factor,cmap=Generate_Discrete_HeatMap(num_uni_domains,base_cmap='gist_rainbow'))
		#cbar = plt.colorbar(ticks=[1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,10.5,11.5,12.5,13.5])
		cbar = plt.colorbar(ticks=mticks)
		plt.clim(0,num_uni_domains)
		plt.xticks(np.linspace(XTicks[0],XTicks[1],5))
		plt.yticks(np.linspace(YTicks[0],YTicks[1],5))
		plt.minorticks_on()
		plt.tick_params(axis='both',direction='inout',length=8,width=1,right=True,top=True)
		plt.tick_params(axis='both',direction='in',which='minor',length=3,width=1,right=True,top=True)
		cbar.set_ticklabels(unique_domains)
		#cbar.set_ticklabels(['A1','A2','C','O12','O13','O23','R','Ma', 'Mb','Mca','Mcb','Mab','Cubic'])
	
	plt.xlim(XTicks[0],XTicks[1])
	plt.ylim(YTicks[0],YTicks[1])
	plt.xlabel(XLabel, fontweight='bold',fontvariant='small-caps',fontsize=14)
	plt.ylabel(YLabel, fontweight='bold',fontvariant='small-caps',fontsize=14)
	
	if SaveFlag == True:
		plt.savefig(SaveName + '.png',dpi=300)
	
	plt.show()
	
def Generate_Discrete_HeatMap(NDomains, base_cmap='rainbow'):
	
	base = plt.cm.get_cmap(base_cmap)
	color_list = base(np.linspace(0,1,NDomains))
	cmap_name = base.name + str(NDomains)
	
	return base.from_list(cmap_name, color_list, NDomains)
	
def Export_Numerical_Data(Data1, XRange, XLabel, Potential, Study, YRange = '', YLabel = '', Extension='txt', Data2 = '', Data3 = '', Data4 = '', Data5 = '', Data6 = ''):
	
	file_name = Study + '_' + Potential + '.' + Extension
	file_handler = open(file_name,'w')
	file_handler.write('This files corresponds to a ' + Study + ' using the ' + Potential + ' Thermodynamic Potential. \n')
	
	#check how many data sources I am writing
	All_Data = Data1
	sources = 1
	if Data2 != '':
		sources = 2
		All_Data = zip(Data1,Data2)
		if Data3 != '':
			sources = 3
			All_Data = zip(Data1,Data2,Data3)
			if Data4!= '':
				sources = 4
				All_Data = zip(Data1,Data2,Data3,Data4)
				if Data5 != '':
					sources = 5
					All_Data = zip(Data1,Data2,Data3,Data4,Data5)
					if Data6 != '':
						sources = 6
						All_Data = zip(Data1,Data2,Data3,Data4,Data5,Data6)
	
	if YRange != '':
		file_handler.write(XLabel + '\t' + YLabel + '\n')
		if sources == 6:
			for x, d1, d2, d3, d4, d5, d6 in zip(XRange, Data1, Data2, Data3, Data4, Data5, Data6):
				for y, i, j, k, l, m, n in zip(YRange, d1, d2, d3, d4, d5, d6):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(i) + '\t' + str(j) + '\t' + str(k) + + '\t' + str(l) + '\t' + str(m) + '\t' + str(n) +'\n')
		if sources == 5:
			for x, d1, d2, d3, d4, d5 in zip(XRange, Data1, Data2, Data3, Data4, Data5):
				for y, i, j, k, l, m in zip(YRange, d1, d2, d3, d4, d5):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(i) + '\t' + str(j) + '\t' + str(k) + + '\t' + str(l) + '\t' + str(m) + '\n')
		if sources == 4:
			for x, d1, d2, d3, d4 in zip(XRange, Data1, Data2, Data3, Data4):
				for y, i, j, k, l in zip(YRange, d1, d2, d3,d4):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(i) + '\t' + str(j) + '\t' + str(k) + '\t' + str(l) +'\n')
		if sources == 3:
			for x, d1, d2, d3 in zip(XRange, Data1, Data2, Data3):
				for y, i, j, k in zip(YRange, d1, d2, d3):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(i) + '\t' + str(j) + '\t' + str(k) + '\n')
		if sources == 2:
			for x, d1, d2 in zip(XRange,Data1,Data2):
				for y, i, j in zip(YRange,d1,d2):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(i) + '\t' + str(j) + '\n')
		if sources == 1:
			for x, d1 in zip(XRange,Data1):
				for y, i in zip(YRange,d1):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(i) + '\n')
				
	else:
		file_handler.write(XLabel + '\n')
		if sources == 6:
			for x,i,j,k,l,m,n in zip(XRange, Data1, Data2, Data3, Data4, Data5, Data6):
				file_handler.write(str(x) + '\t' + '\t' + str(i) + '\t' + str(j) + '\t' + str(k) + '\t' + str(l)  + '\t' + str(m) + '\t' + str(n) + '\n')
		if sources == 5:
			for x,i,j,k,l,m in zip(XRange, Data1, Data2, Data3, Data4, Data5):
				file_handler.write(str(x) + '\t' + '\t' + str(i) + '\t' + str(j) + '\t' + str(k) + '\t' + str(l)  + '\t' + str(m) + '\n')
		if sources == 4:
			for x,i,j,k,l in zip(XRange, Data1, Data2, Data3, Data4):
				file_handler.write(str(x) + '\t' + '\t' + str(i) + '\t' + str(j) + '\t' + str(k) + '\t' + str(l)  + '\n')
		if sources == 3:
			for x,i,j,k in zip(XRange, Data1, Data2, Data3):
				file_handler.write(str(x) + '\t' + '\t' + str(i) + '\t' + str(j) + '\t' + str(k) + '\n')
		if sources == 2:
			for x,i,j in zip(XRange, Data1, Data2):
				file_handler.write(str(x) + '\t' + '\t' + str(i) + '\t' + str(j) + '\n')
		if sources == 1:
			for x,i in zip(XRange, Data1):
				file_handler.write(str(x) + '\t' + '\t' + str(i) + '\n')
				
	file_handler.close()

#Code-----------------------------------------------------------------
main()