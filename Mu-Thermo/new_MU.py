#New MU-Thermo

#Import Modules
import numpy as np
import time as time
import sympy as sp
import pandas as pd
import mpmath as mpm
import matplotlib.pyplot as plt
from matplotlib import colors
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from termcolor import colored 
import matplotlib, sys, os, textwrap, math

def Intro_Message():
	print('')
	print('Welcome to \u03BC-Thermo')
	print('A Program for Calculating Equilibrium Polarization States')
	print('and properties of Ferroelectric materials.')
	print('------------------------------------------------------------')
	print('')
	print('		\u03BC-Thermo')
	print('Copyright Notice (c) 2019 Jacob Zorn')
	print('Created at Pennsylvania State University')
	print('Version: 0.8.0')
	print('Last Updated: 09/23/2019')
	print('')
	print('------------------------------------------------------------')
	print('')
	print('Help and User Information: Can be found in the accompanying')
	print('README file and \u03BC-Thermo manual.')
	print('')
	print('------------------------------------------------------------')
	print('')
	print('This Software is Licensed via the XXXX License.')
	print('Information regarding this license can be found in ')
	print('the provided License.txt software provided with this')
	print('software or at XXX.Gitlab.XXX')
	print('')
	print('------------------------------------------------------------')
	print('')
	
def Outro_Message():
	print('')
	string_to_print = 'The Calculation has completed. Thank you for using mu-Thermo. The data and/or the image files have been properly exported.'
	for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
	print('------------------------------------------------------------')
	print('')
	print('		\u03BC-Thermo')
	print('Copyright Notice (c) 2019 Jacob Zorn')
	print('Created at Pennsylvania State University')
	print('Version: 0.8.0')
	print('Last Updated: 09/23/2019')
	print('')
	print('------------------------------------------------------------')
	
def Read_Input_File():

	file_handler = open('newMuInput.in')
	
	tag_list = ['System', 'Temp', 'Potential', 'Fraction', 'Misfit', 
				'Stress', 'Property', 'Study', 'SweepParameters', 
				'SweepLists', 'Output_Data', 'Output_Data_Name', 
				'Output_Img','Output_Img_Name', 'Output_Screen', 
				'Electric_Field', 'Rotation', 'RotAngles']
	
	default_values = ['Bulk', 298, 'BTO-10-Wang', 0.00, [0,0,0,0,0,0], 
					[0,0,0,0,0,0], 'Polarization', 0, ['',''], 
					[[100,500,100],[0,5e9,5e8]],1,'muThermo.out', 0, 
					'muThermo.png', 0, [0,0,0],False, [0,0,0]]
	
	tag_dict = dict(zip(tag_list,default_values))
	
	for line in file_handler:
		current_line = line.replace('\n','')
		try:
			if current_line[0] != '#':
				arg = current_line.split(' ')
				for key in tag_dict.keys():
					if key.lower() == arg[0].lower():
						try:
							tag_dict[key] = eval(arg[2])
						except:
							tag_dict[key] = arg[2]
						if key.lower() == 'misfit' or key.lower() == 'stress':
							tag_dict[key] = [eval(arg[2]),eval(arg[3]),eval(arg[4]),eval(arg[5]),eval(arg[6]),eval(arg[7])]
						if key.lower() == 'electric_field' or key.lower() == 'rotangles':
							tag_dict[key] = [eval(arg[2]),eval(arg[3]),eval(arg[4])]
						if key.lower() == 'rotation':
							if arg[2] == 1 or arg[2].lower() == 'true':
								tag_dict[key] = True
							else:
								tag_dict[key] = False
						if key.lower() == 'sweepparameters':
							try:
								tag_dict[key] = [arg[2],arg[3]]
							except:
								tag_dict[key] = [arg[2],'']
						if key.lower() == 'sweeplists':
							try:
								tag_dict[key] = [[eval(arg[2]),eval(arg[3]),eval(arg[4])],[eval(arg[5]),eval(arg[6]),eval(arg[7])]]
							except:
								tag_dict[key] = [[eval(arg[2]),eval(arg[3]),eval(arg[4])],['']]
		except:
			skip_flag = 0
	
	return tag_dict
	
def main():
	
	Intro_Message()
	
	tag_dict = Read_Input_File()
	
	if tag_dict['Study'] == 0:
		if tag_dict['System'] == 'Bulk':
			string_to_print = 'Conducting an Equilibrium Calculation for ' + str(tag_dict['Property']) + ' for a ' + str(tag_dict['System']) + ' using the ' + str(tag_dict['Potential']) + ' potential where the stress is equal to ' + str(tag_dict['Stress']) + ' at temperature of ' + str(tag_dict['Temp']) + ' K.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
				print('')
		elif tag_dict['System'] == 'Film':
			string_to_print = 'Conducting an Equilibrium Calculation for ' + str(tag_dict['Property']) + ' for a ' + str(tag_dict['System']) + ' using the ' + str(tag_dict['Potential']) + ' potential where the Misfit is equal to ' + str(tag_dict['Misfit']) + ' at temperature of ' + str(tag_dict['Temp']) + ' K.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
				print('')
		else:
			string_to_print = 'Inaccurate System tag was used. Please consult the Manual and the Input File. Shutting down Mu-Thermo.'
			string_to_print += ' Input was ' + str(tag_dict['System']) + ' the correct values are only: Bulk and Film.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
				print('')
			quit()
	if tag_dict['Study'] == 1:
		if tag_dict['System'] == 'Bulk':
			string_to_print = 'Conducting an One Parameter Sweep of ' + str(tag_dict['SweepParameters'][0] ) + ' from ' + str(tag_dict['SweepLists'][0]) + ' for ' + str(tag_dict['Property']) + ' for a ' + str(tag_dict['System']) + ' using the ' + str(tag_dict['Potential']) + ' potential where the stress is equal to ' + str(tag_dict['Stress']) + ' at temperature of ' + str(tag_dict['Temp']) + ' K.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
				print('')
		elif tag_dict['System'] == 'Film':
			string_to_print = 'Conducting an One Parameter Sweep of ' + str(tag_dict['SweepParameters'][0] ) + ' from ' + str(tag_dict['SweepLists'][0]) + ' for ' + str(tag_dict['Property']) + ' for a ' + str(tag_dict['System']) + ' using the ' + str(tag_dict['Potential']) + ' potential where the Misfit is equal to ' + str(tag_dict['Misfit']) + ' at temperature of ' + str(tag_dict['Temp']) + ' K.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
			print('')
		else:
			string_to_print = 'Inaccurate System tag was used. Please consult the Manual and the Input File. Shutting down Mu-Thermo.'
			string_to_print += ' Input was ' + str(tag_dict['System']) + ' the correct values are only: Bulk and Film.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
				print('')
			quit()	
	if tag_dict['Study'] == 2:
		if tag_dict['System'] == 'Bulk':
			string_to_print = 'Conducting an Two Parameter Sweep of ' + str(tag_dict['SweepParameters'][0] ) + ' and ' + str(tag_dict['SweepParameters'][1]) + ' from ' + str(tag_dict['SweepLists'][0]) + ' and ' + str(tag_dict['SweepLists'][1]) + ' for ' + str(tag_dict['Property']) + ' for a ' + str(tag_dict['System']) + ' using the ' + str(tag_dict['Potential']) + ' potential where the stress is equal to ' + str(tag_dict['Stress']) + ' at temperature of ' + str(tag_dict['Temp']) + ' K.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
				print('')
		elif tag_dict['System'] == 'Film':
			string_to_print = 'Conducting an Two Parameter Sweep of ' + str(tag_dict['SweepParameters'][0] ) + ' and ' + str(tag_dict['SweepParameters'][1]) + ' from ' + str(tag_dict['SweepLists'][0]) + ' and ' + str(tag_dict['SweepLists'][1]) + ' for ' + str(tag_dict['Property']) + ' for a ' + str(tag_dict['System']) + ' using the ' + str(tag_dict['Potential']) + ' potential where the Misfit is equal to ' + str(tag_dict['Misfit']) + ' at temperature of ' + str(tag_dict['Temp']) + ' K.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
				print('')
		else:
			string_to_print = 'Inaccurate System tag was used. Please consult the Manual and the Input File. Shutting down Mu-Thermo.'
			string_to_print += ' Input was ' + str(tag_dict['System']) + ' the correct values are only: Bulk and Film.'
			for text in (textwrap.wrap(string_to_print,width=60)):
				print(text)
				print('')
			quit()
	
	
	if tag_dict['SweepParameters'][1] != '':
		string_to_print = ('Two sweep parameters were given. Therefore running a two parameter sweep. Please wait and  the screen with update throughout the calculation process.')
		for text in (textwrap.wrap(string_to_print,width=60)):
			print(text)
		print('')
		
		if tag_dict['System'].lower() == 'film':
			Parameteric_Sweep_2D(tag_dict['System'], tag_dict['Potential'], tag_dict['Property'], SweepParameters=tag_dict['SweepParameters'], SweepLists=tag_dict['SweepLists'], Temp=tag_dict['Temp'], Misfit_App_Stress=tag_dict['Misfit'], Elec_Field=tag_dict['Electric_Field'], XF=tag_dict['Fraction'], Iterations=500, RotateFlag=tag_dict['Rotation'], Rotation=tag_dict['RotAngles'])
		if tag_dict['System'].lower() == 'bulk':
			Parameteric_Sweep_2D(tag_dict['System'], tag_dict['Potential'], tag_dict['Property'], SweepParameters=tag_dict['SweepParameters'], SweepLists=tag_dict['SweepLists'], Temp=tag_dict['Temp'], Misfit_App_Stress=tag_dict['Stress'], Elec_Field=tag_dict['Electric_Field'], XF=tag_dict['Fraction'], Iterations=500, RotateFlag=tag_dict['Rotation'], Rotation=tag_dict['RotAngles'])

	elif tag_dict['SweepParameters'][0] != '':
		string_to_print = ('One sweep parameter was given. Therefore running a one parameter sweep. Please wait and  the screen with update throughout the calculation process.')
		for text in (textwrap.wrap(string_to_print,width=60)):
			print(text)
		print('')

		if tag_dict['System'].lower() == 'film':
			Parametric_Sweep_1D(tag_dict['System'], tag_dict['Potential'], tag_dict['Property'], SweepParameter=tag_dict['SweepParameters'][0], SweepList=tag_dict['SweepLists'][0], Temp=tag_dict['Temp'], Misfit_App_Stress=tag_dict['Misfit'], Elec_Field=tag_dict['Electric_Field'], XF=tag_dict['Fraction'], Iterations=500, RotateFlag=tag_dict['Rotation'], Rotation=tag_dict['RotAngles'])
		elif tag_dict['System'].lower() == 'bulk':
			Parametric_Sweep_1D(tag_dict['System'], tag_dict['Potential'], tag_dict['Property'], SweepParameter=tag_dict['SweepParameters'][0], SweepList=tag_dict['SweepLists'][0], Temp=tag_dict['Temp'], Misfit_App_Stress=tag_dict['Stress'], Elec_Field=tag_dict['Electric_Field'], XF=tag_dict['Fraction'], Iterations=500, RotateFlag=tag_dict['Rotation'], Rotation=tag_dict['RotAngles'])
			
	else:
		string_to_print = 'No Sweep Parameters were given. Therefore only running an equilibrium calculation.'
		for text in (textwrap.wrap(string_to_print,width=60)):
			print(text)
	
	Outro_Message()

def Polarization_Solver(Prob, Temp=273, Misfit_App_Stress=[0,0,0,0,0,0], Elec_Field=[0,0,0], XF=0.0, Iterations=500):

	bounds 	= [(1e-10,1),(1e-10,1),(1e-10,1)]
	popsize = 40
	iters	= Iterations
	crossp	= 0.8
	mut		= 0.6
	
	pop = np.random.rand(popsize,len(bounds))
	min_b,max_b = np.asarray(bounds).T
	diff = np.fabs(min_b - max_b)
	pop_denorm = min_b + pop * diff
	fitness = np.asarray([Prob( ind[0], ind[1], ind[2], Temp, Misfit_App_Stress[0], Misfit_App_Stress[1], Misfit_App_Stress[2], Misfit_App_Stress[3], Misfit_App_Stress[4], Misfit_App_Stress[5], Elec_Field[0], Elec_Field[1], Elec_Field[2]) for ind in pop_denorm])
	best_idx = np.argmin(fitness)
	best = pop_denorm[best_idx]
	best_ener = 0
	
	for i in range(iters):
		for j in range(popsize):
			idxs = [idx for idx in range(popsize) if idx != j]
			a,b,c = pop[np.random.choice(idxs,3,replace=False)]
			mutant = np.clip(a + mut * (b-c), 0, 1)
			cross_points = np.random.rand(len(bounds)) < crossp
			if not np.any(cross_points):
				cross_points[np.random.randint(0,len(bounds))] = True
			trial = np.where(cross_points, mutant, pop[j])
			trial_denorm = min_b + trial * diff
			f = Prob( trial_denorm[0], trial_denorm[1], trial_denorm[2], Temp, Misfit_App_Stress[0], Misfit_App_Stress[1], Misfit_App_Stress[2], Misfit_App_Stress[3], Misfit_App_Stress[4], Misfit_App_Stress[5], Elec_Field[0], Elec_Field[1], Elec_Field[2])
			if f < fitness[j]:
				fitness[j] = f
				pop[j] = trial
				if f < fitness[best_idx]:
					best_idx = j
					best = trial_denorm
					best_ener = f	
		equil_polar = best
		
	return best_ener, equil_polar

def Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3):
	
	a1 		= a_values[0];	a11 	= a_values[1]
	a12 	= a_values[2];	a111	= a_values[3]
	a112	= a_values[4];	a123	= a_values[5]
	a1111	= a_values[6];	a1112	= a_values[7]
	a1122	= a_values[8];	a1123	= a_values[9]
	
	s11		= s_values[0];	s12		= s_values[1];	s44		= s_values[2]
	
	q11		= q_values[0];	q12		= q_values[1];	q44		= q_values[2]
	
	Land = a1 * (p1**2 + p2**2 + p3**2) + a12*(p1**2 * p2**2 + p1**2 * p3**2 + p2**2 * p3**2)
	Land += a11 * (p1**4 + p2**4 + p3**4) + a111 * (p1**6 + p2**6 + p3**6)
	Land += a112 * (p1**4 * (p2**2 + p3**2) + p2**4 * (p1**2 + p3**2) + p3**4 * (p2**2 + p1**2))
	Land += a123 * (p1**2 * p2**2 * p3**2) + a1111 * (p1**8 + p2**8 + p3**8)
	Land += a1112 * (p1**6 * (p2**2 + p3**2) + p2**6 * (p1**2 + p3**2) + p3**6 * (p2**2 + p1**2))
	Land += a1122 * (p1**4 * p2**4 + p1**4 * p3**4 + p2**4 * p3**4)
	Land += a1123 * (p1**4 * p2**2 * p3**2 + p1**2 * p2**4 * p3**2 + p1**2 * p2**2 * p3**4)

	Elas  = 0.5 * s11 * (s1**2 + s2**2 + s3**2) + s12 * (s1*s2 + s1*s3 + s2*s3)
	Elas += 0.5 * s44 * (s4**2 + s5**2 + s6**2)
	
	QEn  =  q11 * (s1*p1**2 + s2*p2**2 + s3*p3**2) + q12 * (s1*(p2**2 + p3**2) + s3*(p1**2 + p2**2) + s2*(p1**2 +p3**2))
	QEn += q44 * (p2*p3*s4 + p1*p3*s5 + p2*p1*s6)
	
	Elec = e1*p1 + e2*p2 + e3*p3
	
	GTot = Land - Elas - QEn - Elec
	
	return GTot

def Setup_Helmholtz(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3):

	a1 		= a_values[0];	a11 	= a_values[1]
	a12 	= a_values[2];	a111	= a_values[3]
	a112	= a_values[4];	a123	= a_values[5]
	a1111	= a_values[6];	a1112	= a_values[7]
	a1122	= a_values[8];	a1123	= a_values[9]
	
	s11		= s_values[0];	s12		= s_values[1];	s44		= s_values[2]
	
	q11		= q_values[0];	q12		= q_values[1];	q44		= q_values[2]
	
	Land = a1 * (p1**2 + p2**2 + p3**2) + a12*(p1**2 * p2**2 + p1**2 * p3**2 + p2**2 * p3**2)
	Land += a11 * (p1**4 + p2**4 + p3**4) + a111 * (p1**6 + p2**6 + p3**6)
	Land += a112 * (p1**4 * (p2**2 + p3**2) + p2**4 * (p1**2 + p3**2) + p3**4 * (p2**2 + p1**2))
	Land += a123 * (p1**2 * p2**2 * p3**2) + a1111 * (p1**8 + p2**8 + p3**8)
	Land += a1112 * (p1**6 * (p2**2 + p3**2) + p2**6 * (p1**2 + p3**2) + p3**6 * (p2**2 + p1**2))
	Land += a1122 * (p1**4 * p2**4 + p1**4 * p3**4 + p2**4 * p3**4)
	Land += a1123 * (p1**4 * p2**2 * p3**2 + p1**2 * p2**4 * p3**2 + p1**2 * p2**2 * p3**4)

	Elas  = 0.5 * s11 * (s1**2 + s2**2 + s3**2) + s12 * (s1*s2 + s1*s3 + s2*s3)
	Elas += 0.5 * s44 * (s4**2 + s5**2 + s6**2)
	
	QEn  =  q11 * (s1*p1**2 + s2*p2**2 + s3*p3**2) + q12 * (s1*(p2**2 + p3**2) + s3*(p1**2 + p2**2) + s2*(p1**2 +p3**2))
	QEn += q44 * (p2*p3*s4 + p1*p3*s5 + p2*p1*s6)
	
	Elec = e1*p1 + e2*p2 + e3*p3
	
	Land = Land
	
	GTot = Land + Elas + Elec
	
	return GTot

def Setup_Landau(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3):

	a1 		= a_values[0];	a11 	= a_values[1]
	a12 	= a_values[2];	a111	= a_values[3]
	a112	= a_values[4];	a123	= a_values[5]
	a1111	= a_values[6];	a1112	= a_values[7]
	a1122	= a_values[8];	a1123	= a_values[9]
	
	s11		= s_values[0];	s12		= s_values[1];	s44		= s_values[2]
	
	q11		= q_values[0];	q12		= q_values[1];	q44		= q_values[2]
	
	Land = a1 * (p1**2 + p2**2 + p3**2) + a12*(p1**2 * p2**2 + p1**2 * p3**2 + p2**2 * p3**2)
	Land += a11 * (p1**4 + p2**4 + p3**4) + a111 * (p1**6 + p2**6 + p3**6)
	Land += a112 * (p1**4 * (p2**2 + p3**2) + p2**4 * (p1**2 + p3**2) + p3**4 * (p2**2 + p1**2))
	Land += a123 * (p1**2 * p2**2 * p3**2) + a1111 * (p1**8 + p2**8 + p3**8)
	Land += a1112 * (p1**6 * (p2**2 + p3**2) + p2**6 * (p1**2 + p3**2) + p3**6 * (p2**2 + p1**2))
	Land += a1122 * (p1**4 * p2**4 + p1**4 * p3**4 + p2**4 * p3**4)
	Land += a1123 * (p1**4 * p2**2 * p3**2 + p1**2 * p2**4 * p3**2 + p1**2 * p2**2 * p3**4)

	Elas  = 0.5 * s11 * (s1**2 + s2**2 + s3**2) + s12 * (s1*s2 + s1*s3 + s2*s3)
	Elas += 0.5 * s44 * (s4**2 + s5**2 + s6**2)
	
	QEn  =  q11 * (s1*p1**2 + s2*p2**2 + s3*p3**2) + q12 * (s1*(p2**2 + p3**2) + s3*(p1**2 + p2**2) + s2*(p1**2 +p3**2))
	QEn += q44 * (p2*p3*s4 + p1*p3*s5 + p2*p1*s6)
	
	Elec = e1*p1 + e2*p2 + e3*p3
	
	Land = Land
	
	GTot = Land - Elas - QEn #+ Elec
	
	return Land, Elas, QEn, Elec

def Energy_Surface(Plane, System, Potential, Temp=298, Misfit_App_Stress=[0,0,0,0,0,0], Elec_Field=[0,0,0], XF=0, Iterations=500, RotateFlag=False, Rotation=[0,0,0]):

	print('Need to implement')

def Setup_Bulk_Symbols():
	
	T  = sp.Symbol('Temp')
	p1 = sp.Symbol('P1')
	p2 = sp.Symbol('P2')
	p3 = sp.Symbol('P3')
	s1 = sp.Symbol('Sig1')
	s2 = sp.Symbol('Sig2')
	s3 = sp.Symbol('Sig3')
	s4 = sp.Symbol('Sig4')
	s5 = sp.Symbol('Sig5')
	s6 = sp.Symbol('Sig6')
	e1 = sp.Symbol('E1')
	e2 = sp.Symbol('E2')
	e3 = sp.Symbol('E3')
	
	return T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3
	
def Setup_Film_Symbols():

	T  = sp.Symbol('Temp')
	p1 = sp.Symbol('P1')
	p2 = sp.Symbol('P2')
	p3 = sp.Symbol('P3')
	s1 = sp.Symbol('Sig1')
	s2 = sp.Symbol('Sig2')
	s3 = sp.Symbol('Sig3')
	s4 = sp.Symbol('Sig4')
	s5 = sp.Symbol('Sig5')
	s6 = sp.Symbol('Sig6')
	e1 = sp.Symbol('E1')
	e2 = sp.Symbol('E2')
	e3 = sp.Symbol('E3')
	u11, u22, u12 = sp.symbols('U11 U22 U12')
	
	return T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3, u11, u22, u12

def Potential_Choice(Potential, T, App_Stress, XF):
	
	if   Potential == 'BTO-10-Wang':
		Curie_Temp = 390
		a0 = abs(5*1e5*160*(sp.cosh(160/300)/sp.sinh(160/300) - np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)))
		a1 = 5*1e5*160*(sp.cosh(160/T)/sp.sinh(160/T) - np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp))
		a11 = -1.154 * 1e8 * (1 + 0.037*(App_Stress[0]*3/1e9))
		a12 = 6.530 * 1e8 * (1 + 0.037*(App_Stress[0]*3/1e9))
		a111 = -2.106 * 1e9 * (1 + 0.023*(App_Stress[0]*3/1e9))
		a112 = 4.091 * 1e9 * (1 + 0.023*(App_Stress[0]*3/1e9))
		a123 = -6.688 * 1e9 * (1 + 0.023*(App_Stress[0]*3/1e9))
		a1111 = 75900000000
		a1112 = -21930000000
		a1122 = -22210000000
		a1123 = 24160000000
		q11 = 0.11
		q12 = -0.045
		q44 = 0.029
		c11 = 177940000000
		c12 = 96350000000
		c44 = 121990000000
		p0 = 0.26
	elif Potential == 'BTO-Test':
		Curie_Temp = 110+273
		a0 = abs(3.3e5 * (T - Curie_Temp))
		a1 = 3.3e5 * (T - Curie_Temp)
		a11 = 3.6e6 * (T - 175+273)
		a12 = 4.9e8
		a111 = 6.6e9
		a112 = 2.9e9
		a123 = 7.6e7 * (T - 120+273) + 4.4e10
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.11
		q12 = -0.043
		q44 = 0.059
		s11 = 8.3e-12
		s12 = -2.7e-12
		s44 = 9.24e-12
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		p0 = 0.26
	elif Potential == 'BTO-84-Bell':
		Curie_Temp = 381
		a0 = (abs(3.34e5 * (T- Curie_Temp)))
		a1 = 3.34e5 * (T-Curie_Temp)
		a11 = 4.69e6 * (T-393) - 2.02e8
		a12 = 3.230e8
		a111 = -5.52e7 * (T-393) + 2.76e9
		a112 = 4.470e9
		a123 = 4.910e9
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.1
		q12 = -0.034
		q44 = 0.029
		c11 = 1.78e11
		c12 = 0.964e11
		c44 = 1.22e11
		p0 = 0.26
	elif Potential == 'BTO-01-Bell':
		Curie_Temp = 381
		a0 = (abs(3.34e5 * (T- Curie_Temp)))
		a1 = 3.34e5 * (T-Curie_Temp)
		a11 = 4.69e6 * (T-393) - 2.02e8
		a12 = 3.230e8
		a111 = -5.52e7 * (T-393) + 2.76e9
		a112 = 4.470e9
		a123 = 4.910e9
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.11
		q12 = -0.045
		q44 = 0.059
		c11 = 1.78e11
		c12 = 0.964e11
		c44 = 1.22e11
		p0 = 0.26
	elif Potential == 'BTO-05-Li':
		Curie_Temp = 388
		a0 = abs(4.124e5 * (300 - Curie_Temp))
		a1 = 4.124e5 * (T - Curie_Temp)
		a11 = -2.097e8
		a12 = 7.974e8
		a111 = 1.294e9
		a112 = -1.950e9
		a123 = -2.500e9
		a1111 = 3.863e10
		a1112 = 2.529e10
		a1122 = 1.637e10
		a1123 = 1.367e10
		q11 = 0.11
		q12 = -0.045
		q44 = 0.059
		c11 = 1.78e11
		c12 = 0.964e11
		c44 = 1.22e11
		p0 = 0.26
	elif Potential == 'BTO-07-Wang':
		Curie_Temp = 391
		a0 = abs(3.61e5 * (300 - Curie_Temp))
		a1 = 3.61e5 * (T - Curie_Temp)
		a11 = -1.83e9 + 4e6 * T
		a12 = -2.24e9 + 6.7e6 * T
		a111 = 1.39e10 - 3.2e7 * T
		a112 = -2.2e9
		a123 = 5.51e10
		a1111 = 4.84e10
		a1112 = 2.53e11
		a1122 = 2.8e11
		a1123 = 9.35e10
		q11 = (0.11 + 0.10) / 2
		q12 = -(0.045 + 0.034)/2
		q44 = 0.29
		c11 = 1.78e11
		c12 = 0.964e11
		c44 = 1.22e11
		p0 = 0.26
	elif Potential == 'BFO-16-Xue':
		Curie_Temp = 1120
		a0 = abs(4 * 1.093e5 * (300 - Curie_Temp))
		#a1 = 4.64385e5 * (T - Curie_Temp)
		a1 = 4 * 1.093e5 * (T - Curie_Temp)
		a11 = 4*5.318e8
		a12 = -4 * 5.123e8
		a111 = -4 * 4.4e8
		a112 = 4 * 2.074e8
		a123 = 4 * 4.198e8
		a1111 = 4 * 0.98e8
		a1112 = 4 * 1.1e7
		a1122 = 4 * 9.5e7
		a1123 = 4 * 2.0e8
		q11 = 0.032
		q12 = -0.016
		q44 = 0.02015
		c11 = 2.28e11
		c12 = 1.28e11
		c44 = 6.5e10
		p0 = 1.0
	elif Potential == 'BFO-12-Vasudevam':
		Curie_Temp = 1103
		a0 = abs(4.64385e5 * (300 - Curie_Temp))
		a1 = 4.64385e5 * (T - Curie_Temp)
		a11 = 2.29047e8
		a12 = 3.06361e8
		a111 = 5.99186e7
		a112 = -3.33980e5
		a123 = -1.77754e8
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.032
		q12 = -0.016
		q44 = 0.04
		c11 = 3.02e11
		c12 = 1.62e11
		c44 = 0.68e11
		p0 = 0.52
	elif Potential == 'BFO-14-Xue':
		Curie_Temp = 1193
		a0 = abs(4.0e5 * (300 - Curie_Temp))
		a1 = 4.0e5 * (T - Curie_Temp)
		a11 = 3.0e8
		a12 = 1.188e8
		a111 = 0
		a112 = 0
		a123 = 0
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.057
		q12 = -0.02
		q44 = 0.000733
		c11 = 2.280e11
		c12 = 1.28e11
		c44 = 0.65e11
		p0 = 0.52
	elif Potential == 'PTO-02-Li':
		Curie_Temp = 752.15
		a0 = abs(3.8e5 * (300 - Curie_Temp))
		a1 = 3.8e5 * (T - Curie_Temp)
		a11 = -73000000
		a12 = 7.5e8
		a111 = 2.6e8
		a112 = 6.1e8
		a123 = -3700000000
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.089
		q12 = -0.026
		q44 = 0.03375
		c11 = 174600000000
		c12 = 79370000000
		c44 = 111100000000
		p0 = 0.757
	elif Potential == 'STO-07-Li':
		a0 = abs(2.6353e7 * (1/(np.tanh(42/T)) - 0.90476))
		a1 = 2.6353e7 * (1/(np.tanh(42/T)) - 0.90476)
		a11 = 1696000000
		a12 = 1373000000
		a111 = 0
		a112 = 0
		a123 = 0
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.066
		q12 = -0.0135
		q44 = 0.0096
		c11 = 336000000000
		c12 = 107000000000
		c44 = 127000000000
		p0 = 0.10
	elif Potential == 'STO-10-Sheng':
		Curie_Temp = 30
		a0 = abs(4.06e7 * (1/(np.tanh(42/T)) - 1/(np.tanh(42/Curie_Temp))))
		a1 = 4.06e7 * (1/(np.tanh(42/T)) - 1/(np.tanh(42/Curie_Temp)))
		a11 = 1.701e9
		a12 = 3.645e9
		a111 = 0
		a112 = 0
		a123 = 0
		a1111 = 0
		a1112 = 0
		a1122 = 0
		a1123 = 0
		q11 = 0.04581
		q12 = -0.0135
		q44 = 0.0096
		c11 = 3.36e11
		c12 = 1.07e11
		c44 = 1.27e11
		p0 = 0.10
	elif Potential == 'KNN-17-Pohlmann':
		Curie_Temp = 650
		a0 = abs(2 * XF * 4.29e7 * (1/np.tanh(140/300) - 1/np.tanh(140/Curie_Temp)) + (1-2*XF)*5.98e7*(1/np.tanh(140/300) - 1/np.tanh(140/Curie_Temp)))
		a1 = 2 * XF * 4.29e7 * (1/np.tanh(140/T) - 1/np.tanh(140/Curie_Temp)) + (1-2*XF)*5.98e7*(1/np.tanh(140/T) - 1/np.tanh(140/Curie_Temp))
		a11 = 2*XF*(-2.7302e8) + (1-2*XF)*(-6.36e8)
		a12 = 2*XF*(1.0861e9) + (1-2*XF)*(9.66e8)
		a111 = 2*XF*(3.0448e9) + (1-2*XF)*(2.81e9)
		a112 = 2*XF*(-2.727e9)+(1-2*XF)*(-1.99e9)
		a123 = 2*XF*(1.5513e10)+(1-2*XF)*(4.5e9)
		a1111 = 2*XF*(2.4044e10) + (1-2*XF)*(1.74e10)
		a1112 = 2*XF*(3.7328e9) + (1-2*XF)*(5.99e9)
		a1122 = 2*XF*(3.3485e8) + (1-2*XF)*(2.5e10)
		a1123 = 2*XF*(-6.2017e10) + (1-2*XF)*(-1.17e10)
		q11 = 0.12
		q12 = -0.053
		q44 = 0.052
		s11 = 4.6e-12
		s12 = -1.1e-12
		s44 = 1.11e-11
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		p0 = 0.54
	elif Potential == 'KNO-09-Liang':
		Curie_Temp = 650
		a0 = abs(4.273e5 * (300 - Curie_Temp))
		a1 = 4.273e5 * (T - Curie_Temp)
		a11 = -636000000
		a12 = 966000000
		a111 = 2810000000
		a112 = -1990000000
		a123 = 6030000000
		a1111 = 17400000000
		a1112 = 5990000000
		a1122 = 25000000000
		a1123 = -11700000000
		q11 = 0.12
		q12 = -0.053
		q44 = 0.052
		s11 = 4.6e-12
		s12 = -1.1e-12
		s44 = 1.11e-11
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		p0 = 0.45
	elif Potential == 'BZTO-18-Peng':
		Curie_Temp = 115.038 - 80.884*XF - 1421.437*XF**2 - 3088.612*XF**3 + 8443.89*XF**4 + 273
		
		p0 = 0.26
		
		if XF >= 0 and XF <= 0.1:
			a0 = 4.124 + 4.16*XF + 176.6*XF**2 - 27761*XF**4
		else:
			a0 = 2.84 - 1.396*XF + 0.83 * (np.sinh(7.65-34.17*XF)/np.cosh(7.65-34.17*XF))
		
		a11 = -8.2571 - 174.043 * XF**3 + 6.2368 * (np.sinh(7.3152 - 43.3423*XF)/np.cosh(7.65 - 43.3423*XF))
		
		if XF >= 0 and XF <= 0.08:
			a12 = 7.974 + 101.897*XF - 84612.4*XF**4
		else:
			a12 = 11.2932 + 11.2489 * (np.sinh(2.1278 - 25.0715*XF)/np.cosh(2.1278 - 25.0715*XF))
			
		if XF >= 0 and XF <= 0.1:
			a111 = -102270.39 + 102271.684*np.exp(XF) - 102142*XF - 52790*XF**2
		else:
			a111 = 505.76 - ((501.54)/(1 + np.exp(50.193*(XF - 0.202)))) + 80359*XF**4
			
		if XF >=0 and XF <= 0.08:
			a112 = -1.95 - 64.042*XF - 1879.2*XF**2
		else:
			a112 = -10.996 - 8.601* (np.sinh(3.475 - 21.3579*XF)/np.cosh((3.475 - 21.3579*XF)))
			
		if XF >= 0 and XF <= 0.08:
			a123 = -2.5 - 179.4*XF + 2300*XF**2
		else:
			a123 = -3.513 + 19.363*XF - 27.1237*XF**2
			
		if XF >= 0 and XF <= 0.08:
			a1111 = 3.863 + 82.4165*XF + 666.47*XF**2
		else:
			a1111 = 7.3375 + 7.3934 * (np.sinh(7.6408 - 49.3288*XF)/np.cosh(7.6408 - 49.3288*XF))
			
		if XF >= 0 and XF <= 0.15:
			a1112 = 7.8471- 5.3814 * (np.sinh(1.9787 - 22.1963*XF)/np.cosh(1.9787 - 22.1963*XF))
		else:
			a1112 = 6.5749 + 6.6076 * (np.sinh(13.0115 - 77.0462*XF)/np.cosh(13.0115 - 77.0462*XF))
			
		if XF >= 0 and XF <= 0.15:
			a1122 = 7.05 - 6.066 * (np.sinh(1.41 - 20.091 * XF)/np.cosh(1.41 - 20.091 * XF))
		else:
			a1122 = 6.5378 + 6.6708 * (np.sinh(12.9508 - 76.7401*XF)/np.cosh(12.9508 - 76.7401*XF))
			
		if XF >= 0 and XF <= 0.1:
			a1123 = 8.845 - 7.7478 * (np.sinh(8 - 104*XF)/np.cosh(8 - 104*XF))
		else:
			a1123 = 8.1048 + 8.2268 * (np.sinh(6.5178 - 41.525*XF)/np.cosh(6.5178 - 41.525*XF))
			
		s11 = 8.3 - 4.87*XF;		s11 = s11*1e-12
		s12 = -2.7 + 2.025 * XF;	s12 = s12*1e-12
		s44 = 9.24 + 5.02 * XF;		s44 = s44*1e-12
		
		q11 = 0.11 - 0.004*XF
		q12 = -0.034 - 0.005*XF
		q44 = 0.029 + 0.002*XF
		
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		
		a1 = a0 * (T - Curie_Temp)	
		
		a1 = a1*1e5;		a11 = a11*1e8;		a12 = a12*1e8
		a111 = a111*1e9;	a112 = a112*1e9;	a123 = a123*1e9
		a1111 = a1111*1e10;	a1112 = a1112*1e10;	a1122 = a1122*1e10
		a1123 = a1123*1e10;
	elif Potential == 'BSTO-17-Cao':
		Curie_Temp = 390
		a0 = abs(5.0e5 * 160 * ((np.cosh(160/300)/np.sinh(160/300)) - (np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)))*(1-x) + 2.525e7*((np.cosh(54/300)/np.sinh(54/300)) - (np.cosh(54/30)/np.sinh(54/30)))*x)
		a1 = 5.0e5 * 160 * ((np.cosh(160/T)/np.sinh(160/T)) - (np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)))*(1-x) + 2.525e7*((np.cosh(54/T)/np.sinh(54/T)) - (np.cosh(54/30)/np.sinh(54/30)))*x
		a11 = -1.154e8
		a12 = 6.530e8 * (1 - 1.4196*XF)
		a111 = -2.106e9
		a112 = 4.091e9
		a123 = -6.688e9 * (1 - 1.3*XF)
		a1111 = 7.590e10
		a1112 = -2.193e10
		a1122 = -2.221e10
		a1123 = 2.416e10
		q11 = (1-XF)*0.11 + XF*0.066
		q12 = (1-XF)*(-0.045) + XF*(-0.0135)
		q44 = (1-XF)*(0.029) + XF*(0.0096)
		c11 = (1-XF)*177940000000 + XF*(3.36e11)
		c12 = (1-XF)*96350000000 + XF*(1.07e11)
		c44 = (1-XF)*121990000000 + XF*(1.27e11)
		p0 = 0.26
	elif Potential == 'BCTO-17-Cao':
		Curie_Temp = 390
		a0 = abs(5.0e5 * 160 * ((np.cosh(160/300)/np.sinh(160/300)) - (np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp))))
		a1 = 5.0e5 * 160 * ((np.cosh(160/T)/np.sinh(160/T)) - (np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)))
		a11 = -1.154e8
		a12 = 6.530e8 * (1 + 2.8775*XF)
		a111 = -2.106e9
		a112 = 4.091e9 * (1 + 3.5865*XF)
		a123 = -6.688e9 * (1 + 4.0015*XF)
		a1111 = 7.590e10
		a1112 = -2.193e10
		a1122 = -2.221e10
		a1123 = 2.416e10
		q11 = (1-XF)*0.11 + XF*0.066
		q12 = (1-XF)*(-0.045) + XF*(-0.0135)
		q44 = (1-XF)*(0.029) + XF*(0.0096)
		c11 = (1-XF)*177940000000 + XF*(4.03e11)
		c12 = (1-XF)*96350000000 + XF*(1.07e11)
		c44 = (1-XF)*121990000000 + XF*(9.99e10)
		p0 = 0.26
	elif Potential == 'BSTO-18-Huang':
		Curie_Temp = 390
		a0 = abs(8.0e7 * ((np.cosh(160/300)/np.sinh(160/300))-(np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)) - (-1.75)*XF))
		a1 = 8.0e7 * ((np.cosh(160/T)/np.sinh(160/T))-(np.cosh(160/Curie_Temp)/np.sinh(160/Curie_Temp)) - (-1.75)*XF)
		a11 = -1.154e8 * (1 - 1.05*XF)
		a12 = 6.530e8 * (1 - 1.05*XF)
		a111 = -2.106e9 * (1 - 0.483*XF)
		a112 = 4.091e9 * (1 - 0.483*XF)
		a123 = -6.688e9 * (1 - 0.483*XF)
		a1111 = 7.590e10
		a1112 = -2.193e10
		a1122 = -2.221e10
		a1123 = 2.416e10
		q11 = (1-XF)*0.11 + XF*0.066
		q12 = (1-XF)*(-0.045) + XF*(-0.0135)
		q44 = (1-XF)*(0.029) + XF*(0.0096)
		c11 = (1-XF)*177940000000 + XF*(3.36e11)
		c12 = (1-XF)*96350000000 + XF*(1.07e11)
		c44 = (1-XF)*121990000000 + XF*(1.27e11)
		p0 = 0.26
	elif Potential == 'PZT-88-Haun':
		CurieT = 462.23 + 843.4*XF - 2105.5*XF**2 + 4041.8*XF**3 - 3828.3*XF**4 + 1337.8*XF**5
		
		if XF >= 0.0 and XF < 0.5:
			Curie = (2.1716 / (1.0 + 500.05*(XF-0.5)**2) + 0.131*XF+2.01)*10**5
		else:
			Curie = (2.8339 / (1.0+126.56*(XF-0.5)**2) + 1.4132)*10**5
		
		Z1 = ((-9.6 - 0.012501*XF)*np.exp(-12.6*XF) +0.42743*XF+2.6213)*10**14 / Curie
		Z2 = ((16.225-0.088651*XF)*np.exp(-21.255*XF)-0.76973*XF+0.887)*10**14 / Curie
		a0 = 1/(2*8.85E-012*Curie)
		a11 = (10.612 - 22.655*XF + 10.955*XF**2)*10**13/Curie
		a111 = (12.026-17.296*XF+ 9.179*XF**2)*10**13/Curie
		a112 = (58.804*np.exp(-29.397*XF)-3.3754*XF+4.2904)*10**14 / Curie
		p0 = 0.757
		
		a1 = a0 * (T - CurieT)
		a11 = (10.612 - 22.655*XF + 10.955*XF**2)*10**13 / Curie
		a12 = Z1/3-a11
		a111 = (12.026 - 17.296*XF + 9.179*XF**2)*10**13 / Curie
		a112 = (58.804 * np.exp(-29.397*XF) - 3.3754*XF + 4.2904)*10**14 / Curie
		a123 = (Z2 - 3*a111 - 6*a112)
		a1111 = 0;	a1112 = 0;	a1122 = 0;	a1123 = 0
		
		q11 = 0.045624+0.042796*XF+0.029578/(1+200*(XF-0.5)**2)
		q12 = -0.026568/( 1+ 200*(XF-0.5)**2) - 0.012093*XF - 0.013386
		q44 = 0.5*(0.046147+0.020857*XF+0.025325/(1+200.*(XF-0.5)**2))
		
		if XF >= 0 and XF < 0.45:
			s11 = 8.8e-12;	s12 = -2.9e-12;	s44 = 2.46e-11
		elif XF >= 0.45 and XF < 0.55:
			s11 = 1.05e-11;	s12 = -3.7e-12;	s44 = 2.87e-11
		elif XF >= 0.55 and XF < 0.65:
			s11 = 8.6e-12;	s12 = -2.8e-12;	s44 = 2.12e-11
		elif XF >= 0.65 and XF < 0.75:
			s11 = 8.4e-12;	s12 = -2.7e-12;	s44 = 1.75e-11
		elif XF >= 0.75 and XF < 0.85:
			s11 = 8.2e-12;	s12 = -2.6e-12;	s44 = 1.44e-11
		elif XF >= 0.85 and XF <= 1.0:
			s11 = 8.1e-12;	s12 = -2.5e-12;	s44 = 1.2e-11
			
		c11 = (s11 + s12) / ((s11 - s12)*(s11 + 2*s12))
		c12 = (-s12) / ((s11 - s12)*(s11 + 2*s12))
		c44 = 1 / s44
		
	else:
		print('The Potential: ', Potential, ' is not yet implemented in this code.')
		quit()

	s11 = (c11 + c12) / ((c11 + 2*c12) * (c11 - c12))
	s12 = (-c12) / ((c11 + 2*c12) * (c11 - c12))
	s44 = 1/c44

	a_values = [a1, a11, a12, a111, a112, a123, a1111, a1112, a1122, a1123]
	c_values = [c11, c12, c44]
	s_values = [s11, s12, s44]
	q_values = [q11, q12, q44]
	
	return a0, a_values, c_values, s_values, q_values, p0

def Equilibrium(System, Potential, Temp=298, Misfit_App_Stress=[0,0,0,0,0,0], Elec_Field=[0,0,0], XF=0.0, Iterations=500, RotateFlag=False, Rotation=[0,0,0]):
	
	if System.lower() == 'bulk':
		T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Bulk_Symbols()
		
		a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
		
		Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
		
		Prob = Prob.subs({s4:Misfit_App_Stress[3],s5:Misfit_App_Stress[4],s6:Misfit_App_Stress[5]})
		
		newProb = sp.lambdify([p1, p2, p3, T, s1, s2, s3, e1, e2, e3], Prob, 'numpy')
		
		f, equil_polar = Polarization_Solver(newProb, Temp=Temp, Misfit_App_Stress=Misfit_App_Stress, Elec_Field=Elec_Field, XF=XF, Iterations=Iterations)
	
	if System.lower() == 'film':
		T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3, u11, u22, u12 = Setup_Film_Symbols()
		
		a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
		
		Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
		
		Prob = Prob.subs({s3:Misfit_App_Stress[2],s4:Misfit_App_Stress[3],s5:Misfit_App_Stress[4]})
		
		solutions = (sp.solve([Prob.diff(s1)+u11,Prob.diff(s2)+u22,Prob.diff(s6)+u12],[s1,s2,s6]))
		
		Prob += Misfit_App_Stress[0] * s1 + Misfit_App_Stress[1] * s2 + Misfit_App_Stress[2] * s6
		
		Prob = Prob.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6]})
		
		newProb = sp.lambdify([p1, p2, p3, T, u11, u22, u12, e1, e2, e3], Prob, 'numpy')
		
		f, equil_polar = Polarization_Solver(newProb, Temp=Temp, Misfit_App_Stress=Misfit_App_Stress, Elec_Field=Elec_Field, XF=XF, Iterations=Iterations)
		
	return f, equil_polar

def Property_Calculation(System, Potential, Property, Temp=298, Misfit_App_Stress=[0,0,0,0,0,0], Elec_Field=[0,0,0], XF=0, Iterations=500, RotateFlag=False, Rotation=[0,0,0]):


	if Property.lower() == 'dielectric':
		if System.lower() == 'bulk':
			T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Bulk_Symbols()
			
			a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
			
			Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
			
			Prob = Prob.subs({s4:Misfit_App_Stress[3],s5:Misfit_App_Stress[4],s6:Misfit_App_Stress[5]})
			
			PolarProb = sp.lambdify([p1, p2, p3, T, s1, s2, s3, e1, e2, e3], Prob, 'numpy')
			
			f, equil_polar = Polarization_Solver(PolarProb, Temp=Temp, Misfit_App_Stress=Misfit_App_Stress, Elec_Field=Elec_Field, XF=XF, Iterations=Iterations)
			
			#domain, equil_polar = Domain_Qualifier(equil_polar,'Bulk')
			
			n1 = Prob.diff(p1,p1);		n2 = Prob.diff(p2,p2);		n3 = Prob.diff(p3,p3);
			n4 = Prob.diff(p2,p3);		n5 = Prob.diff(p1,p3);		n6 = Prob.diff(p1,p2);
			
			suscep_derives = [n1, n2, n3, n4, n5, n6]
			
			suscep_lambda_eqs = []
			for eq in suscep_derives:
				suscep_lambda_eqs.append(sp.lambdify([p1, p2, p3, T, s1, s2, s3, e1, e2, e3], eq, 'numpy'))
			
			suscep_list = []
			for eq in suscep_lambda_eqs:
				suscep_list.append(eq(equil_polar[0], equil_polar[1], equil_polar[2], Temp, Misfit_App_Stress[0], Misfit_App_Stress[1], Misfit_App_Stress[2],Elec_Field[0],Elec_Field[1],Elec_Field[2]))
			
			x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
			x_values = np.matrix(x_values,dtype='float')

			x_values = (np.linalg.inv(x_values))
			
			k_values = (x_values)/8.85e-12
			
			k11 = k_values[0,0];	k22 = k_values[1,1];	k33 = k_values[2,2]
			k23 = k_values[1,2];	k13 = k_values[0,2];	k12 = k_values[0,1]
			
			Print_Dielectric_Tensor(k_values,Potential)
		if System.lower() == 'film':
			
			if RotateFlag == True:
				print('Needs to be Implemented')
			else:
				T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3, u11, u22, u12 = Setup_Film_Symbols()
				a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
				Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
				Prob = Prob.subs({s3:Misfit_App_Stress[2],s4:Misfit_App_Stress[3],s5:Misfit_App_Stress[4]})
				Prob += Misfit_App_Stress[0] * s1 + Misfit_App_Stress[1] * s2 + Misfit_App_Stress[2] * s6
				solutions = (sp.solve([Prob.diff(s1)+u11,Prob.diff(s2)+u22,Prob.diff(s6)+u12],[s1,s2,s6]))
				Prob = Prob.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6]})
				PolarProb = sp.lambdify([p1, p2, p3, T, u11, u22, u12, e1, e2, e3], Prob, 'numpy')
				f, equil_polar = Polarization_Solver(PolarProb, Temp=Temp, Misfit_App_Stress=Misfit_App_Stress, Elec_Field=Elec_Field, XF=XF, Iterations=Iterations)
				
				n1 = Prob.diff(p1,p1);		n2 = Prob.diff(p2,p2);		n3 = Prob.diff(p3,p3);
				n4 = Prob.diff(p2,p3);		n5 = Prob.diff(p1,p3);		n6 = Prob.diff(p1,p2);
				
				suscep_derives = [n1, n2, n3, n4, n5, n6]
				
				suscep_lambda_eqs = []
				for eq in suscep_derives:
					suscep_lambda_eqs.append(sp.lambdify([p1, p2, p3, T, u11, u22, u12, e1, e2, e3], eq, 'numpy'))
				
				suscep_list = []
				for eq in suscep_lambda_eqs:
					suscep_list.append(eq(equil_polar[0], equil_polar[1], equil_polar[2], Temp, Misfit_App_Stress[0], Misfit_App_Stress[1], Misfit_App_Stress[2],Elec_Field[0],Elec_Field[1],Elec_Field[2]))
				
				x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
				
				x_values = np.matrix(x_values,dtype='float')

				x_values = (np.linalg.inv(x_values))
				
				k_values = (x_values)/8.85e-12
				
				k11 = k_values[0,0];	k22 = k_values[1,1];	k33 = k_values[2,2]
				k23 = k_values[1,2];	k13 = k_values[0,2];	k12 = k_values[0,1]
				
				Print_Dielectric_Tensor(k_values,Potential)
	if Property.lower() == 'piezoelectric':
		if System.lower() == 'bulk':
			print('Need to Implement')
		if System.lower() == 'film':
			print('Need to Implement')
	if Property.lower() == 'electrocaloric':
		if System.lower() == 'bulk':
			print('Need to Implement')
		if System.lower() == 'film':
			print('Need to Implement')

def Parametric_Sweep_1D(System, Potential, Property, SweepParameter='Temp', SweepList=[100,300,10], Temp=298, Misfit_App_Stress=[0,0,0,0,0,0], Elec_Field=[0,0,0], XF=0, Iterations=500, RotateFlag=False, Rotation=[0,0,0]):

	sweeper = np.arange(SweepList[0],SweepList[1]+SweepList[2], SweepList[2])
	print('Sweep List is: ',sweeper)
	
	#Generate the Equilibrium Polarization Functions here
	
	bulk 	= 'bulk' in System.lower()
	film 	= 'film' in System.lower()
	rotated	= RotateFlag	
	
	if bulk:
		T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Bulk_Symbols()
		a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
		Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
		PolarProb = sp.lambdify([p1, p2, p3, T, s1, s2, s3, s4, s5, s6, e1, e2, e3], Prob, 'numpy')
		
	if film:
		if rotated:

			Rotation = [mpm.radians(Rotation[0]),mpm.radians(Rotation[1]),mpm.radians(Rotation[2])]
		
			T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3, u11, u22, u12 = Setup_Film_Symbols()
			
			"""
			p1 p2 p3 are the local components of polarization
			s1 s2 s3 s4 s5 s6 are the local components of stress
			u1 u2 u3 u4 u5 u6 are the local components of strain
			"""
			
			a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
			
			Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
			
			angle = sp.Symbol('Ang')
			
			A_z = sp.Matrix([	[sp.cos(angle),sp.sin(angle),0],
								[-sp.sin(angle),sp.cos(angle),0],
								[0,0,1]])
			A_x = sp.Matrix([	[1,0,0],
								[0,sp.cos(angle),sp.sin(angle)],
								[0,-sp.sin(angle),sp.cos(angle)]])
								
			A_1ij = A_z.subs({angle:Rotation[0]})
			A_2ij = A_x.subs({angle:Rotation[1]})
			A_3ij = A_z.subs({angle:Rotation[2]})
			
			m = A_3ij * A_2ij * A_1ij
			
			m = m.transpose()
			
			e1app = Misfit_App_Stress[0];	e2app = Misfit_App_Stress[1]
			e6app = Misfit_App_Stress[2];	s3app = Misfit_App_Stress[3]
			s4app = Misfit_App_Stress[4];	s5app = Misfit_App_Stress[5]
			
			s1g, s2g, s6g, e3g, e4g, e5g = sp.symbols('sg1 sg2 sg6 eg3 eg4 eg5')
			
			sig_app = sp.Matrix([	[s1g,s6g,s5app],
									[s6g,s2g,s4app],
									[s5app,s4app,s3app]])
			
			eta_app = sp.Matrix([	[e1app,0.5*e6app,0.5*e5g],
									[0.5*e6app,e2app,0.5*e4g],
									[0.5*e5g,0.5*e4g,e3g]])
									
			pglobal = (m * sp.Matrix([p1,p2,p3]))
			sig_local = m.transpose() * sig_app * m
			eta_local = m.transpose() * eta_app * m
			
			print(sig_local)
			
			quit()
			
		else:
			T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3, u11, u22, u12 = Setup_Film_Symbols()
			a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
			Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
			Prob = Prob.subs({s3:Misfit_App_Stress[2],s4:Misfit_App_Stress[3],s5:Misfit_App_Stress[4]})
			solutions = (sp.solve([Prob.diff(s1)+u11,Prob.diff(s2)+u22,Prob.diff(s6)+u12],[s1,s2,s6]))
			
			Prob += u11 * s1 + u22 * s2 + u12 * s6
			
			Prob = Prob.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6]})
			PolarProb = sp.lambdify([p1, p2, p3, T, u11, u22, u12, s3, s4, s5, e1, e2, e3], Prob, 'numpy')
	
	#Generate the lambda functions and save them. Then we can apply them in the second half of this function
	
	pyro_bool = 'pyro' 	in Property.lower()
	diel_bool = 'diel' 	in Property.lower()
	piez_bool = 'piez' 	in Property.lower()
	polr_bool = 'pol'	in Property.lower()
	phas_bool = 'phas'	in Property.lower()
		
	if diel_bool:
		k11_list = [];		k22_list = [];		k33_list = []
		k23_list = [];		k13_list = [];		k12_list = []
		n1 = Prob.diff(p1,p1);	n2 = Prob.diff(p2,p2)
		n3 = Prob.diff(p3,p3);	n4 = Prob.diff(p2,p3)
		n5 = Prob.diff(p1,p3);	n6 = Prob.diff(p1,p2)
		suscep_derives = [n1, n2, n3, n4, n5, n6]
		if bulk:
			suscep_lambda_eqs = []
			for eq in suscep_derives:
				suscep_lambda_eqs.append(sp.lambdify([p1, p2, p3, T, s1, s2, s3, s4, s5, s6, e1, e2, e3], eq, 'numpy'))
		if film:
			suscep_lambda_eqs = []
			for eq in suscep_derives:
				suscep_lambda_eqs.append(sp.lambdify([p1, p2, p3, T, u11, u22, u12, s3, s4, s5, e1, e2, e3], eq, 'numpy'))	
	if piez_bool:
		d11_list = [];		d22_list = [];		d33_list = []
		d15_list = [];		d24_list = [];		d31_list = []
		#Calculate the Strains from the Gibbs Energy
		eta1 = -Prob.diff(s1);	eta2 = -Prob.diff(s2)
		eta3 = -Prob.diff(s3);	eta4 = -Prob.diff(s4)
		eta5 = -Prob.diff(s5);	eta6 = -Prob.diff(s6)
		strains = [eta1, eta2, eta3, eta4, eta5, eta6]
		
		#Calculate the Susceptabilities
		n1 = Prob.diff(p1,p1);	n2 = Prob.diff(p2,p2)
		n3 = Prob.diff(p3,p3);	n4 = Prob.diff(p2,p3)
		n5 = Prob.diff(p1,p3);	n6 = Prob.diff(p1,p2)
		suscep_derives = [n1, n2, n3, n4, n5, n6]
		
		#Calculate the Derivative of Strains with Respect to Polarization
		polars = [p1, p2, p3];	eta_derives = []
		for eta in strains:
			temp_derive = []
			for polar in polars:
				temp_derive.append(eta.diff(polar))
			eta_derives.append(temp_derive)
	if polr_bool:
		p1_list = [];	p2_list = [];	p3_list = []; pmag_list = []
	if phas_bool:
		domain_list = []
	
	#This allows for all parameters to be specified and a vareity of different work to be accomplished. For example. It now possible to vary the in-plane misfit strain at a constant (non-zero) shear strain or a constant (non-zero) applied stress. But we do have to change the param just slightly
	
	temp_bool 	= 'temp' in SweepParameter.lower()
	stress_bool = 'stress' in SweepParameter.lower()
	hydro_bool	= 'hydrostatic' in SweepParameter.lower()
	misfit_bool = 'misfit' in SweepParameter.lower()
	elec_bool	= 'electric' in SweepParameter.lower()
	
	for param in sweeper:
	
		if not temp_bool:
			temp_run = Temp
		if not stress_bool and not misfit_bool and not hydro_bool:
			stress_run = Misfit_App_Stress
		if not elec_bool:
			elec_run = Elec_Field
		
		if SweepParameter.lower() == 'stress-xx':
			stress_run = np.asarray([1*param,Misfit_App_Stress[1],Misfit_App_Stress[2],Misfit_App_Stress[3],Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'stress-yy':
			stress_run = np.asarray([Misfit_App_Stress[0],1*param,Misfit_App_Stress[2],Misfit_App_Stress[3],Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'stress-zz':
			stress_run = np.asarray([Misfit_App_Stress[0],Misfit_App_Stress[1],1*param,Misfit_App_Stress[3],Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'stress-yz':
			stress_run = np.asarray([Misfit_App_Stress[0],Misfit_App_Stress[1],Misfit_App_Stress[2],1*param,Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'stress-xz':
			stress_run = np.asarray([Misfit_App_Stress[0],Misfit_App_Stress[1],Misfit_App_Stress[2],Misfit_App_Stress[3],1*param,Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'stress-xy':
			stress_run = np.asarray([Misfit_App_Stress[0],Misfit_App_Stress[1],Misfit_App_Stress[2],Misfit_App_Stress[3],Misfit_App_Stress[4],1*param])
		if SweepParameter.lower() == 'hydrostatic':
			stress_run = np.asarray([1*param,1*param,1*param,Misfit_App_Stress[3],Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'isotropic-misfit':
			stress_run = np.asarray([1*param,1*param,Misfit_App_Stress[2],Misfit_App_Stress[3],Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'uniaxial-misfit-x':
			stress_run = np.asarray([1*param,Misfit_App_Stress[1],Misfit_App_Stress[2],Misfit_App_Stress[3],Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'uniaxial-misfit-y':
			stress_run = np.asarray([Misfit_App_Stress[0],1*param,Misfit_App_Stress[2],Misfit_App_Stress[3],Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'misfit-shear-strain':
			stress_run = np.asarray([Misfit_App_Stress[0],Misfit_App_Stress[1],1*param,Misfit_App_Stress[3],Misfit_App_Stress[4],Misfit_App_Stress[5]])
		if SweepParameter.lower() == 'electric-field-x':
			elec_run = np.asarray([1*param,Elec_Field[1],Elec_Field[2]])
		if SweepParameter.lower() == 'electric-field-y':
			elec_run = np.asarray([Elec_Field[0],1*param,Elec_Field[2]])
		if SweepParameter.lower() == 'electric-field-z':
			elec_run = np.asarray([Elec_Field[0],Elec_Field[1],1*param])
		if temp_bool:
			temp_run = param

		f, equil_polar = Polarization_Solver(PolarProb, Temp=temp_run, Misfit_App_Stress=stress_run, Elec_Field=elec_run, Iterations=Iterations)
		
		if rotated:
			print(equil_polar[0],equil_polar[1],equil_polar[2])
			equil_polar = (m * sp.Matrix(equil_polar))
			print('')
			print(equil_polar[0],equil_polar[1],equil_polar[2])
			print('--------------------------------------------')
			p1_run = equil_polar[0];	p2_run = equil_polar[1];	p3_run = equil_polar[2]
		else:
			p1_run = equil_polar[0];	p2_run = equil_polar[1];	p3_run = equil_polar[2]
		
		if polr_bool:
			p1_list.append(p1_run);	p2_list.append(p2_run);	
			p3_list.append(p3_run); pmag_list.append((p1_run**2 + p2_run**2 + p3_run**2)**0.5)
		if diel_bool:
			if bulk:
				suscep_list = []
				for eq in suscep_lambda_eqs:
					suscep_list.append(eq(p1_run, p2_run, p3_run, temp_run, stress_run[0], stress_run[1], stress_run[2], stress_run[3], stress_run[4], stress_run[5], elec_run[0], elec_run[1], elec_run[2]))
				
				x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
				x_values = np.matrix(x_values,dtype='float')

				x_values = (np.linalg.inv(x_values))
			
				k_values = (x_values)/8.85e-12
				
				k11_list.append(k_values[0,0]);		k22_list.append(k_values[1,1])
				k33_list.append(k_values[2,2]);		k23_list.append(k_values[1,2])
				k13_list.append(k_values[0,2]);		k12_list.append(k_values[0,1])
				
			if film:
				suscep_list = []
				for eq in suscep_lambda_eqs:
					suscep_list.append(eq(p1_run, p2_run, p3_run, temp_run, stress_run[0], stress_run[1], stress_run[2], stress_run[3], stress_run[4], stress_run[5], elec_run[0], elec_run[1], elec_run[2]))
				
				x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
				x_values = np.matrix(x_values,dtype='float')

				x_values = (np.linalg.inv(x_values))
			
				k_values = (x_values)/8.85e-12
				
				k11_list.append(k_values[0,0]);		k22_list.append(k_values[1,1])
				k33_list.append(k_values[2,2]);		k23_list.append(k_values[1,2])
				k13_list.append(k_values[0,2]);		k12_list.append(k_values[0,1])
		if piez_bool:
			print('Need to Implement')
		if phas_bool:
			equil_polar, domain = Classify_Domains(equil_polar, System)
			domain_list.append(domain)
		
		#On_Screen_Updater(SweepParameter,param)
	
	
	if polr_bool:
		Export_Numerical_Data(p1_list, sweeper.tolist(),SweepParameter, Potential, Property, Data2=p2_list, Data3=p3_list, Data4=pmag_list, DataLabel=['P1','P2','P3','PMag'])
	if pyro_bool:
		Export_Numerical_Data(pi1_list, sweeper.tolist(),SweepParameter, Potential, Property, Data2=pi2_list, Data3=pi3_list, DataLabel=['Pi1','Pi2','Pi3'])
	if diel_bool:
		Export_Numerical_Data(k11_list, sweeper.tolist(),SweepParameter, Potential, Property, Data2=k22_list, Data3=k33_list, Data4=k23_list, Data5=k13_list, Data6=k12_list, DataLabel=['k11','k22','k33','k23','k13','k12'])
	if piez_bool:
		Export_Numerical_Data(d11_list, sweeper.tolist(),SweepParameter, Potential, Property, Data2=d22_list, Data3=d33_list, Data4=d15_list, Data5=d24_list, Data6=d31_list, DataLabel=['d11','d22','d33','d15','d24','d31'])
	if phas_bool:
		Export_Domain_Data(domain_list, sweeper.tolist(), SweepParameter, Potential, Property)

def Parameteric_Sweep_2D(System, Potential, Property, SweepParameters=['Temp','Stress'], SweepLists=[[100,300,10],[0e9, 5e8, 6e9]], Temp=298, Misfit_App_Stress=[0,0,0,0,0,0], Elec_Field=[0,0,0], XF=0, Iterations=500, RotateFlag=False, Rotation=[0,0,0]):

	sweeper1 = np.arange(SweepLists[0][0],SweepLists[0][1]+SweepLists[0][2], SweepLists[0][2])
	sweeper2 = np.arange(SweepLists[1][0],SweepLists[1][1]+SweepLists[1][2], SweepLists[1][2])
	print('Sweep List 1 is: ', sweeper1)
	print('Sweep List 2 is: ', sweeper2)
	
	study_bool_tags = ['stress-xx', 'stress-yy', 'stress-zz', 'stress-yz', 'stress-xz', 'stress-xy',
						'hydrostatic', 'isotropic-misfit', 'uniaxial-misfit-x', 'uniaxial-misfit-y',
						'misfit-shear-strain', 'temp', 'electric-field-x', 'electric-field-y', 'electric-field-z']
	study_bool_variables = [Misfit_App_Stress[0], Misfit_App_Stress[1], Misfit_App_Stress[2],
							Misfit_App_Stress[3], Misfit_App_Stress[4], Misfit_App_Stress[5],[Misfit_App_Stress[0], Misfit_App_Stress[1], Misfit_App_Stress[2]],[Misfit_App_Stress[0], Misfit_App_Stress[1]],Misfit_App_Stress[0],Misfit_App_Stress[1],Misfit_App_Stress[2], Temp,
							Elec_Field[0], Elec_Field[1], Elec_Field[2]]
						
	bool_dict = {}
	for tag, var in zip(study_bool_tags, study_bool_variables):
		bool_dict[tag] = var
	
	#Generate the Equilibrium Polarization Functions here
	
	bulk 	= 'bulk' in System.lower()
	film 	= 'film' in System.lower()
	rotated	= RotateFlag
	
	if bulk:
		T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3 = Setup_Bulk_Symbols()
		a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
		Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
		PolarProb = sp.lambdify([p1, p2, p3, T, s1, s2, s3, s4, s5, s6, e1, e2, e3], Prob, 'numpy')

	if film:
		if RotateFlag == True:
			print('Needs to be Implemented')
		else:
			T, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3, u11, u22, u12 = Setup_Film_Symbols()
			
			a0, a_values, c_values, s_values, q_values, p0 = Potential_Choice(Potential, T, Misfit_App_Stress, XF)
			
			Prob = Setup_Equations(a_values, c_values, s_values, q_values, p1, p2, p3, s1, s2, s3, s4, s5, s6, e1, e2, e3)
			
			Prob = Prob.subs({s3:Misfit_App_Stress[2],s4:Misfit_App_Stress[3],s5:Misfit_App_Stress[4]})
			
			solutions = (sp.solve([Prob.diff(s1)+u11,Prob.diff(s2)+u22,Prob.diff(s6)+u12],[s1,s2,s6]))
			
			Prob += u11 * s1 + u22 * s2 + u12 * s6
			
			stress_prob = Prob
			
			Prob = Prob.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6]})
			
			PolarProb = sp.lambdify([p1, p2, p3, T, u11, u22, u12, s3, s4, s5, e1, e2, e3], Prob, 'numpy')

	pyro_bool = 'pyro' 	in Property.lower()
	diel_bool = 'diel' 	in Property.lower()
	piez_bool = 'piez' 	in Property.lower()
	polr_bool = 'pol'	in Property.lower()
	phas_bool = 'phas'	in Property.lower()
		
	if diel_bool:
		k11_list = [];		k22_list = [];		k33_list = []
		k23_list = [];		k13_list = [];		k12_list = []
		n1 = Prob.diff(p1,p1);	n2 = Prob.diff(p2,p2)
		n3 = Prob.diff(p3,p3);	n4 = Prob.diff(p2,p3)
		n5 = Prob.diff(p1,p3);	n6 = Prob.diff(p1,p2)
		suscep_derives = [n1, n2, n3, n4, n5, n6]
		if bulk:
			suscep_lambda_eqs = []
			for eq in suscep_derives:
				suscep_lambda_eqs.append(sp.lambdify([p1, p2, p3, T, s1, s2, s3, s4, s5, s6, e1, e2, e3], eq, 'numpy'))
		if film:
			suscep_lambda_eqs = []
			for eq in suscep_derives:
				suscep_lambda_eqs.append(sp.lambdify([p1, p2, p3, T, u11, u22, u12, s3, s4, s5, e1, e2, e3], eq, 'numpy'))	
	if piez_bool:
		d11_list = [];		d22_list = [];		d33_list = []
		d15_list = [];		d24_list = [];		d31_list = []
		#Calculate the Strains from the Gibbs Energy
		eta1 = -stress_prob.diff(s1);	eta2 = -stress_prob.diff(s2)
		eta3 = -stress_prob.diff(s3);	eta4 = -stress_prob.diff(s4)
		eta5 = -stress_prob.diff(s5);	eta6 = -stress_prob.diff(s6)
		strains = [eta1, eta2, eta3, eta4, eta5, eta6]
		temp_list = []
		for eta in strains:
			temp_list.append(eta.subs({s1:solutions[s1],s2:solutions[s2],s6:solutions[s6]}))
		strains = temp_list
		
		#Calculate the Susceptabilities
		n1 = Prob.diff(p1,p1);	n2 = Prob.diff(p2,p2)
		n3 = Prob.diff(p3,p3);	n4 = Prob.diff(p2,p3)
		n5 = Prob.diff(p1,p3);	n6 = Prob.diff(p1,p2)
		suscep_derives = [n1, n2, n3, n4, n5, n6]
		
		#Calculate the Derivative of Strains with Respect to Polarization
		polars = [p1, p2, p3];	eta_derives = []
		for eta in strains:
			temp_derive = []
			for polar in polars:
				temp_derive.append(eta.diff(polar))
			eta_derives.append(temp_derive)
			
		print('suscep derives')
		print(suscep_derives)
		print('')
		print('eta derives')
		print(strains)
		quit()
	if polr_bool:
		p1_list = [];	p2_list = [];	p3_list = []; pmag_list = []
	if phas_bool:
		domain_list = []
	
	for param1 in sweeper1:
		if SweepParameters[0].lower() == 'hydrostatic':
			bool_dict['stress-xx'] = param1
			bool_dict['stress-yy'] = param1
			bool_dict['stress-zz'] = param1
		elif SweepParameters[0].lower() == 'isotropic-misfit':
			bool_dict['uniaxial-misfit-x'] = param1
			bool_dict['uniaxial-misfit-y'] = param1
		else:
			bool_dict[SweepParameters[0].lower()] = param1
		
		if polr_bool:
			temp_p1 = []; temp_p2 = []; temp_p3 = []; temp_pmag = []
		if piez_bool:
			temp_d11 = [];	temp_d22 = [];	temp_d33 = []
			temp_d15 = [];	temp_d24 = [];	temp_d31 = []
		if diel_bool:
			temp_k11 = [];	temp_k22 = [];	temp_k33 = []
			temp_k23 = [];	temp_k13 = [];	temp_k12 = []
		if phas_bool:
			temp_domain = []
			
		for param2 in sweeper2:
		
			start_time = time.time()
			if SweepParameters[1].lower() == 'hydrostatic':
				bool_dict['stress-xx'] = param2
				bool_dict['stress-yy'] = param2
				bool_dict['stress-zz'] = param2
			elif SweepParameters[1].lower() == 'isotropic-misfit':
				bool_dict['uniaxial-misfit-x'] = param2
				bool_dict['uniaxial-misfit-y'] = param2
			else:
				bool_dict[SweepParameters[1].lower()] = param2
				
			temp_run = bool_dict['temp']
			elec_run = [bool_dict['electric-field-x'],bool_dict['electric-field-y'],bool_dict['electric-field-z']]
			if System.lower() == 'bulk':
				stress_run = [bool_dict['stress-xx'],bool_dict['stress-yy'],bool_dict['stress-zz'],
								bool_dict['stress-yz'],bool_dict['stress-xz'],bool_dict['stress-xy']]
			elif System.lower() == 'film':
				stress_run = [bool_dict['uniaxial-misfit-x'],bool_dict['uniaxial-misfit-y'],bool_dict['misfit-shear-strain'],
								bool_dict['stress-zz'],bool_dict['stress-yz'],bool_dict['stress-xz']]

			f, equil_polar = Polarization_Solver(PolarProb, Temp=temp_run, Misfit_App_Stress=stress_run, Elec_Field=elec_run, Iterations=Iterations)
			
			if rotated:
				print('Need to Implement')
				print('Rotate from Global to Local')
			if not rotated:
				p1_run = equil_polar[0];	p2_run = equil_polar[1];	p3_run = equil_polar[2]
			
			if polr_bool:
				temp_p1.append(p1_run);	temp_p2.append(p2_run);	
				temp_p3.append(p3_run); temp_pmag.append((p1_run**2 + p2_run**2 + p3_run**2)**0.5)
			if pyro_bool:
				print('Need to Implement')
			if diel_bool:
				if bulk:
					suscep_list = []
					for eq in suscep_lambda_eqs:
						suscep_list.append(eq(p1_run, p2_run, p3_run, temp_run, stress_run[0], stress_run[1], stress_run[2], stress_run[3], stress_run[4], stress_run[5], elec_run[0], elec_run[1], elec_run[2]))
					
					x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
					x_values = np.matrix(x_values,dtype='float')

					x_values = (np.linalg.inv(x_values))
				
					k_values = (x_values)/8.85e-12
					
					k11_list.append(k_values[0,0]);		k22_list.append(k_values[1,1])
					k33_list.append(k_values[2,2]);		k23_list.append(k_values[1,2])
					k13_list.append(k_values[0,2]);		k12_list.append(k_values[0,1])
					
				if film:
					suscep_list = []
					for eq in suscep_lambda_eqs:
						suscep_list.append(eq(p1_run, p2_run, p3_run, temp_run, stress_run[0], stress_run[1], stress_run[2], stress_run[3], stress_run[4], stress_run[5], elec_run[0], elec_run[1], elec_run[2]))
					
					x_values = [[suscep_list[0],suscep_list[5],suscep_list[4]],[suscep_list[5],suscep_list[1],suscep_list[3]],[suscep_list[4],suscep_list[3],suscep_list[2]]]
					x_values = np.matrix(x_values,dtype='float')

					x_values = (np.linalg.inv(x_values))
				
					k_values = (x_values)/8.85e-12
					
					temp_k11.append(k_values[0,0]);		temp_k22.append(k_values[1,1])
					temp_k33.append(k_values[2,2]);		temp_k23.append(k_values[1,2])
					temp_k13.append(k_values[0,2]);		temp_k12.append(k_values[0,1])
			if piez_bool:
				print('Need to Implement')
			if phas_bool:
				equil_polar, domain = Classify_Domains(equil_polar, System)
				temp_domain.append(domain)
			
			On_Screen_Updater(SweepParameters[0],param1,Parameter2=SweepParameters[1],Parameter2Value=param2)
		
		if polr_bool:
			p1_list.append(temp_p1);	p2_list.append(temp_p2)
			p3_list.append(temp_p3);	pmag_list.append(temp_pmag)
		if diel_bool:
			k11_list.append(temp_k11);	k12_list.append(temp_k12)
			k22_list.append(temp_k22);	k23_list.append(temp_k23)
			k31_list.append(temp_k31);	k33_list.append(temp_k33)
		if piez_bool:
			d11_list.append(temp_d11);	d15_list.append(temp_d15)
			d22_list.append(temp_d22);	d24_list.append(temp_d24)
			d31_list.append(temp_d31);	d33_list.append(temp_d33)
		if phas_bool:
			domain_list.append(temp_domain)
	
	
	if polr_bool:
		Export_Numerical_Data(p1_list, sweeper1.tolist(), SweepParameters[0], Potential, Property, Data2=p2_list, Data3=p3_list, Data4=pmag_list, DataLabel=['P1','P2','P3','PMag'],YRange=sweeper2.tolist(), YLabel=SweepParameters[1])

	if diel_bool:
		Export_Numerical_Data(k11_list, sweeper1.tolist(), SweepParameters[0], Potential, Property, Data2=k22_list, Data3=k33_list, Data4=k23_list, Data5=k13_list, Data6=k12_list, DataLabel=['k11','k22','k33','k23','k13','k12'],YRange=sweeper2.tolist(), YLabel=SweepParameters[1])
	
	if piez_bool:
		Export_Numerical_Data(d11_list, sweeper1.tolist(), SweepParameters[0], Potential, Property, Data2=d22_list, Data3=d33_list, Data4=d15_list, Data5=d24_list, Data6=d31_list, DataLabel=['d11','d22','d33','d15','d24','d31'],YRange=sweeper2.tolist(), YLabel=SweepParameters[1])
	
	if phas_bool:
		Export_Domain_Data(domain_list, sweeper1.tolist(), SweepParameters[0], Potential, Property, YRange=sweeper2.tolist(), YLabel=SweepParameters[1])
		Phase_Map_Plotting(domain_list, SweepParameters[0],SweepParameters[1],SweepLists[0],SweepLists[1])

def Print_Dielectric_Tensor(Tensor, Potential):
	print('')
	print('*********************************************************')
	print('------------------Dielectric Tensor----------------------')
	print('-------------------',Potential,'-------------------------')
	print('-------------------Printed Unitless----------------------')
	print('{:.3f}'.format(round(Tensor[0,0],4)),'','{:.3f}'.format(round(Tensor[0,1],4)),'','{:.3f}'.format(round(Tensor[0,2],4)))
	print('{:.3f}'.format(round(Tensor[1,0],4)),'','{:.3f}'.format(round(Tensor[1,1],4)),'','{:.3f}'.format(round(Tensor[1,2],4)))
	print('{:.3f}'.format(round(Tensor[2,0],4)),'','{:.3f}'.format(round(Tensor[2,1],4)),'','{:.3f}'.format(round(Tensor[2,2],4)))
	print('---------------------------------------------------------')
	print('*********************************************************')
	
def Print_Piezoelectric_Tensor(Tensor, Potential):

	Tensor = Tensor * 1e12

	print('')
	print('**********************************************************')
	print('-----------------Piezoelectric Tensor---------------------')
	print('-------------------',Potential,'--------------------------')
	print('-------------------Printed in pm/V------------------------')
	print('{:.3f}'.format(round(Tensor[0,0],4)),'','{:.3f}'.format(round(Tensor[0,1],4)),'','{:.3f}'.format(round(Tensor[0,2],4)),'','{:.3f}'.format(round(Tensor[0,3],4)),'','{:.3f}'.format(round(Tensor[0,4],4)),'','{:.3f}'.format(round(Tensor[0,5],4)))
	print('{:.3f}'.format(round(Tensor[1,0],4)),'','{:.3f}'.format(round(Tensor[1,1],4)),'','{:.3f}'.format(round(Tensor[1,2],4)),'','{:.3f}'.format(round(Tensor[1,3],4)),'','{:.3f}'.format(round(Tensor[1,4],4)),'','{:.3f}'.format(round(Tensor[1,5],4)))
	print('{:.3f}'.format(round(Tensor[2,0],4)),'','{:.3f}'.format(round(Tensor[2,1],4)),'','{:.3f}'.format(round(Tensor[2,2],4)),'','{:.3f}'.format(round(Tensor[2,3],4)),'','{:.3f}'.format(round(Tensor[2,4],4)),'','{:.3f}'.format(round(Tensor[2,5],4)))
	print('----------------------------------------------------------')
	print('**********************************************************')

def On_Screen_Updater(Parameter1, Parameter1Value, Parameter2='',Parameter2Value=''):

	if Parameter2 == '':
		text_to_print = 'Completed: ' + Parameter1 + ' = ' + str(Parameter1Value)
	else:
		text_to_print = 'Completed: ' + Parameter1 + ' = ' + str(round(Parameter1Value,4)) + ' and ' + Parameter2 + ' = ' + str(round(Parameter2Value,4))
	
	print(text_to_print)

def Custom_LinePlot(X_List, Y_Lists, XLabel, YLabel, SeriesLabel=[''], MarkerType=['o'], LineStyle=['--'], SaveFlag=False, SaveName=''):

	print('Need to Implement')

def Custom_ScatterPlot(X_List, Y_Lists, XLabel, YLabel, SeriesLabel=[''], MarkerType=['o'], LineStyle=['--'], SaveFlag=False, SaveName=''):

	print('Need to Implement')

def Property_Map_Plotting(Data, XLabel, YLabel, XTicks, YTicks, SaveFlag=False, SaveName='', CLabel='', Colormap='viridis'):

	print('Need to Implement')

def Phase_Map_Plotting(Data, XLabel, YLabel, XTicks, YTicks, System='Bulk', SaveFlag=False, SaveName=''):

	domain_array = np.asarray(Data)
	unique_domains = np.unique(domain_array)
	num_uni_domains = len(unique_domains)
	domain_dict = {}
	for d in range(num_uni_domains):
		domain_dict[unique_domains[d]] = d
	
	size_factor = int(len(Data)) * int(len(Data[0]))
	
	x, y = np.meshgrid(np.arange(YTicks[0],YTicks[1]+YTicks[2],YTicks[2]),np.arange(XTicks[0],XTicks[1]+XTicks[2],XTicks[2]))
	
	for udomain in range(num_uni_domains):
		domain_array[domain_array == unique_domains[udomain]] = udomain
	
	domain_array = domain_array.astype('int')
	
	mticks = []
	for udomain in range(num_uni_domains):
		mticks.append(udomain + 0.5)
	
	fig = plt.figure()
	ax = fig.add_subplot(111)
	plt.scatter(x,y,c=domain_array,marker='s',s=10000/size_factor,cmap=Generate_Discrete_HeatMap(num_uni_domains,base_cmap='gist_rainbow'))
	cbar = plt.colorbar(ticks=mticks)
	plt.clim(0,num_uni_domains)
	plt.yticks(np.linspace(XTicks[0],XTicks[1],5))
	plt.xticks(np.linspace(YTicks[0],YTicks[1],5))
	plt.minorticks_on()
	plt.tick_params(axis='both',direction='inout',length=8,width=1,right=True,top=True)
	plt.tick_params(axis='both',direction='in',which='minor',length=3,width=1,right=True,top=True)
	cbar.set_ticklabels(unique_domains)
	
	plt.xlim(YTicks[0],YTicks[1])
	plt.ylim(XTicks[0],XTicks[1])
	plt.xlabel(YLabel, fontweight='bold',fontvariant='small-caps',fontsize=14)
	plt.ylabel(XLabel, fontweight='bold',fontvariant='small-caps',fontsize=14)
	
	plt.show()

def Generate_Discrete_HeatMap(NDomains, base_cmap='rainbow'):
	
	base = plt.cm.get_cmap(base_cmap)
	color_list = base(np.linspace(0,1,NDomains))
	cmap_name = base.name + str(NDomains)
	
	return base.from_list(cmap_name, color_list, NDomains)

def Export_Numerical_Data(Data1, XRange, XLabel, Potential, Study, YRange='', YLabel='', DataLabel='',Extension='txt', Data2 = '', Data3 = '', Data4 = '', Data5 = '', Data6 = ''):

	file_name = Study + '_' + Potential + '.' + Extension
	file_handler = open(file_name,'w')
	file_handler.write('This files corresponds to a ' + Study + ' using the ' + Potential + ' Thermodynamic Potential. \n')
	
	#check how many data sources I am writing
	All_Data = Data1
	sources = 1
	if Data2 != '':
		sources = 2
		All_Data = zip(Data1,Data2)
		if Data3 != '':
			sources = 3
			All_Data = zip(Data1,Data2,Data3)
			if Data4!= '':
				sources = 4
				All_Data = zip(Data1,Data2,Data3,Data4)
				if Data5 != '':
					sources = 5
					All_Data = zip(Data1,Data2,Data3,Data4,Data5)
					if Data6 != '':
						sources = 6
						All_Data = zip(Data1,Data2,Data3,Data4,Data5,Data6)
	
	if YRange != '':
		file_handler.write(XLabel + '\t' + YLabel)
		for label in DataLabel:
			file_handler.write( '\t' + label)
		file_handler.write('\n')
		if sources == 6:
			for x, d1, d2, d3, d4, d5, d6 in zip(XRange, Data1, Data2, Data3, Data4, Data5, Data6):
				for y, i, j, k, l, m, n in zip(YRange, d1, d2, d3, d4, d5, d6):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\t' + str(round(k,5)) + '\t' + str(round(l,5))  + '\t' + str(round(m,5)) + '\t' + str(round(n,5)) + '\n')
		if sources == 5:
			for x, d1, d2, d3, d4, d5 in zip(XRange, Data1, Data2, Data3, Data4, Data5):
				for y, i, j, k, l, m in zip(YRange, d1, d2, d3, d4, d5):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\t' + str(round(k,5)) + '\t' + str(round(l,5))  + '\t' + str(round(m,5)) + '\n')
		if sources == 4:
			for x, d1, d2, d3, d4 in zip(XRange, Data1, Data2, Data3, Data4):
				for y, i, j, k, l in zip(YRange, d1, d2, d3,d4):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\t' + str(round(k,5)) + '\t' + str(round(l,5))  + '\n')
		if sources == 3:
			for x, d1, d2, d3 in zip(XRange, Data1, Data2, Data3):
				for y, i, j, k in zip(YRange, d1, d2, d3):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\t' + str(round(k,5))  + '\n')
		if sources == 2:
			for x, d1, d2 in zip(XRange,Data1,Data2):
				for y, i, j in zip(YRange,d1,d2):
					file_handler.write(str(x) + '\t' + str(y) + '\t' + + str(round(i,5)) + '\t' + str(round(j,5)) + '\n')
		if sources == 1:
			for x, d1 in zip(XRange,Data1):
				for y, i in zip(YRange,d1):
					file_handler.write(str(x) + '\t' + str(y) + '\t'+ str(round(i,5)) + '\n')
				
	else:
		file_handler.write(XLabel)
		for label in DataLabel:
			file_handler.write( '\t' + label)
		file_handler.write('\n')
		if sources == 6:
			for x,i,j,k,l,m,n in zip(XRange, Data1, Data2, Data3, Data4, Data5, Data6):
				file_handler.write(str(x) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\t' + str(round(k,5)) + '\t' + str(round(l,5))  + '\t' + str(round(m,5)) + '\t' + str(round(n,5)) + '\n')
		if sources == 5:
			for x,i,j,k,l,m in zip(XRange, Data1, Data2, Data3, Data4, Data5):
				file_handler.write(str(x) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\t' + str(round(k,5)) + '\t' + str(round(l,5))  + '\t' + str(round(m,5)) + '\n')
		if sources == 4:
			for x,i,j,k,l in zip(XRange, Data1, Data2, Data3, Data4):
				file_handler.write(str(x) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\t' + str(round(k,5)) + '\t' + str(round(l,5))  + '\n')
		if sources == 3:
			for x,i,j,k in zip(XRange, Data1, Data2, Data3):
				file_handler.write(str(x) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\t' + str(round(k,5)) + '\n')
		if sources == 2:
			for x,i,j in zip(XRange, Data1, Data2):
				file_handler.write(str(x) + '\t' + str(round(i,5)) + '\t' + str(round(j,5)) + '\n')
		if sources == 1:
			for x,i in zip(XRange, Data1):
				file_handler.write(str(x) + '\t' + str(round(i,5)) + '\n')
				
	file_handler.close()
	
def Export_Domain_Data(DomainData, XRange, XLabel, Potential, Study, YRange='', YLabel='',Extension='txt'):
	
	file_name = Study + '_' + Potential + '.' + Extension
	file_handler = open(file_name,'w')
	file_handler.write('This files corresponds to a ' + Study + ' using the ' + Potential + ' Thermodynamic Potential. \n')
	
	if YRange != '':
		file_handler.write(XLabel + '\t' + YLabel + '\t' + 'Domain' + '\n')
		for x, d1 in zip(XRange, DomainData):
			for y, i in zip(YRange, d1):
				file_handler.write(str(x) + '\t' + str(y) + '\t' + str(i) + '\n')
	
	else:
		file_handler.write(XLabel + '\t' + 'Domain' + '\n')
		for x, i in zip(XRange, DomainData):
			file_handler.write(str(x) + '\t' + str(i) + '\n')

def Plot_2D_Contour(Plane, System, Potential, Temp=298, Misfit_App_Stress=[0,0,0,0,0,0], Elec_Field=[0,0,0], XF=0, Iterations=500, RotateFlag=False, Rotation=[0,0,0]):

	print('Need to implement')

def Classify_Domains(Polar, System):

	p1 = abs(Polar[0]);		p2 = abs(Polar[1]);		p3 = abs(Polar[2])
	
	CubicTol = 0.001
	
	p1p2 = min(p1/p2,p2/p1)
	p1p3 = min(p1/p3,p3/p1)
	p2p3 = min(p2/p3,p3/p2)
	
	#Cubic Domains
	if ( p1 <= CubicTol ) and ( p2 <= CubicTol ) and ( p3 <= CubicTol ):
		Domain = 'Cubic'
	#Tetragonal Domains
	elif ( p1 <= CubicTol ) and ( p2 <= CubicTol ) and (p3 > CubicTol ):
		Domain = 'Tc'
	elif ( p1 <= CubicTol ) and ( p2 > CubicTol ) and (p3 <= CubicTol ):
		Domain = 'Tb'
	elif ( p1 > CubicTol ) and ( p2 <= CubicTol ) and (p3 <= CubicTol ):
		Domain = 'Ta'
	#Orthorhombic Domains
	elif ( p1 <= CubicTol ) and ( p2 > CubicTol ) and (p3 > CubicTol ) and ( p2p3 > 0.95 ):
		Domain = 'Obc'
	elif ( p1 > CubicTol ) and ( p2 <= CubicTol ) and (p3 > CubicTol ) and ( p1p3 > 0.95 ):
		Domain = 'Oac'
	elif ( p1 > CubicTol ) and ( p2 > CubicTol ) and (p3 <= CubicTol ) and ( p1p2 > 0.95 ):
		Domain = 'Oab'
	#Rhomohedral Domains
	#If all the polarization components are similiar we should end up with a rhomohedral domain.
	elif ( p1 > CubicTol ) and ( p2 > CubicTol ) and ( p3 > CubicTol ) and (0.95 <= p1p2 <= 1.0) and (0.95 <= p1p3 <= 1.0) and (0.95 <= p2p3 <= 1.0):
		Domain = 'R'
	#Monoclinic Domains
	#This ensures that the structure will all be larger than a given Cubic Tolerance number, which makes sure we can't confuse an Orthorhombic domain with a Monoclinic Domain
	elif ( p1 > CubicTol ) and ( p2 > CubicTol ) and ( p3 > CubicTol ) and ( p1 < p3 ) and ( p2 < p3 ) and ( p1p3 >= 0.59 ) and ( p1p3 <= 1.0 ):
		Domain = 'Ma'
	elif ( p1 > CubicTol ) and ( p2 > CubicTol ) and ( p3 > CubicTol ) and ( p1 > p3 ) and ( p2 > p3 ) and ( p1p3 >= 1.0 ) and ( p1p3 <= 1.5 ):
		Domain = 'Mb'
	elif ( p1 > CubicTol ) and ( p2 < CubicTol ) and ( p3 > CubicTol ) and ( p1p3 <= 0.95 ):
		Domain = 'Mca'
	elif ( p1 < CubicTol ) and ( p2 > CubicTol ) and ( p3 > CubicTol ) and ( p2p3 <= 0.95 ):
		Domain = 'Mcb'
	elif ( p1 > CubicTol ) and ( p2 > CubicTol ) and ( p3 < CubicTol ) and ( p1p2 <= 0.95 ):
		Domain = 'Mab'
	#Triclinic Domains
	else:
		Domain = 'Tri'
	

	# if System.lower() == 'film':
		# print('Need to Implement film domain definitions')

	if System.lower() == 'bulk':
		if Domain == 'Tc' or Domain == 'Ta' or Domain == 'Tb':
			Domain = 'T'
			Polar = (min(p1,min(p2,p3)),min(p1,min(p2,p3)),max(p1,max(p2,p3)))
		elif Domain == 'Obc' or Domain == 'Oac' or Domain == 'Oab':
			Domain = 'O'
			Polar = (min(p1,min(p2,p3)),max(p1,max(p2,p3)),max(p1,max(p2,p3)))
		elif Domain == 'Cubic':
			Domain = 'C'
		else:
			Domain = 'R'
	
	return Polar, Domain
	
#Code-----------------------------------------------------------------------------------------------------
main()
