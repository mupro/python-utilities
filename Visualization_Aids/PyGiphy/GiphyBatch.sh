#! /bin/bash
#
# Batch GIF production

DIRVEC=( Li Wang07 Wang10 Bell )
FOLDVEC=( N20 N15 N10 N05 P00 P05 P10 P15 P20 )

ROOT=`pwd`
mkdir $ROOT/GIFREPO
REPOROOT=$ROOT/GIFREPO

for DIR in "${DIRVEC[@]}"
do
	J=0
	for FOLD in "${FOLDVEC[@]}"
	do
		for (( K=$J,K<=8,++K ))
		do
			FOLD2NAME=${FOLDVEC[$K]}
			cd $ROOT/$DIR/$FOLD/$FOLD2NAME
			python ~/work/bin/PyGiphy.py Polar 1000 200 55000 PNG $DIR.$FOLD.$FOLD2NAME
			cp $DIR.$FOLD.$FOLD2NAME.gif $REPOROOT
		done
		J=$J+1
	done
done
