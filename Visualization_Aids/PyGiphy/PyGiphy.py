#-----------------------------------------------
#Script Information
#Written by Jacob Zorn
#Penn State University
#3/27/2019
#Python GIF Creator
#------------------------------------------------

#------------------------------------------------
#License Information
#This software is licensed under the GNU General
#Public License version 3.0. All the necessary license
#information can be seen in the provided license.txt
#file that was provided with this software
#------------------------------------------------

#------------------------------------------------
#Script Usage
# Argument Choices:
# Arg1: DataFileName
# Arg2: Starting Iteration
# Arg3: Iteration Delta
# Arg4: Ending Iteration
# Arg5: Datafile Type (ex. PNG, SVG, TIF, JPG, etc.)
# Arg6: Output FileName

# Example Usage
# PyGiphy.py Polar 1000 1000 55000 PNG PolarGif
#-------------------------------------------------

#-------------------------------------------------
#Import necessary libraries/modules
import numpy as np
from PIL import Image, ImageFont, ImageDraw
import sys
import os
import imageio
#--------------------------------------------------

#--------------------------------------------------
#Script Code

file_id = sys.argv[1]
start_iter = int(sys.argv[2])
delta_iter = int(sys.argv[3])
end_iter = int(sys.argv[4])
read_type = sys.argv[5]
out_name = sys.argv[6]

out_file = out_name + '.gif'
images = []

for iter in range(start_iter, end_iter+1, delta_iter):
	iter_name = str(iter).zfill(8)
	file_name = file_id + '.' + iter_name + '.' + read_type.lower()
	
	im = Image.open(file_name)
	d = ImageDraw.Draw(im)
	location = (100,100)
	text_color = (100,100,200)
	
	d.text(location,str(iter),fill=text_color,size=40)
	
	im.save(file_name)
	
	images.append(imageio.imread(file_name))
	
imageio.mimsave(out_file,images,format='GIF',duration=0.1)

print('Completed Creating ', out_name,' into a gif.')

#-------------------------------------------------------
