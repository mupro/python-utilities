#---------------------------------------------
#Script Information
#Written by Jacob Zorn
#Penn State University
#2/5/2019
#Visualization of Scalar DataFiles
#---------------------------------------------

#---------------------------------------------
#License Information
#This software is licensed under the GNU General
#Public License version 3.0. All the necessary license
#information can be seen in the provided license.txt
#file that was provided with this software
#---------------------------------------------

#---------------------------------------------
#Script Usage
#Agrument Choices:
#Arg1: DataFileName (ex. Strain,Stress,Charge,etc.)
#Arg2: Starting Iteration
#Arg3: Iteration Delta
#Arg4: Ending Iteration
#Arg5: Plane to Plot (ex. XY, XZ, YZ)
#Arg6: Column to Plot (ex. 4, 5, 6, etc.)
#Arg7: Datafile type (ex. HDF5, txt, csv, dat, etc.)
#Arg8: Output Format (ex. PNG, SVG, TIF, JPG. etc.)
#Arg9: Slice Plane (ex. 4, 10, 14, etc.)
#Example Usage
#ScalarPy.py strain 1000 1000 30000 XY 3 dat PNG 2
#This command will plot the 3rd column on the
#XY at Z=2 plane of a collection of strain dat 
#files in PNG format. 
#---------------------------------------------

#---------------------------------------------
#Import necessary libaries/modules
import numpy as np
import pandas as pd
import sys
import os
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
#---------------------------------------------

#---------------------------------------------
#Actual Code

file_id = sys.argv[1]
start_iter = int(sys.argv[2])
delta_iter = int(sys.argv[3])
end_iter = int(sys.argv[4])
plane = sys.argv[5]
plot_col = int(sys.argv[6])
read_type = sys.argv[7]
out_type = sys.argv[8]
plot_plane = int(sys.argv[9])


for iter in range(start_iter,end_iter+1,delta_iter):
	iter_name = str(iter).zfill(8)
	file_name = file_id + '.' + iter_name + '.' + read_type
	pic_name = file_id + '.' + iter_name + '.' + plane + '.' + str(plot_col) + '.' + out_type
	
	#Will need to construct different reading mechanisms for different file types
	print('Reading ' + file_name + ' and beginning visualization.')
	f = open(file_name, 'r')
	dims = f.readline().split()
	dims = np.asarray(dims,dtype=int)
	
	data =[]
	for line in f:
		data.append(line.split())
	
	data = np.asarray(data,dtype=float)
	#Allocate the necessary array space for plotting
	arr1 = np.zeros((dims[0],dims[1],dims[2]))
	for row in data:
		arr1[row[0].astype(int)-1,row[1].astype(int)-1,row[2].astype(int)-1] = row[plot_col-1]
	
	if plane == 'XY' or plane == 'xy' or plane == 'Xy' or plane == 'xY':
		plt.imshow(arr1[:,:,plot_plane])
		plt.clim(np.amin(arr1[:,:,plot_plane]),np.amax(arr1[:,:,plot_plane]))
		
	if plane == 'XZ' or plane == 'xz' or plane == 'Xz' or plane == 'xZ':
		plt.imshow(np.rot90(arr1[:,plot_plane,:],1))
		plt.clim(np.amin(arr1[:,plot_plane,:]),np.amax(arr1[:,plot_plane,:]))
		
	if plane == 'YZ' or plane == 'yz' or plane == 'Yz' or plane == 'yZ':
		plt.imshow(arr1[plot_plane,:,:])
		plt.clim(np.amin(arr1[plot_plane,:,:]),np.amax(arr1[plot_plane,:,:]))
		
	plt.colorbar(orientation='horizontal')
	plt.set_cmap('hot')
	plt.savefig(pic_name,dpi=150,bbox_inches='tight',format=out_type)
	
print('Finished the visualization.')
