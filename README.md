# python-utilities

Python tools for MUPRO

# Useful links
Unit testing in python https://docs.python.org/3/library/unittest.html

Example for implementing a python testing pipeline on gitlab https://docs.gitlab.com/ee/ci/examples/test-and-deploy-python-application-to-heroku.html.

The python coding style guide https://www.datacamp.com/community/tutorials/pep8-tutorial-python-code

JZ: Working to make sure it falls in line with the python style guide. Looks good thus far, just some minor details


# TODOs
2. Writer a test class for the thermodynamics_notebook.py
3. Implement the python testing gitlab pipeline
4. Can we adjust the differential evolution algorithm to work faster?
4b. Some form of error tolerance? If change is less than 1e-7 stop or similiar?
4c. What effect does starting guess have on differential evolution convergence?
4d. Optimize the differential evolution algorithm via population size, cross pollination, and mutant rate.
5. Should we release as .py source file or package as .exe or similiar extension?
6. Pyroelectric Coefficient Calculations
8. Implement Rotostrictive Energy Contributions
9. Potential Fitting Functions
10. Test with a variety of papers

# Python Dependencies
In order to run this script the following python packages must be installed:

Numpy (https://www.numpy.org/)

Sympy (https://www.sympy.org/en/index.html)

MpMath (http://mpmath.org/)

MatPlotLib (https://matplotlib.org/)

# Licensing
Currently, the project is licensed via GNU GPL v.3.0. But we can adjust that depending on how we want to release the software. I think this is a pretty fair license but if we want to package it with mu-Pro or something we may want to rethink what License we have with this software. I have uploaded GNU GPL v.3.0 to this repo as well for the interested reader.
